var firstName
firstName = 'Oscar'
console.log(firstName)

var lastName = 'David'  // declarar / asignar
lastName = 'Ana' // reasignar
console.log(lastName)

var secondName = 'David'  // declarar y asignar
var secondName = 'Ana'   // reasignar
console.log(secondName)


// let
let fruit = 'Aple'  // declar y asignar
fruit = 'kiwi'      // reasignar``
console.log(fruit)

//const
const animal = 'dog'
// animal = 'cat'  // reasignar
// const animal = 'snake'
 // console.log(animal)

const vehicles = []
vehicles.push('card')
vehicles.push('Marnia')
console.log(vehicles)

vehicles.pop()
console.log(vehicles)

vehicles.push('bicy')
console.log(vehicles)
