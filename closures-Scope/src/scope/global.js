// variables
var a; // declarar
var b = 'b' // declaracion y asignacion
b = 'bb' // reasignacion
var a = 'aa' // redeclaracion

// Global scope

var fruit = 'Apple'  // global 
console.log(fruit)

function bestFruit() {
  console.log(fruit)
}

bestFruit()


function countries() {
  country = 'colombia';
  console.log(country)
}

countries()
console.log(country)
