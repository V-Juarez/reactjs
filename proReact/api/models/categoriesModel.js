import db from '../adapter'

function list () {
  return db.get('categories').value()
}

module.exports = { list }
