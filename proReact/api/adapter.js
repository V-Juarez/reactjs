// const low = require("lowdb");
import { low } from ('lowdb');
import FileSync from 'lowdb/adapters/FileSync'
import Memory from 'lowdb/adapters/Memory'

const json = require('./db.json')
const isLocal = !process.env.NOW_REGION
const type = isLocal ? new FileSync('./db.json') : new Memory

const db = low(type)
db.defaults(json).write()

module.exports = db
