import React from "react";
import { Anchor, Image } from './styles'

const IMAGE = 'https://i.imgur.com/dJa0Hpl.jpg';

export const Category = ({ cover = IMAGE, path, emoji = "?" }) => (
  <Anchor href={path}>
    <Image src={cover} />
    {emoji}
  </Anchor>    
);
