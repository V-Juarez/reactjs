import React, { useEffect, useState } from "react";
import { Category } from "../Category";

import { List, Item } from "./styles";
// import { categories as mockCategories } from "../../../api/db.json";

export const ListOfCategories = () => {
  // useState
  const [categories, setCategories] = useState();

  // useEffect
  useEffect(function () {
    window.fetch('https://petgram-server-edsf8xpy2.now.sh/categories')
      .then(res => res.json())
      .then(response => {
        setCategories(response)
      })
  })
  return (
    <List>
      {categories.map((category) => (
        <Item key={category.id}>
          <Category {...category} />
        </Item>
      ))}
    </List>
  );
};
