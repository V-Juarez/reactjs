import { keyframes, css } from "styled-components"

const fadeInKeyframes = keyframes`
  from {
    filter: blur(5px);
    opacity: 0;
  }
  to {
    fitler: blur(0);
    opacity: 1;
  }
`
export const fadeIn = ({ time = '3s', type = 'ease'} = {}) => css`
  animation: ${time} ${fadeInKeyframes} ${type};
`