## curso-platzi-react-avanzado ⚛️

En cambio se hace anexando al settings.json lo siguiente

```json
 // Configuraciones para integrar ESLint, prettier y vscode
    "editor.formatOnSave": true,
    // Colocar Off el format de JS y JSX, para poder hacerlo via Eslint
    "[javascript]": {
        "editor.formatOnSave": false
    },
    "[javascriptreact]": {
        "editor.formatOnSave": false
    },
    // Esto le dice al plugin de ESLint que cualquier lint se ejecute al salvar. 
    "editor.codeActionsOnSave": {
        "source.fixAll": true
    },
    // Adicional, PERO MEGA IMPORTANTE... si tienes prettier como extension, debes apagarlo para js y jsx pq esto se hara a traves del eslint
    "prettier.disableLanguages": [
        "javascript",
        "javascriptreact"
    ]
```

instalar la version semistandard en vez de a standar

```sh
npm i semistandard --save-dev
```

## Vercel

Clonen o descarguen el repositorio, luego ejecutan el comando vercel

```
$ vercel
```

Les saldran una serie de preguntas

```shell
? Set up and deploy “~\Downloads\react-vercel-master”? [Y/n] y (Presionan y)
? Which scope do you want to deploy to? [nick de vercel] (Presionan Enter)
? Link to existing project? [y/N] (Presionan n)
? What’s your project’s name? petgram-vercel (Presionan Enter)
? In which directory is your code located? ./ (Presionan Enter)
```

Al terminar saldria algo así

```shell
❗️  The `name` property in vercel.json is deprecated (https://vercel.link/name-prop)
�  Linked to cavesa/petgram-vercel (created .vercel)
�  Inspect: https://vercel.com/cavesa/petgram-vercel/n93wwp0u1 [3s]
✅  Production: https://petgram-vercel.vercel.app [copied to clipboard] [25s]
�  Deployed to production. Run `vercel --prod` to overwrite later (https://vercel.link/2F).
�  To change the domain or build command, go to https://vercel.com/cavesa/petgram-vercel/settings
```

Para el deploy en produccion debemos cambiar el output directory: para ellos nos vamos a vercel, buscamos el proyecto y luego en settings, habilitamos el output directory y escribimos dist le damos en el botón SAVE. Luego hacemos el siguiente codigo:

![img](https://i.imgur.com/YTGH874.png)

Luego hacemos el siguiente codigo:

```shwll
$ vercel --prod
```
