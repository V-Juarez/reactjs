import React from 'react';
import { useStorageListener } from './useStorageListener';
import './ChangeAlert.css'


function ChangeAlert({ sincronize }) {
  const { show, toggleShow } = useStorageListener(sincronize);

  if (show) {
    return (
      <div className="ChangeAlert-bg">
        <div className="ChangeAlert-container">
          <p>Haz realizado cambios en Tus TODOs en otra ventana</p>
        <p>Sincronizar TODO's</p>
        <button
          className="ChangeAlert-button TodoForm-button--add"
          onClick={toggleShow}
          >
          Yes!
          </button>
        </div>
      </div>
    );
  } else {
    return null;
  }
}


export { ChangeAlert };