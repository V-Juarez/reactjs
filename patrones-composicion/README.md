<h1>React.js: Patrones de Render y Composición</h1>

<h2>Juan DC</h2>

<h1>Table of Contents</h1>

- [1. Introducción](#1-introducción)
  - [Qué son los patrones de render](#qué-son-los-patrones-de-render)
  - [Filosofía y principios de diseño en React](#filosofía-y-principios-de-diseño-en-react)
- [2. Composición de componentes](#2-composición-de-componentes)
  - [Qué es composición de componentes y colocación del estado](#qué-es-composición-de-componentes-y-colocación-del-estado)
  - [Composición y colocación del estado en React](#composición-y-colocación-del-estado-en-react)
  - [Analizando la composición de Todo Machine](#analizando-la-composición-de-todo-machine)
  - [Composición de componentes con React Context](#composición-de-componentes-con-react-context)
  - [Composición de componentes sin React Context](#composición-de-componentes-sin-react-context)
- [3. Render props](#3-render-props)
  - [Qué son las render props y render functions](#qué-son-las-render-props-y-render-functions)
  - [Poniendo en práctica las render props](#poniendo-en-práctica-las-render-props)
  - [Súper poderes para render props y render functions](#súper-poderes-para-render-props-y-render-functions)
  - [React.Children y React.cloneElement](#reactchildren-y-reactcloneelement)
- [4. High Order Components](#4-high-order-components)
  - [Qué son los High Order Components](#qué-son-los-high-order-components)
  - [Creando tu primer HOC](#creando-tu-primer-hoc)
  - [Notificando cambios con StorageEventListener](#notificando-cambios-con-storageeventlistener)
  - [Completando el StorageEventListener](#completando-el-storageeventlistener)
  - [Resolviendo los retos de StorageEventListener](#resolviendo-los-retos-de-storageeventlistener)
- [5. React Hooks](#5-react-hooks)
  - [Render props vs. High Order Components vs. React Hooks](#render-props-vs-high-order-components-vs-react-hooks)
  - [Cambiando HOCs por React Hooks](#cambiando-hocs-por-react-hooks)
- [6. Próximos pasos](#6-próximos-pasos)
  - [Toma el Curso de React.js: Manejo](#toma-el-curso-de-reactjs-manejo)

# 1. Introducción

## Qué son los patrones de render

Los patrones de render son soluciones comunes que nos pueden ayudar a resolver un problema en específico, por lo general estos patrones son bien conocidos dentro de la comunidad, aunque esto no significa que siempre los debamos usar, ya que cada proyecto conlleva diferentes problemas y funcionalidades. Los patrones simplemente son formas que podríamos usar para llegar a una solución. 

**Anteriormente en Platzi:**

- Que es React
- Cuando usarlo
- Los componentes
- Los estados
- Los eventos
- Los efectos
- La comunicación con props y React context
- Transportación de componentes

**Sobre la calidad del código**

- Aprenderemos a mejorar nuestro código utilizando patrones de render.
- Siempre hay más de una forma de resolver un mismo problema.
- Hay soluciones fáciles de implementar al principio pero en el futuro no ayudaran
- Hay soluciones complicadas de implementar al principio pero en el futuro ayudaran si la app se vuelve grande.

**Aprenderemos principios:**

- Composición de componentes
- Colocación del estado

**Estos principios serán de ayuda al implementar patrones:**

- Render props
- High Order Components
- React hooks

**Spoiler:**

- Visualmente no cambiara nuestra app web TODO MACHINE
- Cambiará el código de el que organizamos nuestra app y con el que compatirmos lógica entre los componentes

## Filosofía y principios de diseño en React

Los principios de diseño en React:

- **Abstracciones comunes**: se refiere que a React no quiere incluir código inútil en su core, código que sea demasiado especifico para caso de uso demasiado concreto. Sin embargo, existen excepciones.
- **Interoperabilidad**: React trata de trabajar bien con otras bibliotecas de interfaz de usuario.
- **Estabilidad**: React va a mantener sus apis, componentes, funcionamiento, etc… aunque estén descontinuados para no romper el código que usamos.
- **Válvulas de escape**: Cuando React quiera descontinuar un patrón que no les gusta, es sus responsabilidad considerar todos los casos de uso existentes para él, y antes de descontinuarlo educar a la comunidad respecto a las alternativas.
- **Experiencia de desarrollo:** el objetivo de React no es solo que con su código podamos solucionar nuestro problemas también van a buscar una solución que nos den una buena experiencia y disfrute.
- **Implementación**: Siempre que sea posible React preferirá código aburrido a código elegante. El código es descartable y cambia a menudo. Así que es importante que no introduzca nuevas abstracciones internas al menos que sea absolutamente necesario. Código detallado que sea fácil de mover, cambiar y eliminar es preferido sobre código elegante que esté abstraído de manera prematura y que sea difícil de cambiar.
- **Optimizado para instrumentación:** React siempre va a buscar el nombre mas distintivos y detallados(no necesariamente nombres largos).
- **Dogfooding**: significa que React va a periodizar la implementación de funcionalidades que necesite su empresa, Facebook, Esto tiene la ventaja no solo para su empresa sino también a todos los desarrolladores que utiliza React.
- **Planificación**: Acá es donde nosotros dividimos nuestras responsabilidades de los que debemos hacer y lo que tiene que hacer React por detrás con las descripciones que le hacemos.
- **Configuración**: React cree que una configuración global no funciona bien con la composición. Dado que la composición es central en React, no proveen configuración global en el código. React siempre se asegurara que nosotros tengamos compatibilidad entre cualquier librería y aplicación que utilicemos React.
- **Depuración**: se refiere que a React siempre va a dejarte pistas un rastro predecible, donde podamos buscar los errores en nuestra aplicación.
- **Composición**: 
  [Documentación](https://es.reactjs.org/docs/design-principles.html)

#### Ideas/conceptos claves

**Principios de diseño** son los alineamientos, reglas o condiciones que sigue un equipo de desarrollo

**Código aburrido** se refiere al código fácil de reemplazar, mover y cambiar

#### Apuntes

- React nos brinda principios el cual tiene el propósito de encaminarnos y coherentes con el cual React espera que escribiéramos

**Framework o librería**

- Abstracciones comunes
  - React no desea incluir código demasiado específico el cual será para casos demasiados concretos, debido a que el mismo ya te los brinda
- Interoperabilidad
  - React debe acoplarse a una aplicación sin necesidad de reescribir todo el código

**Cambios al core**

- Estabilidad
  - React es consiente que es usado en empresas y lugares importantes, por ello los fundamentos se mantienen
  - En caso de haber alguna actualización fuerte, React da un tiempo para actualizarte
- Válvulas de escape
  - Si React deja de usar algún patrón, el mismo avisará y mencionara alguna mejor alternativa
- Experiencia de desarrollo
  - React tratará de encontrar soluciones las cuales como desarrolladores te sean simples de usar.
  - Usualmente tratan de brindarte una solución declarativa, en caso de no poderla realizar, dan una solución imperativa y en el peor de los casos brindan una solución que funcione

**Prioridades**

- Implementación

  - Siempre que sea posible React dará un código elegante

  - Pero nunca pondrán el código elegante por encima del

     

    ```
    código aburrido
    ```

    - Al utilizar código elegante es más complicado de buscar formas de mejorar y escalar

- Optimizado para instrumentación

  - React buscará el nombre más descriptivo para sus características
  - Un nombre descriptivo no necesariamente debe de ser un nombre largo, sino uno correcto el cual le dará sentido

- Dogfooding

  - De alguna manera el equipo de React le dará prioridad a las necesidades de Facebook

#### 👩‍💻 **Filosofía React para ti**

**El trabajo de React**

- Planificación
  - React será el encargado de recibir las órdenes y realizarlas como la renderización, el manejo de estado o eventos
- Configuración
  - React se asegura que siempre se tenga una compatibilidad con cualquier librería o aplicación que se utilice por lo cual la configuración del mismo no es algo que se te permita directamente como desarrollador

**Tu trabajo**

- Depuración

  - React te dejará migajas de pan o rastros donde podrás encontrar errores de tu aplicación, como ser errores del lenguaje, framework o incluso lógica
  - Recuerda que un error es más

  🪜 Pasos para solucionar errores:

  1. Lee bien los errores de consola
  2. Lee varias veces el código relacionado con la parte que nos está funcionando correctamente
  3. Investiga
     1. Busca el error en Google
     2. ¿Investiga el porqué la solución funciona?

- Composición

[![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org/docs/design-principles.html/favicon.ico)Principios de diseño – React](https://es.reactjs.org/docs/design-principles.html)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)¿Qué son los paradigmas de programación y cuál aprender?](https://platzi.com/blog/paradigmas-programacion/)

# 2. Composición de componentes ⚛

## Qué es composición de componentes y colocación del estado

Es un patrón para crear componentes que nos da libertad para elegir dónde y cómo usamos nuestros componentes. Cada componente debe cumplir una tarea muy específica pero no debe de decirnos exactamente como usar esa solución que nos provee, debe ser muy flexible dándonos libertad para usar la información como queramos.

Esto nos permite hacer a los componentes más fáciles de integrar al resto de componentes, y agiliza el proceso de reutilizar o hacer cambios en los componentes.

Colocación del estado

¿Dónde los guardamos? Este problema también se conoce como state colocation.

- Máxima cercanía a la relevancia: El estado debe estar tan cerca como sea posible de donde lo estemos usando y actualizando.
- Stateful vs stateless: Separar lógica y estado de componentes que manejan UI.


Ir de lo grande a lo específico.

Hay que examinar que componentes manejan su propio estado, asumiendo que todos los componentes consumen el estado general de la app queremos encontrar a los componentes que crean un estado aparte del general. De esta manera podemos dividir componentes, de un lado tendremos a los componentes que solo consumen el estado general de la app y esos son componentes stateless(de interfaz, de UI), y del otro lado a los componentes que crean su propio código interno (estado) serán los stateful y siguiendo el principio de separar al estado de la UI podemos dividir a estos componentes con su propio estado en 2 uno stateful, y el otro en stateless.

- La Composición de componentes indica que cada componente debe darnos mucha libertad para elegir donde y como usarlo
- Cada componente debe realizar una tarea en específica, pero no debe de decirnos como usar esa solución que nos brinda, debe de ser flexible al momento de utilizarlo
  - Nos permitirá
    1. Tener componentes mucho más fáciles de integrar al resto de componentes
    2. Nos facilitará reutilizar o hacer cambios en nuestros componentes

**Ejemplo**

Una solución para renderizar una tarea podría ser este componente

```jsx
const App = () => (
	<TodoList todos={todos} />
);
const TodoList = ({todos}) => (
	<section>
		{todos.map(todo => (
			<TodoItem {...todo} />
		))}
	</section>
)
```

Existe otra forma de realizar esta tarea, y es que el componente App tenga también la tarea de renderizar cada tarea, esta manera nos dará mayor flexibilidad con el componente de TodoList

```jsx
const TodoList = ({children}) => (
	<section>
		{children}
	</section>
)
const App = () => (
	<TodoList>
		{todos.map(todo => (
			<TodoItem {...todo} />
		))}
	</TodoList>
);
```

#### **Colocación del estado**

**¿Dónde va tu estado?**

- Máxima cercanía a la relevancia
  - El estado ira según al área donde se aplique el mismo
- Stateful vs. stateless
  - Se refiere a no tener revuelo entre componentes que manejan lógica y estado con los componentes que solo renderizan elementos

Se puede usar ambos principios siempre si los entendemos muy bien

Pensar en lo más grande y poco a poco ir a lo más especifico

#### ¿Necesitas React Context?

Al usar composición de componentes puede ser una alternativa interesante en caso de que el árbol de componentes no sea demasiado profundo.

**Ventajas**

- No será necesario pasar props por cada componente intermedio entre donde se encuentre el dato inicialmente hasta su destino
- Podrás entender la aplicación con entender lo que está pasando en un archivo

**Ejemplo**

```jsx
const App = () => {
	const [state, setState] = useState();
	return (
		<>
			<TodoHeader>
				<TodoCounter />
				<TodoSearch onSearch={setState} />
			</TodoHeader>
			<TodoList>
				{state.todos.map(todo => <TodoItem {...todo} {...state} />)}
			</TodoList>
		</>
	);
}
```

Puedes observar que el componente se puede comunicar sin mucha complicación directamente con sus componentes hijos, nietos o incluso bisnietos.

- De esta manera se está reduciendo la complejidad de los componentes intermedios
- Además que se puede observar legibilidad con solo ver un archivo
- Pero esta manera es insostenible en el caso que el árbol de componentes sea muy grande, pero aún se puede utilizar composición de componentes al utilizar React context

[![img](https://www.google.com/s2/favicons?domain=https://kentcdodds.com/blog/colocation/favicons/android-chrome-192x192.png)Colocation](https://kentcdodds.com/blog/colocation)

[![img](https://www.google.com/s2/favicons?domain=https://kentcdodds.com/blog/state-colocation-will-make-your-react-app-faster/favicons/android-chrome-192x192.png)State Colocation will make your React app faster](https://kentcdodds.com/blog/state-colocation-will-make-your-react-app-faster)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/composition-vs-inheritance.html/favicon.ico)Composition vs Inheritance – React](https://reactjs.org/docs/composition-vs-inheritance.html)

[![img](https://www.google.com/s2/favicons?domain=https://abs.twimg.com/responsive-web/client-web-legacy/icon-svg.168b89d5.svg)https://twitter.com/sparragus/status/1299427179210706944](https://twitter.com/sparragus/status/1299427179210706944)

## Composición y colocación del estado en React

⭐️ Cuando los componentes nietos de `App` no solo son nietos, sino también componentes hijos, podemos pasarles *props* directamente y mejorar su comunicación.

–

Casi siempre que llamamos a un componente… pos lo llamamos y ya. 😅

```html
function App() {
  return (
    <TodoHeader />
  );
}

function TodoHeader() {
  return (
    <TodoCounter />
  );
}
```

Esto implica que para compartir el estado debemos pasar *props* y *props* y *props* por cada componente intermedio entre `App` y los componentes que realmente necesiten esas *props* en cualquier lugar de la jerarquía. 😓

```html
function App() {
  const [state, setState] = React.setState(initialState);

  return (
    <TodoHeader state={state} setState={setState} />
  );
}

function TodoHeader({ state, setState }) {
  return (
    <header>
      <TodoCounter state={state} setState={setState} />
    </header>
  );
}
```

Pero otra forma de trabajar es que `App` no solo llame a sus componentes directamente hijos, sino que también llamen a los siguientes componentes en la jerarquía de la aplicación. 😮

```html
function App() {
  return (
    <TodoHeader>
      <TodoCounter />
    </TodoHeader>
  );
}

function TodoHeader({ children }) {
  return (
    <header>
      {children}
    </header>
  );
}
```

Y esta nueva forma de trabajar implica que ya no tenemos que pasar *props* y *props* y *props* entre App y el resto de componentes para compartir el estado, sino que `App` puede comunicarse directamente con el componente que realmente necesita ese estado. 🤩

```html
function App() {
  const [state, setState] = React.setState(initialState);

  return (
    <TodoHeader>
      <TodoCounter state={state} setState={setState} />
    </TodoHeader>
  );
}
```

💚 **Esta es la magia de la composición de componentes**.

 TypeScript y no sepan como tipar un children aquí os dejo dos formas de tipar los componentes de react que se suelen usar:

- Tipando inline en los parámetros del componente

```TypeScript
function TodoHeader({ children }: {children?: ReactNode}) {
  return (
    <>
             {children}
        </>
  );
}
```

- Creando un type que le aplicaremos esta se suele usar mucho por que queda más limpia

```TypeScript
type TodoHeaderProps = {
  children?: ReactNode
}

function TodoHeader({ children }: TodoHeaderProps) {
  return (
    <>
             {children}
         </>
  );
}
```

## Analizando la composición de Todo Machine

>  _Una composición saludable puede evitarnos el uso el react context_

## Composición de componentes con React Context

**codigo en TodoHeader**

```react
import React from 'react';

function TodoHeader({children}) {
  return (
    <header>
      {children}
    </header>
  );
}

export { TodoHeader };
```

**codigo en AppUI del header**

```react
        <TodoHeader>
        	<TodoCounter
          		totalTodos={totalTodos}
          		completedTodos={completedTodos}
        	/>
        <TodoSearch
          searchValue={searchValue}
          setSearchValue={setSearchValue}
        />
      </TodoHeader>
```

## Composición de componentes sin React Context

Usar props enviando componentes!

[Components React](https://daveceddia.com/pluggable-slots-in-react-components/)

```react
function App({ user }) {
	return (
		<div className="app">
			<Nav>
				<UserAvatar user={user} size="small" />
			</Nav>
			<Body
				sidebar={<UserStats user={user} />}
				content={<Content />}
			/>
		</div>
	);
}

// Accept children and render it/them
const Nav = ({ children }) => (
  <div className="nav">
    {children}
  </div>
);

// Body needs a sidebar and content, but written this way,
// they can be ANYTHING
const Body = ({ sidebar, content }) => (
  <div className="body">
    <Sidebar>{sidebar}</Sidebar>
    {content}
  </div>
);

const Sidebar = ({ children }) => (
  <div className="sidebar">
    {children}
  </div>
);

const Content = () => (
  <div className="content">main content here</div>
);
```

# 3. Render props

## Qué son las render props y render functions

Nos ayudan a elevar nuestra composición de componentes a otro nivel.

Las render props nos permiten ser más específicos sobre que vamos a renderizar, cuando y donde vamos a renderizar cada parte del contenido de nuestros componentes.

**Render Function**

Es el patrón de entregar la información de nuestro componente en una función. No es exclusivo de react context, nosotros podemos crear nuestros propios componentes que usen este patrón, que reciban una función para que le podamos enviar la información que queremos proveer y luego si, renderizar los componentes que ya tienen la info gracias a la función.

**Render Props**

Cuando ya no mandamos la función dentro del componente, si no que la enviamos en alguna otra propiedad del componente. Podemos jugar con este patrón para que compartir información sea más divertido.

Las renders props vienen a ayudar a hacer una composición de componentes más limpia. Seguimos con todas las ventajas de usar composición de componentes.
A partir de ahora podemos decir que tenemos 2 tipos de props: las props normales que reciben un valor o variable y por otro lado tenemos las props que contienen una función. Estas que contienen una función son las que nos interesan.
Esta función devuelve un componente o un elemento que pudiera tener anidados más elementos y componentes.
La sintaxis es la siguiente:

```react
<MyHeader 
	render={ () => <myLogo type={ type } /> } 
/> 
```

La propiedad render(la cual puede tener cualquier otro nombre) es nuestra render prop, porque contiene una función que al ser llamada devuelve un componente. Entonces cumple todas las condiciones y solo falta llamarla desde el componente que la contiene:

```react
function MyHeader( props ){
	return (
	<header className="header__styles">
		{ props.render() }
	</header>
	)
} 
```

### Entonces podemos interpretar que las render props le dicen al componentes que renderizan(cual va a ser su contenido)

Otra forma de hacer esto es con las render functions y lo que cambia es que se declaran dentro del componente:

```react
<MyHeader>
	{ () => <myLogo type={ type } /> } 
</MyHeader>
```

Y para acceder a lo que está dentro del componentes simplemente usamos props.children:

```react
function MyHeader( props ){
	return (
	<header className="header__styles">
		{ props.children }
	</header>
	)
} 
```

Es prácticamente lo que hicimos con la composición de componentes y la propiedad children.
¡Usen render props y mejoren sus composiciones amigos 😃 !



> ## 🌦️  **Render Function** El patrón de entregar la información en una función es lo que llamamos
>
> **Render Prop** El patrón de enviar como propiedad una función
>
> #### Apuntes
>
> - Son un patrón que nos ayudan a llevar la composición de componentes a un mayor nivel
> - Render Props nos dejan especificar:
>   - Que vamos a renderizar
>   - Cuando lo vamos a renderizar
>   - Donde lo vamos a renderizar
>
> ```jsx
> <Provider>
> 	<Consumer>
> 		{ something => (
> 				<TusComponentes
> 					{...something.xyz}
> 				/>
> 		)}
> 	</Consumer>
> </Provider>
> ```
>
> - React context se puede utilizar a través de los componentes de Provider y Consumer
>   - Con el componente consumer podíamos recibir una función con todas las propiedades que habíamos guardado en el provider, el concepto de esta aplicación es `**Render Function**`
> - También podemos realizar el siguiente código la cual utiliza **`Render Props`**
>
> ```jsx
> <RenderProps
> 	renderProp={<OtroCompo />}
> />
> 
> <RenderProps
> 	renderProp={info => <OtroCompo {...info} />}
> />
> ```

Por ejemplo podríamos realizar la siguiente aplicación

```jsx
<PlatziCourse
	classes={platziClass => (
		<PlatziClassPreview {...platziClass} />
	)}
	lastActivity={question => (
		<PlatziComment q={true} {...question} />
	)}
/>
```

error & correct message

```react
<SuperForMessage
    onError={error => (
        <FormError error={error} />
    )}
    onSuccess={newData => (
    	<FormSucces data={newData} />
    )}
  />
    
```

[Render Props](https://es.reactjs.org/docs/render-props.html)

## Poniendo en práctica las render props

Codigo de TodoList en App/Index.js

```react
      <TodoList
        error={error}
        loading={loading}
        searchedTodos={searchedTodos}
        onError={() => <TodosError />}
        onLoading={() => <TodosLoading />}
        onEmptyTodos={() => <EmptyTodos />}
        render={todo => (
          <TodoItem
            key={todo.text}
            text={todo.text}
            completed={todo.completed}
            onComplete={() => completeTodo(todo.text)}
            onDelete={() => deleteTodo(todo.text)}
          />
        )}
      />
```

codigo de TodoList en TodoList/index.js

```react
function TodoList(props) {
  return (
    <section className="TodoList-container">
      {props.error && props.onError()}
      {props.loading && props.onLoading()}
      {(!props.loading && !props.searchedTodos.length) && props.onEmptyTodos()}

      <ul>
        {props.searchedTodos.map(props.render)}
      </ul>
    </section>
  );
}
```

### TodoList

```react
function TodoList(props) {
    return (
        <section className="TodoList-container">
            {props.error && props.onError()}
            {props.loading && props.onLoading()}

            {(!props.loading && !props.searchedTodos.length) && props.onEmptyTodos()}

            {props.searchedTodos.map(props.render)}

            <ul>
                {props.children}
            </ul>
        </section>
    )
}
```

### index.js

```react
<TodoList
        error={error}
        loading={loading}
        searchedTodos={searchedTodos}
        onError={() => <TodosError />}
        onLoading={() => <TodosLoading />}
        onEmptyTodos={() => <EmptyTodos />}
        render={todo => (
          <TodoItem
            key={todo.text}
            text={todo.text}
            completed={todo.completed}
            onComplete={() => completeTodo(todo.text)} //Marcamos el todo como completado
            onDelete={() => deleteTodo(todo.text)} //Eliminamos en todo
            />
        )}
      />
```

## Súper poderes para render props y render functions

En el App.js:

```javascript
        totalTodos={totalTodos}
        searchedTodos={searchedTodos}
        searchText={searchValue}

        onEmptySearchResults={(searchText) => (
          <p>No hay resultado para {searchText} </p>
        )}
```

TodoList.js:

```jsx
const renderFunc = props.children || props.render;

{!props.loading && !props.totalTodos && props.onEmptyTodos()}
{!!props.totalTodos &&  !props.searchedTodos.length & props.onEmptySearchResults(props.searchText)}

{props.searchedTodos.map(renderFunc)}
```

## React.Children y React.cloneElement

Para poder pasar propiedades especiales a los componentes hijos de nuestros componentes contenedores cuando hacemos composición.

Cuando enviamos más de un componente o elemento hijo al que use CloneElement, la app deja de funcionar y suelta un error. CloneElement necesita recibir un elemento de react, cuando children es más de un componente entonces tenemos un array, para esto existe React.Children que nos ayuda a que CloneElement entienda sin importar cuantos elementos vienen en el props.children.

```jsx
function TodoHeader({ children, loading }) {
  //No importa si viene un elemento, o dos o null siempre nos devuelve un array

  return (
    <header>
      {React.Children.toArray(children).map((child) =>
        React.cloneElement(child, { loading: loading })
      )}
    </header> //Por cada child vamos a llamar a clone element.
  ); //Crear elemento a partir de otro (elemento, objeto con las props que queramos que tenga)
}
```

No son las herramientas más populares pero pueden ser muy útiles cuando queremos compartir una o ciertas props a los componentes hijos de un componente contenedor.

**React.cloneElement**

- Con esta característica de React podemos crear elementos de Nodos React
- Cabe aclarar que esta funciona con un unico nodo, en caso de aplicarla en un conjunto de los mismos podemos ayudarnos de `React.Children`

**React.Children**

- Nos permite manipular la prop children entre uno de sus usos podemos volver un conjunto de nodos react a un array

[![img](https://www.google.com/s2/favicons?domain=https://frontarm.com/james-k-nelson/passing-data-props-children//apple-touch-icon.png)How to pass data to a React component's props.children – Frontend Armory](https://frontarm.com/james-k-nelson/passing-data-props-children/)

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Passing props to props.children using React.cloneElement and render prop pattern | by Justyna Zet | Medium](https://medium.com/@justynazet/passing-props-to-props-children-using-react-cloneelement-and-render-props-pattern-896da70b24f6)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/react-api.html#reactchildren/favicon.ico)React Top-Level API – React](https://reactjs.org/docs/react-api.html#reactchildren)

# 4. High Order Components

## Qué son los High Order Components

Las funciones como las conocemos pueden devolvernos un valor en sus returns, pero estas funciones de “orden superior”, son funciones que devuelven otras funciones.

Si llamamos a la high order function y le enviamos un parámetro no tendremos todavía un resultado, como está devolviendo otra función tenemos que llamar a esa función que obtenemos luego de llamar a la de orden superior, enviarle los nuevos parámetros que necesita la función de retorno y entonces si, obtendremos nuestro resultado.

- Son funciones que retornan otras funciones aplicando el concepto funcional `currying`

```jsx
function highOrderFunction(var1) {
	return function returnFunction(var2) {
		return var1 + var2;
	}
}

const withSum1 = highOrderFunction(1);
const sumTotal = withSum1(2);

// 3
```

Debido a que los componentes son funciones podemos también aplicar este concepto

```jsx
// Caso base

function Componente(props){
	return <p>...</p>
}

function highOrderComponent() {
	return function Componente(props) {
		return <p>...</p>
	}
}
function highOrderComponent(WrappedComponent) {
	return function Componente(props) {
		return (
			<WrappedComponent
				{...algoEspecial}
				{...props}
			/>
		);
	}
}
```

- De esta manera estamos personalizando varios aspectos del componente deseado, como:
  - Los parámetros de las funciones nos permiten configurar el componente que envuelve, las props
  - Podemos reutilizar los HOC

**Ejemplos**

```jsx
function withApi(WrappedComponent) {
	const apiData = fetchApi('https://api.com');
	
	return function WrappedComponentWithApi(props) {
		if (apidData.loading) return <p>Loading</p>;
		return(
			<WrapperdComponent data={apiData.json} />
		);
	}
}
```

- Antes de retornar el componente en sí, hace una petición y entrega al componente esa información
- Además que podemos personalizar el estado de carga

```jsx
function TodoBox(props) {
	return (
		<p>
			Tu nombre es {props.data.name}
		</p>
	);
}

const TodoBoxWithApi = withApi(TodoBox);
```

- También podemos agregar más “capas” para tener más personalizaciones como por ejemplo

```jsx
function withApi(apiUrl){
	return function withApiUrl(WrappedComponent) {
		const apiData = fetchApi(apiUrl);
		
		return function WrappedComponentWithApi(props) {
			if (apidData.loading) return <p>Loading</p>;
			return(
				<WrapperdComponent data={apiData.json} />
			);
		}
	}
}
function TodoBox(props) {
	return (
		<p>
			Tu nombre es {props.data.name}
		</p>
	);
}

const TodoBoxWithApi = withApi('https://api.com')(TodoBox);
```

- Esto nos permite poder extender bastante su uso aplicándolo como.

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/higher-order-components.html/favicon.ico)Higher-Order Components – React](https://reactjs.org/docs/higher-order-components.html)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - deepsweet/hocs: Higher-Order Components for React](https://github.com/deepsweet/hocs)

## Creando tu primer HOC

`App.js`

```jsx
function App(props) {
  return (
    <h1>{props.saludo}, {props.nombre}</h1>
  );
}

function withSaludo(WrappedComponent) {
  return function WrappedComponetWithSaludo(saludo) {
    return function ComponenteDeVerdad(props) {
      return (
        <React.Fragment>
          <WrappedComponent {...props} saludo={saludo} />
          <p>Estamos acompanando al WrappedComponent</p>
        </React.Fragment>
        // <h2>Hey, buenas como has estado!!!</h2>;
      );
    };
  }
}

const AppWithSaludo = withSaludo(App)('Wenas');

ReactDOM.render(
  <AppWithSaludo  nombre="natan" />,
  // <App saludo="buenas" nombre="natan" />,
  document.getElementById('root')
);

```

> Los HOC pueden usarse para el manejo de rutas y elementos cuando implementas diferentes tipos de autentificación dentro del algún proyecto grande o mediano

con TypeScript esta es la mejor forma que se me ocurre lo he hecho con genéricos para que se adapte a cualquier componente

- Primero creamos el componente App que este no tiene ningún misterio solo pasarle el tipado como cualquier componente en TypeScript

```typescript
import React, { ComponentType } from 'react';

type App2Props = {
  name?: string;
  greeting?: string;
}

function App2(props: App2Props) {
  return (
    <h1>{props.greeting}, {props.name}!</h1>
    );
}
```

- Este sería el primer componente que creamos solo con un wrapper:

```TypeScript
function withWhatever<T>(Component: ComponentType<T>) {
  return function ComponenteDeVerdad(hocProps: T) {
    return (
      <>
        	<Component {...hocProps}/>
        	<p>Estamos acompañando al wrapped component</p>
      	     </>
    );
  }
}

const AppWithWhatever = withWhatever(App2)
```

- Y este con dos wrapper:

```TypeScript
function withSaludo<T>(Component: ComponentType<T>) {
  return function WrappedComponentWithGreeting(greeting: string) {
    return function ComponenteDeVerdad(hocProps: T) {
      return (
        <>
         		<Component {...hocProps} greeting={greeting}/>
         		<p>Estamos acompañando al wrapped component</p>
                 </>
      );
    }
  }
}

const AppWithSaludo = withSaludo(App2)("Hola")
```

- En el render es exactamente igual que en JavaScript:

```jsx
ReactDOM.render(
  <React.StrictMode>
    	<AppWithWhatever name="Jairo" greeting="Hola"/>
    	<AppWithSaludo name="Jairo" />
    </React.StrictMode>,
  document.getElementById('root')
);
```

**componentType** el cual uso para crear el tipado genérico se importa de React:

```TypeScript
import React, { ComponentType } from 'react';
```

## Notificando cambios con StorageEventListener

🌈 Los efectos son un tema espectacularmente divertidísimo dentro de React. Una de sus particularidades consideradas entre las más avanzados son las actualizaciones al estado dentro de los efectos y cómo estas afectan a los renders de nuestros componentes.

Si quieres que estudiemos este tema con la profundidad que merece, responde este comentario con un enorme 💚 y pondremos todo nuestro esfuerzo en grabar un **Curso de React.js: Optimización de Render y Debugging**.

ChangeAlert/index.js

```jsx
import React from 'react';
import { WithStorageListener } from './withStorageListener';

const ChangeAlert=({show,toggleShow})=>{
    if(show){return <p>Hubo cambios</p>}
    else{return <></>}
}

const ChangeAlertWithStorageListener = WithStorageListener(ChangeAlert)
export {ChangeAlertWithStorageListener}
```

ChangeAlert/WithStorageListener.js

```jsx
import React from 'react';

const WithStorageListener=(WrappedComponent)=>
{
return function WrappComponentWithStorageListener(props){
 const [storageChange,setStorageChange]= React.useState(false)
    return (
    <WrappedComponent
    show={storageChange}
    toggleShow={setStorageChange}
    />
    )
}
}

export {WithStorageListener}
```

## Completando el StorageEventListener

### (addEventListener(‘storage’)):

- **No funcionará en la misma página que realiza los cambios**; en realidad, es una forma de que otras páginas del dominio usen el almacenamiento para sincronizar los cambios que se realicen. Las páginas de otros dominios no pueden acceder a los mismos objetos de almacenamiento.

Esto lo saqué de MDN
![Captura de pantalla 2021-11-05 a la(s) 10.23.20.png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-05%20a%20la%28s%29%2010.23.20-acf700f5-92dc-4b45-93a9-5433b44cead4.jpg)

Para que no aparezca los todos mientras están sincronizándose

```jsx
{!props.loading && props.searchedTodos.map(renderFunc)}
```

```jsx
import React from 'react';
import { withStorageListener } from './withStorageListener';
// Se importa el HOC



//Componente
function Changealert({show,toggleShow,setItem}){

    if(show){  //Si show es verdadero
        return (

            <div>
                 <p>Hubo Cambios</p>
                 <button onClick={()=>{
                     
                     toggleShow(false);

                     const localStorageItem = localStorage.getItem('TODOS_V1');
                     const parsedItem = JSON.parse(localStorageItem);         
                     setItem(parsedItem)
                     
                     }}>Volver a cargar la información</button>
                
            </div>

        );

    }else{
            
        return null;

    }

       

}

const ChangeAlertWithStorageListener=withStorageListener(Changealert);
//Se crea una variable , y envuelve en el HOC componente Changealert


export {ChangeAlertWithStorageListener};
//Se exporta este vendria siendo el componente de verdad
```



## Resolviendo los retos de StorageEventListener

**Codigo usado por el profesor:**

```jsx
function ChangeAlert({ show, toggleShow}) {
    if(show){
        return (
            <div className="ChangeAlert-bg">
                <div className="ChangeAlert-container">
                <p>Parece que cambiaste tus TODOs en otra pestaña o ventana del navegador.</p>
                <p>¿Quieres sincronizar tus TODOs?</p>
                <button
                    className="TodoForm-button TodoForm-button--add"
                    onClick={toggleShow}
                >
                    Yes!
                </button>
                </div>
            </div>
        );
    } else{
        return null;
    }
}
```

codigo del css:

```css
.ChangeAlert-bg {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #1e1e1f50;
    z-index: 2;
}

.ChangeAlert-container {
    position: fixed;
    bottom: -10px;
    left: -10px;
    right: -10px;
    padding: 34px;
    background-color: #1e1e1f;
    color: #f2f4f5;
    z-index: 2;
    text-align: center;
    font-size: 20px;
}

.ChangeAlert-container button {
    margin-top: 16px;
    margin-bottom: 16px;
}
```

# 5. React Hooks

## Render props vs. High Order Components vs. React Hooks

**Maquetación**

Render props o render functions vs React hooks

- Ambas son formas correctas de trabajar y comunes.
- Las render props suben el nivel de elegancia del código pero también pueden bajar el nivel de código aburrido comparado con los react hooks.
- Si practicamos mucho podremos usar las render props de manera mucho más saludable para los componentes más estructuralmente importantes de nuestras apps. Nos ayudan a proteger nuestros componentes para que no nos equivoquemos y la maquetación sea correcta.


**Share data, compartir información entre componentes.**

Aquí participan todos los patrones.


Render Functions:

- Compartir info con funciones que en sus parámetros nos dejan esa info que necesitamos que nos compartieran.
- Si necesitamos demasiada info de distintas render functions para un mismo componente deja de verse bien y podría llegar al código spaghetti.


HOC:

- Funciones que pueden retornar y retornar y retornar otras funciones hasta que en algún momento retornemos un componente de react y podamos pasarle toda la info.
- Usarlos es sencillo, envolvemos nuestros componentes en estos HOC y automáticamente van a recibir toda la info que nos querían compartir estos HOC.
- Si necesitamos la info de muchos HOC’S en un mismo componente tenemos el mismo problema que con las render functions. Código muy horizontal.


React hooks

- Llamamos al react hook (oficial o custom) y luego consumimos la info en el return del componente.
- Cuando tenemos muchos llamados a distintos react hooks no hay código horizontal.
- Ganaron los hooks para compartir info entre varios componentes. 🎉

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Do React Hooks Replace Higher Order Components (HOCs)? | by Eric Elliott | JavaScript Scene | Medium](https://medium.com/javascript-scene/do-react-hooks-replace-higher-order-components-hocs-7ae4a08b7b58)

## Cambiando HOCs por React Hooks

Agregue la opción de no recargar. Ya que era casi obligatorio y no tendría sentido el botón.

useStorageListener.js

```
    const toggleShow = (even) => {
        if (even) {
            setStorageChange(false)        
        }else{
            sincronize();
            setStorageChange(false)            
        }    
    };
```

ChangeAlert/index.js

```jsx
<div className="ChangeAlert-container">
          <p>Hubo cambios</p>
          <p>¿Desea recargar?</p>
          <button
            className="TodoForm-button TodoForm-button--add"
            onClick={() => {
              toggleShow(false);
            }}
          >
            Yes
          </button>
          <button
            className="TodoForm-button TodoForm-button--add"
            onClick={() => {
              toggleShow(true);
            }}
          >
            do not update
          </button>
        </div>
```

> Prefiero los Hooks más si estas aprendiendo a trabajar con React porque los HOC tienen muchos pasos esto causa confución y muchas veces nos perdemos en e código.

# 6. Próximos pasos

## Toma el Curso de React.js: Manejo

![img](https://github.com/Jason171096/TODO-s/blob/main/gif.gif?raw=true)

![2021-09-27_20h49_40.png](https://static.platzi.com/media/user_upload/2021-09-27_20h49_40-8a257ef8-a233-4669-a744-49833556a917.jpg)