import { SET_POKEMOS } from "./types";

export const setPokemons = (payload) => ({
  type: SET_POKEMOS,
  payload,
}); 