import { Col } from "antd";
import Searcher from "./components/Searcher";
import "./App.css";
import PokemonList from "./components/PokemonList";
import { useEffect } from "react";
import { getPokemon, getPokemonDetails } from "./api";
import { setPokemons } from "./actions";
import { useDispatch, useSelector } from "react-redux";
import logo from "./static/logo.svg"

function App() {
  // function App({ pokemons, setPokemons }) {
  // const [pokemons, setPokemons] = useState([])
  const pokemons = useSelector((state) => state.pokemons)
  const dispatch = useDispatch()

  useEffect(() => {
    const fetchPokemons = async () => {
      const pokemonsRes = await getPokemon();

      const pokemonsDetailed = await Promise.all(
        pokemonsRes.map(pokemon => getPokemonDetails(pokemon))
      );
      dispatch(setPokemons(pokemonsDetailed));
    };

    fetchPokemons();
  }, []);

  return (
    <div className="App">
      <Col span={8} offset={8}>
        <img src={logo} alt="Pokemon" />
        <h1>PockeDex</h1>
        <Searcher />
      </Col>
      <PokemonList pokemons={pokemons} />
    </div>
  );
}

// const mapStateToProps = (state) => ({
//   pokemons: state.pokemons,
// });

// const mapDispatchToProps = (dispatch) => ({
//   setPokemons: (value) => dispatch(setPokemonsActions(value)),
// });

export default App;
// export default connect(mapStateToProps, mapDispatchToProps)(App);
