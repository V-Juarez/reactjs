import { ChevronRightIcon, CalendarIcon, ShoppingBagIcon } from "@heroicons/react/24/solid";

const OrdersCard = (props) => {
  const { totalPrice, totalProducts } = props;

  return (
    <div className="flex justify-between items-center mb-4 border border-black p-4 rounded-lg  w-80">
      <div className="flex justify-between w-full">
        <p className="flex flex-col">
          <span className="font-light"><CalendarIcon className="h-4 w-4 text-black" />01.02.23</span>
          <span className="font-medium"> 
          <ShoppingBagIcon className="h-4 w-4 text-black"></ShoppingBagIcon> 
          {totalProducts} articles
          </span>
        </p>
        <p className="flex gap-4 items-center">
          <span className="font-bold text-2xl"> $ {totalPrice}</span>
          <ChevronRightIcon className="h-6 w-6 text-black cursor-pointer"></ChevronRightIcon>
        </p>
      </div>
    </div>
  );
};

export default OrdersCard;
