import { XMarkIcon } from "@heroicons/react/24/solid";

const OrderCard = (props) => {
  const { id, title, imageUrl, price, handleDelete } = props;

  let renderXMarkIcon;
  if (handleDelete) {
    renderXMarkIcon = (
      <XMarkIcon
        onClick={() => handleDelete(id)}
        className="w-6 h-6 text-black cursor-pointer"
      >
      </XMarkIcon>
    );
  }

  return (
    <div className="flex justify-between items-center mb-4">
      <div className="flex gap-2 items-center">
        <figure className="w-20 h-20">
          <img
            className="w-full h-full rounded-lg object-over"
            src={imageUrl}
            alt={title}
          />
        </figure>
        <p className="text-sm font-light">{title}</p>
      </div>
      <div className="flex gap-2 items-center">
        <p className="text-lg font-medium">$ {price}</p>
        {renderXMarkIcon}
      </div>
    </div>
  );
};

export default OrderCard;
