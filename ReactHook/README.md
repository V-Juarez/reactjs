<h1>React Hooks</h1>

<h3>Oscar Barajas Tavares</h3>

<h1>Tabla de Contenido</h1>

- [1. ¡Bienvenida! Este es un curso especial de React Hooks](#1-bienvenida-este-es-un-curso-especial-de-react-hooks)
  - [¿Qué aprenderás en el Curso Profesional de React Hooks?](#qué-aprenderás-en-el-curso-profesional-de-react-hooks)
  - [¿Qué son los React Hooks y cómo cambian el desarrollo con React?](#qué-son-los-react-hooks-y-cómo-cambian-el-desarrollo-con-react)
- [2. Introducción a React Hooks](#2-introducción-a-react-hooks)
  - [useState: estado en componentes creados como funciones](#usestate-estado-en-componentes-creados-como-funciones)
  - [useEffect: olvida el ciclo de vida, ahora piensa en efectos](#useeffect-olvida-el-ciclo-de-vida-ahora-piensa-en-efectos)
  - [useContext: la fusión de React Hooks y React Context](#usecontext-la-fusión-de-react-hooks-y-react-context)
  - [useReducer: como useState, pero más escalable](#usereducer-como-usestate-pero-más-escalable)
  - [¿Qué es memoization? Programación funcional en JavaScript](#qué-es-memoization-programación-funcional-en-javascript)
  - [useMemo: evita cálculos innecesarios en componentes](#usememo-evita-cálculos-innecesarios-en-componentes)
  - [useRef: manejo profesional de inputs y formularios](#useref-manejo-profesional-de-inputs-y-formularios)
  - [useCallback: evita cálculos innecesarios en funciones](#usecallback-evita-cálculos-innecesarios-en-funciones)
  - [Optimización de componentes en React con React.memo](#optimización-de-componentes-en-react-con-reactmemo)
  - [Custom hooks: abstracción en la lógica de tus componentes](#custom-hooks-abstracción-en-la-lógica-de-tus-componentes)
  - [Third Party Custom Hooks de Redux y React Router](#third-party-custom-hooks-de-redux-y-react-router)
- [3. Configura un entorno de desarrollo profesional](#3-configura-un-entorno-de-desarrollo-profesional)
  - [Proyecto: análisis y retos de Platzi Conf Store](#proyecto-análisis-y-retos-de-platzi-conf-store)
  - [Instalación de Webpack y Babel: presets, plugins y loaders](#instalación-de-webpack-y-babel-presets-plugins-y-loaders)
  - [Configuración de Webpack 5 y webpack-dev-server](#configuración-de-webpack-5-y-webpack-dev-server)
  - [Configuración de Webpack 5 con loaders y estilos](#configuración-de-webpack-5-con-loaders-y-estilos)
  - [Loaders de Webpack para Preprocesadores CSS](#loaders-de-webpack-para-preprocesadores-css)
  - [Flujo de desarrollo seguro y consistente con ESLint y Prettier](#flujo-de-desarrollo-seguro-y-consistente-con-eslint-y-prettier)
  - [Git Hooks con Husky](#git-hooks-con-husky)
- [4. Estructura y creación de componentes para Platzi Conf Store](#4-estructura-y-creación-de-componentes-para-platzi-conf-store)
  - [Arquitectura de vistas y componentes con React Router DOM](#arquitectura-de-vistas-y-componentes-con-react-router-dom)
  - [Maquetación y estilos del home](#maquetación-y-estilos-del-home)
  - [Maquetación y estilos de la lista de productos](#maquetación-y-estilos-de-la-lista-de-productos)
  - [Maquetación y estilos del formulario de checkout](#maquetación-y-estilos-del-formulario-de-checkout)
  - [Maquetación y estilos de la información del usuario](#maquetación-y-estilos-de-la-información-del-usuario)
  - [Maquetación y estilos del flujo de pago](#maquetación-y-estilos-del-flujo-de-pago)
  - [Integración de íconos y conexión con React Router](#integración-de-íconos-y-conexión-con-react-router)
- [4. Integración de React Hooks en Platzi Conf Merch](#4-integración-de-react-hooks-en-platzi-conf-merch)
  - [Creando nuestro primer custom hook](#creando-nuestro-primer-custom-hook)
  - [Implementando useContext en Platzi Conf Merch](#implementando-usecontext-en-platzi-conf-merch)
  - [useContext en la página de checkout](#usecontext-en-la-página-de-checkout)
  - [useRef en la página de checkout](#useref-en-la-página-de-checkout)
  - [Integrando third party custom hooks en Platzi Conf Merch](#integrando-third-party-custom-hooks-en-platzi-conf-merch)
- [5. Configura mapas y pagos con PayPal y Google Maps](#5-configura-mapas-y-pagos-con-paypal-y-google-maps)
  - [Paso a paso para conectar tu aplicación con la API de PayPal](#paso-a-paso-para-conectar-tu-aplicación-con-la-api-de-paypal)
  - [Integración de pagos con la API de PayPal](#integración-de-pagos-con-la-api-de-paypal)
  - [Completando la integración de pagos con la API de PayPal](#completando-la-integración-de-pagos-con-la-api-de-paypal)
  - [Paso a paso para conectar tu aplicación con la API de Google Maps](#paso-a-paso-para-conectar-tu-aplicación-con-la-api-de-google-maps)
  - [Integración de Google Maps en el mapa de checkout](#integración-de-google-maps-en-el-mapa-de-checkout)
  - [Creando un Custom Hook para Google Maps](#creando-un-custom-hook-para-google-maps)
- [6. Estrategias de deployment profesional](#6-estrategias-de-deployment-profesional)
  - [Continuous integration y continuous delivery con GitHub Actions](#continuous-integration-y-continuous-delivery-con-github-actions)
  - [Compra del dominio y despliega con Cloudflare](#compra-del-dominio-y-despliega-con-cloudflare)
- [7. Optimización de aplicaciones web con React](#7-optimización-de-aplicaciones-web-con-react)
  - [Integración de React Helmet para mejorar el SEO con meta etiquetas](#integración-de-react-helmet-para-mejorar-el-seo-con-meta-etiquetas)
  - [Análisis de performance con Google Lighthouse](#análisis-de-performance-con-google-lighthouse)
  - [Convierte tu aplicación de React en PWA](#convierte-tu-aplicación-de-react-en-pwa)
- [8. Bonus: trabaja con Strapi CMS para crear tu propia API](#8-bonus-trabaja-con-strapi-cms-para-crear-tu-propia-api)
  - [Crea una API con Strapi CMS y consúmela con React.js](#crea-una-api-con-strapi-cms-y-consúmela-con-reactjs)
- [9. ¿Qué sigue en tu carrera profesional?](#9-qué-sigue-en-tu-carrera-profesional)
  - [Próximos pasos para especializarte en](#próximos-pasos-para-especializarte-en)

# 1. ¡Bienvenida! Este es un curso especial de React Hooks

## ¿Qué aprenderás en el Curso Profesional de React Hooks?

React Hooks profesional

## ¿Qué son los React Hooks y cómo cambian el desarrollo con React?

Es una característica que salió en la versión 16.8 en febrero de 2019.
Los Hooks vienen a cambiar la forma de desarrollo en React.
⠀
Vienen a resolver problemas ligados a React, como la complejidad de los componentes, no se podía compartir la lógica de estado entre componentes, Component Hell, etc.

Los Hooks presentan una alternativa al desarrollo con clases, ya que estos vienen a trabajar con funciones.

⠀
**¿Qué es un Hook?**
Un Hook es una función especial que nos permitirá conectarnos a características de React, para trabajar con métodos especiales, los cuales nos permitirán manejar el estado de mejor forma sin depender de clases.

Crear proyecto:
```sh
> npx create-react-app react-hooks
```

Ejecutar proyecto:

```bash
> npm run start
```

Uno de los creadores de los Hooks, Dan Abramov, también fue el creador de Redux!
Los hooks permiten que nuestros componentes funcionales puedan tener el ciclo de vida que tienen las clases además de mantener estado.
Así mismo, al trabajar con funciones y no con clases, no tenemos que preocuparnos por el uso de this que en React tiene un contexto distinto a los demás lenguajes, por lo que podía ser más problemático.

# 2. Introducción a React Hooks

## useState: estado en componentes creados como funciones

**React 17**
Nota: desde la version 17 de React es opcional importar React en todos los componentes solo se importa una sola vez en todo el proyecto, este caso solo se importa en el archivo index.js, y solo se necesita importar lo que vamos a usar de la librería
Antes en cualquier componente

```jsx
import React, {useState} from ‘react’;
```

Versión 17 de React

```jsx
import {useState} from 'react’
```

- [mas información acerca de esta versión](https://reactjs.org/blog/2020/10/20/react-v17.html)

**useState**

**Te permite poder usar variables de estado dentro de componentes funcionales.**

El Hook useState s**iempre nos retorna un array de dos posiciones**. En la primera posición [0] vamos a tener el estado y él la segunda posición [1] vamos a tener la funciona para manipular el estado.

const [state, setState] = useState(0);

En este caso hacemos uso de la **desestructuración del array una característica de ES6.**

state ⇒ 0

setState ⇒ Función que actualiza el estado

Nuestro estado puede ser de los siguente tipos:

-String

-Boolean

-Number

-Float

-Null

-Undefined

-Object

-Array

## useState: estado en componentes creados como funciones

Este Hook nos ayuda a manejar el estado en componentes creados como funciones

- Crear carpeta components
- Crear archivo Header.jsx
  - Instalar [plugin](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)

⠀
**Header.jsx**

```jsx
// traemos useState al documento
import React, {useState} from 'react'

const Header = () => {
    /*
     * Integrar useState a esta logica
     * useState va a manejar este estado 
     * y haremos una función que cambia de Darmode a lightmode
     */

    /*
     * Constante que va a estructurar 2 elementos
     * el primero(darkMode) es el estado
     * el segundo(setDarkMode) es la función que cambiará al estado(darkMode)
     * de useState y lo pasamos como una función con estado inicial false
     */
    const [darkMode, setDarkMode] = useState(false);

    /*
     * Función para hacer los cambios de estado
     */
    const handleClick = () => {
        setDarkMode(!darkMode);
    };

		/*
     * Creamos el header con el logo 
     * y un boton para activar el DarkMode
     * dentro del boton ingresamos la logica para mostrar darkmode o lightMode
     */
    return (
        <div className="Header">
            <h1>React hooks</h1>
            <button type="button" onClick={handleClick}>{darkMode ? 'Dark Mode' : 'Light Mode'}</button>
            <button type="button" onClick={() => setDarkMode(!darkMode)}>{darkMode ? 'Dark Mode 2' : 'Light Mode 2'}</button>
        </div>
    )
}

export default Header
```

⠀
**App.js**

```jsx
import './App.css';

// Importamos componente Header
import Header from './components/Header';

function App() {
  return (
    <div className="App">
      <Header />
      <h1>
        Hola Mundo
      </h1>
    </div>
  );
}

export default App;
```

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)ES7 React/Redux/GraphQL/React-Native snippets - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)useState - Repositorio del Curso Profesional de React Hooks](https://github.com/platzi/curso-react-hooks/tree/RH-useState)

## useEffect: olvida el ciclo de vida, ahora piensa en efectos

**Comparto el useEffect usando asyn/await**

```jsx
import React, { useState, useEffect } from "react";

const Characters = () => {
  const [characters, setCharacters] = useState([]);

  const getCharacters = async () => {
    const response = await fetch("https://rickandmortyapi.com/api/character");
    const data = await response.json();
    setCharacters(data.results);
  };

  useEffect(() => {
    getCharacters();
  }, []);

  return (
    <div className="Characters">
      {characters.map((character) => (
        <h2 key={character.id}>{character.name}</h2>
      ))}
    </div>
  );
};

export default Characters;
```

***useEffect\*** nos permite manejar efectos que van a ser transmitidos dentro del componente.
En este ejemplo se llama a una API, traemos la información y la ejecutaremos en el componente

1. Creamos el componente Characters.jsx
2. Usamos el API de RickandMorty

⠀
**Characters.jsx**

```jsx
// importar useState y useEffect
import React, {useState, useEffect} from 'react'


const Characters = () => {
    /**
     * Lógica de useState
     * constante donde internamente estructuramos los elementos que necesitamos
     * de useState y lo iniciamos como un vector vacío
     */
    const [characters, setCharacters] = useState([]);
    
    /**
     * Lógica de useEffect
     * es una función con 2 parámetros
     * el primero es una función anónima donde va a estar la lógica
     * el segundo es una variable que esta escuchando si hay cambios 
     */
    useEffect(() => {
        // useEffect llama a fetch, el cual obtiene la informacion de la api de RickAndMorty
        fetch('https://rickandmortyapi.com/api/character/')
        .then(response => response.json())
        .then(data => setCharacters(data.results));
    }, [])
    
    /** 
     * Nombre del personaje
     * Iteramos por cada uno de los elementos
     */
    return (
        <div className="Characters">
            {characters.map(character => (
                <h2>{character.name}</h2>
            ))}
        </div>
    )
}

export default Characters
```

⠀**App.js**

```jsx
import React from 'react'
// Importamos componente Header
import Header from './components/Header';
// Importamos componente Characters
import Characters from './components/Characters';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Characters />
      <h1>
        Hola Mundo
      </h1>
    </div>
  );
}

export default App;
```

![rick2.png](https://static.platzi.com/media/user_upload/rick2-01fbb692-8a87-4851-9000-3ac66633f03f.jpg)

## useContext: la fusión de React Hooks y React Context

A mi me gusta trabajar los context de la siguiente manera:

![code.png](https://static.platzi.com/media/user_upload/code-4463a0ea-8153-48ae-a2dd-19711f175615.jpg)

Ya luego el index.js queda solo:

![code.png](https://static.platzi.com/media/user_upload/code-9b0e0b48-86f0-4ddd-9f4a-055dc595a2c4.jpg)

Y para modificarlo sería (estoy usando tailwindcss por lo que solo pondré la lógica acá):

![code.png](https://static.platzi.com/media/user_upload/code-a99b953f-9989-45fa-90df-278bfc50c673.jpg)

Al tener un botón con el handleClick, cambiará el valor del theme (valor del context), cambiando así las clases. Para los componentes que no tengan action pero que si cambien clases, solo se destructura { theme } = useContext(ThemeContext)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Tutorial: Cómo acceder a los datos de tu aplicación sin sufrimiento con React Context API](https://platzi.com/blog/tutorial-como-acceder-a-los-datos-de-tu-aplicacion-con-react-context-api/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)useContext - Repositorio del Curso Profesional de React Hooks](https://github.com/platzi/curso-react-hooks/tree/RH-useContext)

## useReducer: como useState, pero más escalable



[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Tutorial: Cómo acceder a los datos de tu aplicación sin sufrimiento con React Context API](https://platzi.com/blog/tutorial-como-acceder-a-los-datos-de-tu-aplicacion-con-react-context-api/)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)React Hooks: useState vs. useReducer | Platzi](https://platzi.com/blog/usestate-vs-usereducer/)

## ¿Qué es memoization? Programación funcional en JavaScript

**Memoization**, o en español **memoización** (sí, sin r) es una técnica muy útil para evitar cálculos innecesarios en nuestro código. Guardamos el resultado de nuestros cálculos cada vez que los hacemos para no tener que repetirlos en el futuro. En otras palabras, **estamos ahorrando grandes cantidades de tiempo a cambio de “mucho” espacio de almacenamiento**.

La memoria de JavaScript no es infinita, existe un máximo de funciones y cálculos que podemos hacer. Incluso si no la usamos toda, gastarla excesivamente causará que nuestras aplicaciones corran lento, con mucho lag o sencillamente briden una muy mala experiencia a los usuarios.

Nuestro código puede parecer pequeño cuando utilizamos técnicas de programación funcional como currying y recursividad. Pero no te dejes engañar. Así estemos llamando a la misma función una y otra vez recursivamente, cada cálculo o llamado a esta función genera nuevos “bloques” en la pila de ejecuciones que debe hacer JavaScript. Esto afecta a la memoria de JavaScript y puede estropear nuestra aplicación.

La buena noticia es que muy seguramente no tienes de qué preocuparte. Este “problema” no será realmente un problema a menos que construyas aplicaciones muy, muy grandes (por ejemplo, videojuegos en el navegador) donde la optimización de memoria es vital.

¡Pero tampoco te relajes! Tu responsabilidad como desarrolladora web profesional es siempre prepararte para resolver cualquier problema de programación, incluso si requieren técnicas “avanzadas” de optimización para que nuestro programa funcione a la perfección y para todos nuestros usuarios.

Pongamos esto en práctica con dos buenos ejemplos.

### Ejemplo práctico de memoization : calcular el factorial

El factorial de un número es su multiplicación por todos los números anteriores a este hasta llegar al 1. La implementación por defecto de una función factorial utilizando recursividad en JavaScript se vería así:

```js
function factorial(n) {
	if (n === 1) {
		return 1;
	} else {
		return n * factorial(n - 1);
	}
}
```

![Función Factorial en JavaScript con Recursividad](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-10-21%20at%2010.59.28%20AM-249f263f-d0a5-4981-a060-2fc7bae6556b.jpg)

Al calcular el factorial de 5 estamos multiplicando **`5 \* 4 \* 3 \* 2 \* 1`**. Y si calculamos el factorial de 10 estamos multiplicando `10 * 9 * 8 * 7 * 6 *` **`5 \* 4 \* 3 \* 2 \* 1`**. Si te fijas bien, ambos cálculos terminan igual, con las multiplicaciones del 5 hasta el 1. Esto significa que al ejecutar el factorial de 5 y luego el factorial de 10, estamos repitiendo la última parte del cálculo.

Vamos a crear una variable de tipo array que nos permita ir guardando el resultado de todos nuestros cálculos. Luego actualizaremos nuestra función para que antes de hacer los cálculos inspeccionamos nuestra variable para encontrar si el factorial de nuestro número ya fue realizado antes y no debemos volver a hacer el cálculo.

```js
const memo = [];

function memoFactorial(n) {
	if (n === 1) {
		return 1;
	} else if (!memo[n]) {
		memo[n] = n * memoFactorial(n - 1);
	}  
	return memo[n];
}
```

![Función Factorial en JavaScript con Recursividad y Memoización](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-10-21%20at%2010.58.12%20AM-ba8cd3cd-47f6-4251-b2a6-da4e454bc132.jpg)

### Segundo ejemplo práctico: [la secuencia Fibonacci](https://platzi.com/clases/1835-programacion-estocastica/26430-optimizacion-de-fibonacci/)

La función Fibonacci es una sucesión de números que llega hasta el infinito. Cada nuevo número en la sucesión es la suma del cálculo Fibonacci de los dos números anteriores en la sucesión (que empieza con cero y uno).

```math
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377...
```

![Cálculo Fibonacci](https://static.platzi.com/media/user_upload/fibonacci4-66d7543c-909e-4e51-b056-75bed4b06b0f.jpg)

El código en JavaScript para encontrar el número que ocupa X posición en la secuencia Fibonacci utilizando recursividad sería el siguiente:

```js
function fibonacci(n) {
	if (n === 0 || n === 1) {
		return 1;
	} else {
		return fibonacci(n - 1) + fibonacci(n - 2);
	}
}
```

![Función Fibonacci en JavaScript con Recursividad](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-07%20at%2010.36.48%20AM-41c8d336-6a36-4664-9a42-01c926a7ce19.jpg)

El cálculo es correcto, el cuarto número en la sucesión Fibonacci es 5. El problema es que estamos repitiendo muchas veces el cálculo de varios números.

![Función Fibonacci en JavaScript con Recursividad](https://static.platzi.com/media/user_upload/fibonacci4withoutmemo-7d9dd781-2770-4836-a6df-d0a6fef6f916.jpg)

Repetimos 5 veces el cálculo de `fibonacci(0)`, 3 veces `fibonacci(1)` y 2 veces `fibonacci(2)`. Para calcular el fibonacci de 4 puede no ser tan grave, pero mientras más grande sean los números, más cálculos debemos realizar y, por lo tanto, más cálculos estaremos repitiendo.

Para darte una idea, este es nuestro log al calcular fibonacci de 8.

![Función Fibonacci en JavaScript con Recursividad](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-07%20at%2011.22.48%20AM-916009f8-5bb8-4da0-bbfb-70cfb6adec39.jpg)

La buena noticia es que utilizando *memoization* podemos evitar hacer los mismos cálculos una y otra vez. Así como en el ejemplo anterior, vamos a guardar el resultado de cada cálculo Fibonacci en una variable memo, así cuando debamos volver a calcular el Fibonacci de un número, podemos simplemente utilizar el resultado que previamente calculamos.

```js
const memo = [];

function memoFibonacci(n) {
	if (n === 0 || n === 1) {
		return 1;
	} else if (!memo[n]) {
		memo[n] = memoFibonacci(n - 1) + memoFibonacci(n - 2);
	}  
	return memo[n];
}
```

Ahora el cálculo de `memoFibonacci(4)` es mucho más corto:

![Función Fibonacci en JavaScript con Recursividad y Memoización](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-07%20at%2011.34.10%20AM-bfe013d9-39aa-4436-ae98-0ff6bcd1b24f.jpg)

Incluso memoFibonnacci de 8 está increíblemente más optimizado que antes, sobre todo si anteriormente hemos calculado algún otro número Fibonacci:

![Función Fibonacci en JavaScript con Recursividad y Memoización](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-07%20at%2011.34.44%20AM-2ae01c10-3e74-42c6-9db3-824e5bc5a882.jpg)

------

Recuerda que solo debemos implementar memoización en funciones puras, es decir, funciones que siempre devuelven el mismo resultado cuando enviamos los mismos argumentos. No implementes memoización en llamados a una API o para trabajar con fechas y horas en JavaScript.

## useMemo: evita cálculos innecesarios en componentes

- Nos ayudan a evitar cálculos innecesarios dentro de nuestros componentes, guardando los resultados en memoria de tal forma que solo se necesite realizar una solución para un problema y esa solución guardarlo en memoria, para que en una próxima ocación se acceda a este resultado

**Ejemplo del hook**

```jsx
import React, { useState, useEffect, useReducer, useMemo } from 'react';

...
const [search, setSearch] = useState('');
...

const filteredUsers = useMemo(
    () =>
      characters.filter((user) => {
        return user.name.toLowerCase().includes(search.toLocaleLowerCase());
      }),
    [characters, search]
  );
```

```jsx
      <div className="Search">
        <input type="text" name="" id="" value={search} onChange={handleSearch} />
      </div>

      {flitteredUsers.map(character => (
        <div className="item" key={character.id}>
          <h2>{character.name}</h2>
          <button type="button" onClick={() => handleClick(character)}>Agregar a Favoritos</button>
        </div>
      ))}
    </div>
```

**NOTA.-** Escribe tu código para que aún funcione sin useMemo - y luego agrégalo para optimizar el rendimiento. Debido a que en el futuro, React puede elegir “olvidar” algunos valores previamente memorizados y recalcularlos en el próximo renderizado, por ejemplo para liberar memoria para componentes fuera de pantalla.

**Ideas/conceptos claves**

**useMemo**.- Devuelve un valor memorizado

## useRef: manejo profesional de inputs y formularios

- Manejo profesional de inputs y formularios

**Creación de la referencia**

```jsx
const refContainer = useRef(initialValue);
```

**Uso de la referencia**

```jsx
<input
  type="text"
  value={search}
  ref={searchInput}
  onChange={handleSearch}
/>
```

<h3>Ideas/conceptos claves</h3>

useRef.- devuelve un objeto ref mutable cuya propiedad .current se inicializa con el argumento pasado (initialValue). El objeto devuelto se mantendrá persistente durante la vida completa del componente

**Referencias**

https://es.reactjs.org/docs/hooks-reference.html#useref

Use ref nos sirve para obtener un elemento del DOM construido por React de tal forma que podamos acceder a sus atributos de una forma imperativa

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)useRef - Repositorio del Curso Profesional de React Hooks](https://github.com/platzi/curso-react-hooks/tree/useRef)

## useCallback: evita cálculos innecesarios en funciones

**“Las optimizaciones de performance siempre vienen con un costo, y no siempre vienen con un beneficio”**

[When to useMemo and useCallback](https://kentcdodds.com/blog/usememo-and-usecallback)

- Cada vez que hacemos un render se vuelve a construir las referencias a las funciones
- La solución para este problema es usar useCallback el cual solo genera una referencia para una función
- Es decir que memoriza la funcion
- a través de la lista de dependencias que mandamos cuando lo generamos, estamos indicando cuando debe volver a memorizar esa función, es decir cuando cambien esos valores
- Esto es útil cuando se transfieren callbacks a componentes hijos optimizados que dependen de la igualdad de referencia para evitar renders innecesarios

```jsx
const memoizedCallback = useCallback(
  () => {
    doSomething(a, b);
  },
  [a, b],
);
```

**Ideas/conceptos claves**

**useCallback**.- Memoriza una función

`Characteres.jsx`

```jsx
import React, { useState, useEffect, useReducer, useMemo, useRef, useCallback } from 'react';
import Search from './Search';

const initialState = {
  favorites: []
}

const favoriteReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_TO_FAVORITE':
      return {
        ...state,
        favorites: [...state.favorites, action.payload]
      };
    default:
      return state;
  }
}

const Characters = () => {
  const [characters, setCharacters] = useState([]);
  const [favorites, dispatch] = useReducer(favoriteReducer, initialState);
  const [search, setSearch] = useState('');
  const searchInput = useRef(null);

  useEffect(() => {
    fetch('https://rickandmortyapi.com/api/character/')
      .then(response => response.json())
      .then(data => setCharacters(data.results));
  }, []);

  const handleClick = favorite => {
    dispatch({ type: 'ADD_TO_FAVORITE', payload: favorite })
  }

  // const handleSearch = () => {
  //   setSearch(searchInput.current.value);
  // }

  const handleSearch = useCallback(() => {
    setSearch(searchInput.current.value);
  }, [])

  // const filteredUsers = characters.filter((user) => {
  //   return user.name.toLowerCase().includes(search.toLowerCase());
  // })

  const filteredUsers = useMemo(() =>
    characters.filter((user) => {
      return user.name.toLowerCase().includes(search.toLowerCase());
    }),
    [characters, search]
  )

  return (
    <div className="Characters">

      {favorites.favorites.map(favorite => (
        <li key={favorite.id}>
          {favorite.name}
        </li>
      ))}

      <Search search={search} searchInput={searchInput} handleSearch={handleSearch} />

      {filteredUsers.map(character => (
        <div className="item" key={character.id}>
          <h2>{character.name}</h2>
          <button type="button" onClick={() => handleClick(character)}>Agregar a Favoritos</button>
        </div>
      ))}
    </div>
  );
}

export default Characters;
```

`Search.jsx`

```jsx
import React from 'react';

const Search = ({ search, searchInput, handleSearch }) => {
  return (
    <div className="Search">
      <input type="text" value={search} ref={searchInput} onChange={handleSearch} />
    </div>
  );
}

export default Search;
```

[Referencia de la API de los Hooks - React](https://es.reactjs.org/docs/hooks-reference.html#usecallback)

## Optimización de componentes en React con React.memo

### ¿Qué significa optimización en React?

**No existe una sola forma de optimizar componentes**. Hay muchísimas formas de crear componentes y aún así podemos mostrar el “mismo” resultado en pantalla. Pero la forma en que lo hacemos puede afectar notoriamente el rendimiento del proyecto para nuestros usuarios.

Optimizar no es una sola técnica o fórmula secreta. Optimizar significa analizar los componentes de nuestro proyecto para mejorar el tiempo que tardamos en ejecutar cierto proceso o identificar procesos que estamos ejecutando en momentos innecesarios y le cuestan trabajo a la aplicación.

En esta lectura vamos a utilizar 2 herramientas oficiales de React para optimizar nuestros componentes. Pero ¿para qué tipo de optimización podemos utilizarlas? Vamos a **evitar que nuestros componentes se rendericen innecesariamente**.

### React.memo vs. React.PureComponent

Vamos a evitar renders innecesarios causados por un mal manejo de las props.

#### ¿Cómo funciona PureComponent?

**PureComponent** es una clase de React muy similar a React.Component, pero por defecto el método **`shouldComponentUpdate`** compara las props nuevas y viejas, si no han cambiado, evita volver a llamar el método render del componente. Esta comparación se llama [Shallow Comparison](https://github.com/facebook/react/blob/cb7075399376f4b913500c4e377d790138b31c74/packages/shared/shallowEqual.js#L19).

> Esta lectura te ayudará si quieres profundizar en cómo funcionan los objetos en JavaScript y por qué es necesario implementar shallow comparison en vez de una comparación “normal”: [Aprende a Copiar Objetos en JavaScript sin morir en el intento](https://platzi.com/blog/como-copiar-objetos-en-javascript-sin-morir-en-el-intento/).

#### ¿Cuándo debo usar React.PureComponent?

En este ejemplo práctico crearemos 3 componentes, un papá y dos hijos. El componente padre tiene un estado con dos elementos, count y canEdit. El padre tiene dos funciones que actualizan cada elemento del estado. Y cada elemento del estado se envía a un componente hijo diferente.

Componente padre (App):

```js
class App extends React.Component {
  constructor(props) {
      super(props);

      this.state = { count: 1, canEdit: true };
    }
    
    render() {
        console.log("Render App");

        const toggleCanEdit = () => {
            console.log("Click al botón de toggleCanEdit");
            this.setState(({ canEdit: oldCanEdit }) => {
              return { canEdit: !oldCanEdit };
            });
        };

        const countPlusPlus = () => {
          console.log("Click al botón de counter");
          this.setState((prevState) => {
            return { count: prevState.count + 1 };
          });
        };

        return (
            <>
              <button onClick={countPlusPlus}>Counter +1</button>
              <Counter count={this.state.count} />

              <button onClick={toggleCanEdit}>Toggle Can Edit</button>
              <Permissions canEdit={this.state.canEdit} />
            </>
        );
    }
}
```

Componente hijo (counter):

```js
class Counter extends React.Component {
    render() {
        console.log("Render Counter")
        const { count } = this.props;
 
        return (
            <form>
                <p>Counter: {count}</p>
            </form>
        );
    }
}
```

Componente hijo (permisos):

```js
class Permissions extends React.Component {
    render() {
        console.log("Render Permissions")
        const { canEdit } = this.props;
 
        return (
            <form>
                <p>El usuario {canEdit ? "" : "no"} tiene permisos de editar...</p>
            </form>
        );
    }
}
```

Si pruebas este código en el navegador, podrás darte cuenta de que, sin importar en qué botón demos clic, todos los componentes se vuelven a renderizar.

![React PureComponent](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-09%20at%2010.06.48%20PM-69fa2774-b1d5-4f4e-915c-29558de043d2.jpg)

Este error puede ser muy grave. La prop `canEdit` no tiene ninguna conexión con el componente `Counter` ni la prop `count` con el componente `Permissions`, pero, aún así, si cualquiera de las dos cambia, los 3 componentes se vuelven a renderizar.

Afortunadamente podemos arreglarlo/optimizarlo cambiando `React.Component` por `React.PureComponent`.

```js
class App extends React.PureComponent { /* … */ }
class Counter extends React.PureComponent { /* … */ }
class Permissions extends React.PureComponent { /* … */ }
```

![React.PureComponent](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-09%20at%2010.12.42%20PM-61fb80b7-91a8-4dfb-ad64-1fa9d9829cda.jpg)

#### ¿Cómo funciona y cuándo debo usar React.memo?

Si `useEffect` es el “reemplazo” del ciclo de vida en componentes creados como funciones con React Hooks, `React.memo` es el “reemplazo” de PureComponent.

Convirtamos el ejemplo anterior a funciones con React Hooks:

```js
const App = function() {
  console.log("Render App");

  const [count, setCount] = React.useState(1);
  const [canEdit, setCanEdit] = React.useState(true);

  const countPlusPlus = () => {
    console.log("Click al botón de counter");
    setCount(count + 1);
  };

  const toggleCanEdit = () => {
      console.log("Click al botón de toggleCanEdit");
      setCanEdit(!canEdit);
  };

  return (
    <>
      <button onClick={countPlusPlus}>Counter +1</button>
      <Counter count={count} />

      <button onClick={toggleCanEdit}>Toggle Can Edit</button>
      <Permissions canEdit={canEdit} />
    </>
  );
}

const Permissions = function({ canEdit }) {
  console.log("Render Permissions")

  return (
      <form>
          <p>Can Edit es {canEdit ? "verdadero" : "falso"}</p>
      </form>
  );
}

const Counter = function({ count }) {
  console.log("Render Counter")

  return (
      <form>
          <p>Counter: {count}</p>
      </form>
  );
}
```

El resultado va a ser exactamente igual que al usar `React.Component`.

![React PureComponent](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-09%20at%2010.06.48%20PM-69fa2774-b1d5-4f4e-915c-29558de043d2.jpg)

Ahora usemos `React.memo` para que nuestro componente no se renderice si las props que recibe siguen igual que en el render anterior.

```js
const App = React.memo(function() {
    /* … */
});

const Permissions = React.memo(function({ canEdit }) {
    /* … */
});

const Counter = React.memo(function({ count }) {
    /* … */
});
```

![React.PureComponent](https://static.platzi.com/media/user_upload/Screen%20Shot%202020-11-09%20at%2010.12.42%20PM-61fb80b7-91a8-4dfb-ad64-1fa9d9829cda.jpg)

#### ¿Cómo crear una comparación personalizada con React.memo o shouldComponentUpdate?

En algunos casos puede que no necesitemos shallow comparison, sino una comparación o validación personalizada. En estos casos lo único que debemos hacer es reescribir el método `shouldComponentUpdate` o enviar un segundo argumento a `React.memo` (casi siempre incluimos los keywords are equal al nombre de esta función).

Esta nueva comparación la necesitaremos, por ejemplo, cuando nuestro componente recibe varias props, pero solo necesita su valor inicial, es decir, sin importar si cambian, a nuestro componente le da igual y solo utilizará la primera versión de las props.

```jsx
// Con clases
class Permissions extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return false;
    }

    render() {
        /* … */
    }
}

// Con hooks
function memoStopIfPropsAreEqualOrNot(oldProps, newProps) {
  return true;
}

const Permissions = React.memo(function({ canEdit }) {
    /* … */
}, memoStopIfPropsAreEqualOrNot);
```

En este caso evitamos que nuestro componente se actualice sin importar si cambian nuestras props. Pero ¿qué tal si sí debemos volver a renderizar cuando cambia alguna de nuestras props?

```jsx
// Con clases
class Permissions extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.input.value !== nextProps.input.value) {
            return true;
        } else {
            return false;
        }
    }
}

// Con hooks
function memoIsInputEqual(oldProps, newProps) {
    if (oldProps.input.value !== newProps.input.value) {
        return false;
    } else {
        return true;
    }
}

const Permissions = React.memo(function({ canEdit }) {
    /* … */
}, memoIsInputEqual);
```

Recuerda que la función `shouldComponentUpdate` debe devolver true si queremos que nuestro componente se vuelva a renderizar. En cambio, la función de evaluación de `React.memo` debe devolver false si nuestras props son diferentes y, por ende, queremos permitir un nuevo render.

------

Ahora que conoces los casos de uso para React.memo y React.PureComponent para evitar renders innecesarios de tus componentes en React… **¿En qué piensas cuando debes “optimizar un componente” en tu aplicación con React.js?**Custom hooks: abstracción en la lógica de tus componentes

## Custom hooks: abstracción en la lógica de tus componentes

`hook/useCharacters.js`

```jsx
import { useState, useEffect } from 'react';

const useCharacters = url => {
  const [characters, setCharacters] = useState([]);
  useEffect(() => {
    fetch(url)
    .then(response => response.json())
    .then(data => setCharacters(data.results))
  }, [url])
  return characters;
};

export default useCharacters;

```

`components/Characters.jsx`

```jsx
import useCharacters from '../hook/useCharacters';

const API = 'https://rickandmortyapi.com/api/character/';

const characters = useCharacters(API)

```

## Third Party Custom Hooks de Redux y React Router

Los React Hooks cambiaron tanto la forma de hacer nuestro código para crear aplicaciones que otras herramientas también han creado sus propios custom hooks, de forma que podemos usarlos para que nuestro código sea más legible y fácil de mantener.

## [React Redux](https://react-redux.js.org/)

Seguramente conoces [react-redux](https://react-redux.js.org/), aquí podrás encontrar dos custom hooks que son muy útiles al momento de usar esta biblioteca: `useSelector` y `useDispatcher`. Estos los encontrarás a partir de la versión 7.1.0 de la biblioteca y a continuación te explicaré para qué sirven:

- **`useSelector`**: nos permite elegir de qué contenido en nuestro estado queremos leer información para usarla en nuestro componente.

```jsx
// Primero debemos importar el hook desde react-redux
import { useSelector } from 'react-redux';

// El hook recibe una función y aquí indicamos qué parte del estado queremos
const myProperty= useSelector(state => state.myProperty);
```

- **`useDispatcher`**: nos permite ejecutar las acciones hacia nuestro estado.

```jsx
// Importamos el hook
import { useDispatcher} from 'react-redux';

// Creamos una variable donde vivirá nuestro dispatcher
const dispatcher = useDispatcher();

// Ahora solo debemos pasarle la información de la acción que se ejecutará en nuestro reducer
dispatcher({ type: actionType, payload });
```

> Si quieres aprender a crear un sencillo contador de clics, pero usando esta configuración de hooks y toda la configuración de Redux en React, te recomiendo seguir este tutorial: [Redux es fácil si usas React Hooks](https://platzi.com/tutoriales/2118-react-hooks/10726-redux-es-facil-si-usas-los-hooks-2/).

## [React Router](https://reactrouter.com/)

React Router también contiene diferentes custom hooks para acceder a varias funcionalidades e información de la navegación del usuario en nuestra aplicación.

- **`useHistory`**: nos permite acceder a los métodos de navegación para movernos a través de ella de la forma que lo veamos más conveniente. Por ejemplo:

```jsx
import { useHistory } from 'react-router-dom';
let history = useHistory();
history.push('/home');
```

- **`useLocation`**: nos permite acceder a la información de la URL actual en la que se encuentran nuestros usuarios.

```jsx
import { useLocation } from 'react-router-dom';
let location = useLocation();
console.log(location.pathname);
```

- **`useParams`**: nos permite acceder a un objeto con la información de los parámetros que tendremos en la ruta que estamos navegando, por ejemplo, el slug de un blogpost.

```jsx
import { useParams } from 'react-router-dom';
let { slug } = useParams();
console.log(slug);
```

- **`useRouteMatch`**: funciona al igual que los componentes `<Route>`, pero este hook también nos permitirá saber si existe algún match adicional que podremos usar para mostrar o no otra información en la misma vista.

```jsx
import { useRouteMatch } from 'react-router-dom';
let match = useRouteMatch('/blog/:slug');

return (
	<div>
		<h1>Hello World</h1>
		{match && <p>Route matches</p>}
	</div>
)
```

## Conclusión

Ahora sabes cómo utilizar los React Hooks de ambas bibliotecas. Podrás darte cuenta de que usarlas nos facilita bastante el desarrollo con React porque, además de escribir menos código, lo hace más legible y rápido de leer.

Te recomiendo seguir investigando sobre este tema. También te invito a compartir en la sección de comentarios si las bibliotecas que más te gustan en React cuentan con la implementación de sus funcionalidades en React Hooks.

# 3. Configura un entorno de desarrollo profesional

## Proyecto: análisis y retos de Platzi Conf Store

## Instalación de Webpack y Babel: presets, plugins y loaders

## Configuración de Webpack 5 y webpack-dev-server

## Configuración de Webpack 5 con loaders y estilos

## Loaders de Webpack para Preprocesadores CSS

## Flujo de desarrollo seguro y consistente con ESLint y Prettier

## Git Hooks con Husky

# 4. Estructura y creación de componentes para Platzi Conf Store

## Arquitectura de vistas y componentes con React Router DOM

## Maquetación y estilos del home

## Maquetación y estilos de la lista de productos

## Maquetación y estilos del formulario de checkout

## Maquetación y estilos de la información del usuario

## Maquetación y estilos del flujo de pago

## Integración de íconos y conexión con React Router

# 4. Integración de React Hooks en Platzi Conf Merch

## Creando nuestro primer custom hook

## Implementando useContext en Platzi Conf Merch

## useContext en la página de checkout

## useRef en la página de checkout

## Integrando third party custom hooks en Platzi Conf Merch

# 5. Configura mapas y pagos con PayPal y Google Maps

## Paso a paso para conectar tu aplicación con la API de PayPal

## Integración de pagos con la API de PayPal

## Completando la integración de pagos con la API de PayPal

## Paso a paso para conectar tu aplicación con la API de Google Maps

## Integración de Google Maps en el mapa de checkout

## Creando un Custom Hook para Google Maps

# 6. Estrategias de deployment profesional

## Continuous integration y continuous delivery con GitHub Actions

## Compra del dominio y despliega con Cloudflare

# 7. Optimización de aplicaciones web con React

## Integración de React Helmet para mejorar el SEO con meta etiquetas

## Análisis de performance con Google Lighthouse

## Convierte tu aplicación de React en PWA

# 8. Bonus: trabaja con Strapi CMS para crear tu propia API

## Crea una API con Strapi CMS y consúmela con React.js

# 9. ¿Qué sigue en tu carrera profesional?

## Próximos pasos para especializarte en