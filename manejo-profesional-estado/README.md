<h1>React.js Menjo Profesional del Estado</h1>

<h3>Juan David Castro</h3>

<h2>Table of Content</h2>

- [1. Introducción](#1-introducción)
  - [Dime cómo manejas el estado y te diré...](#dime-cómo-manejas-el-estado-y-te-diré)
- [2. Estado y ciclo de vida con React.Component](#2-estado-y-ciclo-de-vida-con-reactcomponent)
  - [Nuevo proyecto: códigos de seguridad](#nuevo-proyecto-códigos-de-seguridad)
  - [Estados simples: React.Component vs. useState](#estados-simples-reactcomponent-vs-usestate)
  - [Efectos con useEffect](#efectos-con-useeffect)
  - [Métodos del ciclo de vida en React.Component](#métodos-del-ciclo-de-vida-en-reactcomponent)
- [3. Estados independientes y compuestos](#3-estados-independientes-y-compuestos)
  - [Estados independientes con useState](#estados-independientes-con-usestate)
  - [¿Dónde actualizar el estado?](#dónde-actualizar-el-estado)
  - [Estados compuestos con React.Component](#estados-compuestos-con-reactcomponent)
  - [Estados compuestos con useState](#estados-compuestos-con-usestate)
- [4. Código imperativo y declarativo en React](#4-código-imperativo-y-declarativo-en-react)
  - [Estados imperativos con useState](#estados-imperativos-con-usestate)
  - [Estados semideclarativos con useState](#estados-semideclarativos-con-usestate)
  - [¿Qué es un reducer?](#qué-es-un-reducer)
  - [3 formas de crear un reducer](#3-formas-de-crear-un-reducer)
  - [Estados declarativos con useReducer](#estados-declarativos-con-usereducer)
  - [Action creators y actionTypes](#action-creators-y-actiontypes)
- [5. Manejo del estado en TODO Machine](#5-manejo-del-estado-en-todo-machine)
  - [¿Qué son los estados derivados?](#qué-son-los-estados-derivados)
  - [Migración de useState a useReducer en useLocalStorage](#migración-de-usestate-a-usereducer-en-uselocalstorage)
  - [Imponer tu visión vs. trabajar en equipo](#imponer-tu-visión-vs-trabajar-en-equipo)
- [6. Próximos pasos](#6-próximos-pasos)
  - [¿Quieres más cursos de React.js?](#quieres-más-cursos-de-reactjs)

# 1. Introducción

## Dime cómo manejas el estado y te diré...

Aprenderemos manejo de estados a profundidad.

1. User React.useState es lo primero que podemos hacer con el manejo de estado.
2. Otras maneras de trabajar con el estado:

- Uso de clases con React.component
- Usar funciones con React Hook

1. Creación de estados simples o independientes y complejos o compuestos.
   Podemos manejar el estado de forma imperativa o declarativa.
2. Combinar estas opciones nos permite obtener la mejor solución a un problema específico.
3. Hemos trabajado: estados simples e independientes utilizando React.useState de forma imperativa.
4. Es importante conocer estas formas de trabajo:

- Cuando nos las encontremos con estas podemos trabajarlas
- Saber cuando es la mejor herramienta en cada caso

# 2. Estado y ciclo de vida con React.Component

## Nuevo proyecto: códigos de seguridad

**Pasos a seguir:**

- Ingresamos a la consola de nuestro VSC
- Escribimos : npx create-react-app manejo-estado
- Luego escribimos : cd manejo de estados
- Después: code -r ./
- Listo para escribir código

**Proyecto códigos de seguridad**

- Para realizar acción de eliminar un elemento nos pedirá un código de seguridad.
- Si ingresamos uno incorrecto no realiza
- Si ingresamos un código correcto pasamos al estado de confirmación.
- Si le damos no, volvemos al estado anterior.
- Si le damos si, se realiza la acción.

**Conforme avancemos el proyecto:**

- Entenderemos mejor como funcionaban las clases con React.component
- Conoceremos diferencias con los componentes en funciones utilizando React Hooks
- Conoceremos que paradigmas no daban los estados de las clases comparado con los estados de las funciones.
- Aprovecharemos características que se usaban en las clases pero no en las funciones las cuales son herramientas poderosas.
- Estos son los estados complejos o compuestos y los estados declarativos. (useReducer)

Ahora con la versión 18 de React ya no usamos ReactDOM.render sino **createRoot **

```jsx
import App from './App';
import { createRoot } from 'react-dom/client';
const container = document.getElementById('root');
const root = createRoot(container); 
root.render(<App />);
```

[Documentacion](https://reactjs.org/blog/2022/03/08/react-18-upgrade-guide.html#updates-to-client-rendering-apis)

Es una herramienta para destacar problemas potenciales en la aplicación. Al igual que Fragment, StrictMode no renderiza nada en la interfaz de usuario. Este modo también activa advertencias y comprobaciones adicionales para sus descendientes.

StrictMode en la actualidad ayuda a:

Identificar ciclos de vida inseguros
Advertencia sobre el uso de la API legado de string ref
Advertencia sobre el uso del método obsoleto findDOMNode
Detectar efectos secundarios inesperados
Detectar el uso de la API legado para el contexto

Modifica el archivo App.js por el siguiente código:

```jsx
import React from 'react'
import './App.css'
import UseState from './UseState'
import UseClass from './ClassState'

function App() {
  return (
    <div className='App'>
      <UseState/>
      <UseClass/>
    </div>
  )
}

export default App;
```

Luego crea el archivo UseState.js y escribe el siguiente código:

```jsx
import React from 'react'

export default function UseState() {
  return (
    <div>
      <h2>Eliminar UseState</h2>
      <p>Por favor, escriba el código de seguridad.</p>
      <input type='text' placeholder='código de seguridad'/>
      <button>Comprobar</button>
    </div>
  )
}
```

Por último crea el archivo ClassState y escribe el siguiente código:

```jsx
import React, { Component } from 'react'

export default class ClassState extends Component {
  render() {
    return (
      <div>
        <h2>Eliminar ClassState</h2>
        <p>Por favor, escriba el código de seguridad.</p>
        <input type='text' placeholder='código de seguridad'/>
        <button>Comprobar</button>
      </div>
    )
  }
}
```

Corre la aplicación con **npm start** o **yarn start**
![appStates.jpg](https://static.platzi.com/media/user_upload/appStates-f51afbfc-64e3-4bbc-a707-bf7b98c6c3bd.jpg)

[Documentación oficial](https://es.reactjs.org/docs/strict-mode.html)

## Estados simples: React.Component vs. useState

El estado en los class components tambien se puede declarar de esta forma, omitiendo el metodo contructor.

![classState.png](https://static.platzi.com/media/user_upload/classState-df32b39b-9a03-4398-bca0-ae897764fba0.jpg)

Conoceremos las diferencias entre los componentes creados con clases utilizando React.component y los componentes creados con funciones utilizanod React Hook en el manejo de las propiedades y el estado.

**Envio de propiedades (props)**
Se envia de la misma forma, sea el componente creado con clase o función:

```jsx
import { UseState } from './UseState';
import { ClassState } from './ClassState';
import './App.css';

function App() {
  return (
    <div className="App">
      <UseState name="Use State" />
      <ClassState name="Class State" />
    </div>
  );
}

export default App;
```

**Manejo de propiedades en funciones**
Para el manejo de propiedades debemos recibirlo como parámetro props el cual puede ser destructurado en las propiedades enviadas

```jsx
function UseState({ name }) {
    const [error, setError] = React.useState(true);

    return (
        <div>
            <h2>Eliminar {name}</h2>
            <p>Por favor, escriba el código de seguridad.</p>

            {error && (
                <p>El código es es incorrecto</p>
            )}

            <input type='text' placeholder='código de seguridad'/>
            <button>Comprobar</button>
        </div>
    );
}
```

**Manejo de propiedades en clases**
Para el manejo de propiedades escribimos this.props para acceder a las propiedades de nuestro elemento

```jsx
class ClassState extends React.Component {
    render () {
        return (
            <div>
                <h2>Eliminar {this.props.name}</h2>
                <p>Por favor, escriba el código de seguridad.</p>
                <input type='text' placeholder='código de seguridad'/>
                <button>Comprobar</button>
            </div>
        );
    }
}
```

**Manejo de estado en funciones**
Utilizamos la siguiente sintaxis para declarar e inicializarr un estado y su modificador de valor.

```jsx
function UseState({ name }) {
    // estado error
    const [error, setError] = React.useState(true);

    return (
        <div>
            <h2>Eliminar {name}</h2>
            <p>Por favor, escriba el código de seguridad.</p>

            {error && (
                <p>El código es es incorrecto</p>
            )}

            <input type='text' placeholder='código de seguridad'/>
            <button
                // onClick={()=>setError(!error)}
                onClick={()=>setError(prevState=>!prevState)}
            >Comprobar</button>
        </div>
    );
}

export { UseState }
```

**Manejo de estado en clases**

- En el método constructor utilizamos this.state para definir un objeto cuyas propiedades serán los estados
- Para poder modificar this y conservar lo que vivía en this de la clase que extendemos se debe llamar a super() dentro del método constructor.
- Luego debemos recibir las props desde el constructor y enviarle a super todas las propiedades que recibimos, de esta forma no solo vivirán en el constructor de nuestra clase sino también pasar a la clase React.component
- Una propiedad que viene de React.component es this.setState, con esta modificaremos los estados.

```jsx
class ClassState extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error:false,
        }
    }

    render () {
        return (
            <div>
                <h2>Eliminar {this.props.name}</h2>
                <p>Por favor, escriba el código de seguridad.</p>

                {this.state.error && (
                    <p>El código es es incorrecto</p>
                )}

                <input type='text' placeholder='código de seguridad'/>
                <button
                    // onClick={()=>this.setState({ error: !this.state.error})}
                    onClick={()=>this.setState(prevState => ({error: !prevState.error}))}
                >Comprobar</button>
            </div>
        );
    }
}
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)Clase 02 en orden de grabación · platzi/curso-react-estado-1@d524e10 · GitHub](https://github.com/platzi/curso-react-estado-1/commit/d524e10df05819fe9bfce7cd165f552ea01b27d5)

## Efectos con useEffect

1. El primero, siempre se usa y es una función a ejecutar.

2. El segundo, opcional e importante, nos indica cuando se va a ejecutar nuestro primer parámetro. Los posibles valores de este parámetro son:

   1. Ningun valor:
      esta función se ejecutará cada vez que nuestro componente haga render (es decir, cada vez que haya cambios en cualquiera de nuestros estados).
   2. Arreglo vacio:
      nuestro efecto solo se ejecuta una vez, cuando recién hacemos el primer render de nuestro componente.
   3. Arreglo no vacio:
      O también podemos enviar un array con distintos elementos para decirle a nuestro efecto que no solo ejecute en el primer render, sino también cuando haya cambios en esos elementos del array.

   - Ahora trabajaremos con los efectos en funciones y con su equivalente en React.component que son los métodos de ciclo de vida.
   - En esta clase nos enfocaremos en los efectos del componente UseState

   **Situacion**

   - Queremos que después de 2 segundos que nuestro estado loading cambie a true por el botón vuelva a ser false.
   - No podemos simplemente escribir if(loading) setLoading(false) pues el código se va a ejecutar siempre que React haga un render del componente.
   - El render del componente se va activar con el cambio de cualquier cambio de estado.
   - Cuando se vuelve a renderizar, los estados vuelven a ser creados pero no serán el valor inicial sino el ultimo estado definido con los actualizadores

   **Solución**
   Para poder ejecutar nuestro código no en cada render sino bajo ciertas condiciones utilizamos el React.useEffects

`UseState.jsx`

```jsx
import React from "react";

function UseState({ name }) {
  const [error, setError] = React.useState(true);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    console.log("Starting the effect");

    if(!!loading) {
      setTimeout(() => {
        console.log("Doing the validation");

        setLoading(false);

        console.log("Finishing the validation");
      }, 3000);
    }

    console.log("Finishing the effect");
  }, [loading]);

  return (
    <div>
      <h2>Delete {name}</h2>

      <p>Please enter the security code</p>

      {error && (
        <p>Error: Security code is incorrect</p>
      )}
      {loading && (
        <p>Loading...</p>
      )}

      <input placeholder="Security Code" />

      <button
        onClick={() => setLoading(true)}
      >Check</button>
    </div>
  );
}
```

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)useEffect vs. useLayoutEffect - Platzi](https://platzi.com/blog/useeffect-uselayouteffect/)

## Métodos del ciclo de vida en React.Component

Para complementar esta clase y por qué hay métodos que se dejaron de usar?
.
Antes React utilizaba muchos más metodos para actualizar el estado (siguiente imagen) pero en algun momento a lo mejor se dieron cuenta que era muy complicado porque introducía mucha mas complejidad, codigo repetido y causaba mucha confusión debido a que habían muchos metodos similares, dejo el ciclo de vida con todos los métodos utilizados anetiormente a continuación:

![React-Components-Lifecycle.png](https://static.platzi.com/media/user_upload/React-Components-Lifecycle-2882e1ad-88de-4862-8f2b-1b75c1b778ce.jpg)

.

Ahora este ciclo de vida se ha reducido y ya no hay tantos pasos para hacer una actualización de estado, por lo que es sencillo entender mas fácilmente cómo se actualiza React, ya no hay tantos pasos intermedios y ya no tendríamos que escribir tanto código si siguieramos utilizando las clases para escribir código. Ya que se han marcado que ya no se deberían usar algunos métodos. Este es el ciclo de vida que se usa ahora:

![ogimage.png](https://static.platzi.com/media/user_upload/ogimage-a7c994a6-26e0-4a63-a32f-72fff4b70ecd.jpg)
.
Ahora bien `useEffect()` recoge lo que hace `componentDidMount()`, `componentDidUpdate()`, y `componentWillUnmount()`, es decir, que `useEffect()` se encarga de ejecutar un función cuando nuestro componente se va a mostrar ya sea por una actualización o sencillamente porque es la primera vez que se va a mostrar. Si nos fijamos bien `componentWillUnmount()` es la encargada de eliminar el componente de nuestra interfaz de usuario pues bien con `useEffect()` tambien se puede utilizar para ejecutar una función una vez nuestro componente se vaya a eliminar esto es útli para: eliminar eventos, hacer una petición al servidor para guardar algo o limpiar algun temporizador. Aunque no se si sea la historia oficial de React 😛 imagino que eso fue lo que pensaron los ingenieros.

Pues bien para hacer una comparación de lo que trabajar con clases y useEffect tenemos tres opciones:

`componentDidMount()` Ejecutar algo una vez el componente se inicializa:

```jsx
useEffect( () => {
  // Algo que se ejecuta dentro
}, []); //Los corchetes para indicar que se ejecute solo la primera vez
```

`componentDidUpdate()` Ejecutar algo en alguna actualización del estado:

```jsx
useEffect( () => {
  // Algo que se ejecuta dentro
}, [dependencia]); //Se ejecuta cada vez que la dependencia cambia
```

`componentWillUnmount()` Ejecuta algo cuando el componente se va a dejar de mostrar

```jsx
useEffect( () => {
 window.addEventListener('mousemove', () => {});

 // Una función que se ejecuta despues de que el componente se elimine
 return () => {
   window.removeEventListener('mousemove', () => {})  //Elimina el evento
 }
}, []); 
```


Por último añadir que no se debería usar useEffect sin el arreglo de dependencias (el que se pone al final). Ya que estaríamos ejecutando todo lo que hay dentro en cada nuevo render por lo que agregaríamos calculos que a lo mejor nos podríamos ahorrar.

*Eso fue todo, espero que te haya gustado* **Nunca pares de aprender** 💚

Comparar el **useEffect** hook con los **Lifecycle de los class components** fue la mejor manera para por fin entender este hook. Excelente clase 😄

Les comparto este recurso que me ayudo mucho para complementar mas el contenido de esta clase: [Replacing Lifecycle methods with React Hooks](https://javascript.plainenglish.io/lifecycle-methods-substitute-with-react-hooks-b173073052a)

![img](https://miro.medium.com/max/1400/1*bsk4y_rRxmX_Qtol3H3caw.png)

![img](https://i.stack.imgur.com/usQ9s.jpg)

```jsx
import React from "react";
import { Loading } from "./Loading";

class ClassState extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: true,
      loading: false,
    };
  }

  UNSAFE_componentWillMount() {
    console.log("UNSAFE_componentWillMount");
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  componentDidUpdate() {
    console.log("Update");

    if(!!this.state.loading) {
      setTimeout(() => {
        console.log("Doing the validation");

        this.setState( { loading: false } );

        console.log("Finishing the validation");
      }, 3000);
    }
  }

  render() {
    return (
      <div>
        <h2>Delete {this.props.name}</h2>

        <p>Please enter the security code</p>

        {this.state.error && (
          <p>Error: Security code is incorrect</p>
        )}

        {this.state.loading && (
          <Loading />
        )}

        <input placeholder="Security Code" />

        <button
          onClick={() => this.setState({ loading: true })}
        >Check</button>
      </div>
    );
  }
}

export {ClassState};
```

```jsx
import React from "react";

class Loading extends React.Component {
  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  render() {
    return (
      <p>Loading...</p>
    );
  }
}

export { Loading };
```

[![img](https://www.google.com/s2/favicons?domain=https://www.youtube.com/s/desktop/b75d77f8/img/favicon.ico)Qué es y cómo funciona un componente en React | Platzi Cursos - YouTube](https://www.youtube.com/watch?v=2dND8evRRls)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Ciclo de vida de un componente de React.js](https://platzi.com/blog/ciclo-de-vida-de-un-componente-de-reactjs/)

# 3. Estados independientes y compuestos

## Estados independientes con useState

Tambien podriamos agregar un setError(false), cuando se da click en el botón comprobar…

```jsx
<button
    onClick={() => {
      setLoading(true);
             setError(false);
        }} >
        Check
      </button>
```

**Reto de la clase**
 manteniendo el mismo código, solo agregando un else con setError(false)

```jsx
useEffect(
() => {
 if (loading) {
  setTimeout(() => {
   if (value !== SECURITY_CODE) {
    setError(true)
   }else{
    setError(false)
   }
   setLoading(false)
  }, 1000)
 }
}, [ loading ] )
```

También se le pudiera poner un setTimeout al setError para quitarlo.  La otra forma que se me ocurre es que en el evento onChange al  ejecutarse, poner setError(false), así escribes en el input y nunca  tendrás un error mostrándose 😃

![img](https://i.imgur.com/jgI6Jcq.gif)

## ¿Dónde actualizar el estado?

`UseState.js`

```javascript
useEffect(() => {
        console.log("Empezando el efecto");
        if (loading) {
            setError(false)
            setTimeout(() => {
                console.log("validando");
                if (value !== SECURITY_CODE) {
                    setError(true);
                }
                setLoading(false);
                console.log("Terminando de validar");
            }, 3000);
        }
        console.log("terminando el efecto");
    }, [loading]);
```

## Estados compuestos con React.Component

```javascript
 componentDidUpdate() {
        console.log("Actualizacion");

        if (this.state.loading) {
            setTimeout(() => {
                console.log("Haciendo la validacion");

                this.setState({ loading: false, error: SECURITY_CODE !== this.state.value });

                console.log("Terminando la validacion");
            }, 3000);
        }
    }
```

## Estados compuestos con useState

Estado simple con useState:

```jsx
// Así se declaran
const [ value, setValue ] = React.useState('');
const [ error, setError ] = React.useState(false);
const [ loading, setLoading ] = React.useState(false);
 
// Así se consumen
console.log(loading);

// Así se actualizan
setLoading(true);
```

Estado compuesto con useState:

```jsx
// Así se declaran
const [ state, setState ] = React.useState({
    value: '',
    error: false,
    loading: false,
});
 
// Así se consumen
console.log(state.loading);

// Así se actualizan
setState({
   ...state,
   error: true,
   loading: false,
})
```

Estado compuesto con classState (no hay estados simples):

```jsx
// Así se declaran
constructor(props) {
   super(props);
 
   this.state = {
       value: '',
       error: false,
       loading: false,
   }
}
 
// Así se consumen
console.log(this.state.loading);

// Así se actualizan
this.setState({ error: true, loading: false });
```

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Why you should consider using Objects more in React/Redux | by Hanna Söderström | Medium](https://medium.com/@hanna.soderstrom84/why-you-should-consider-using-objects-more-in-react-redux-96d505d63095)

# 4. Código imperativo y declarativo en React

## Estados imperativos con useState

**Paradigma**
 Paradigma de programación son la forma que traducimos lo que pensamos al código que vamos a escribir.

 **Paradigma imperativo:**
 Describir el paso a paso de lo que vamos a hacer en el código.

 **Paradigma declarativo**
 Cuanto mas declarativo, menos se concentra en el paso a paso. Eso se vera en otra función.

```jsx
Código usado durante la clase:

function UseState({ name }) {
    const [state, setState] = React.useState({
        value:'',
        error:false,
        loading:false,
        deleted: false,
        confirmed: false,
    })

    console.log(state);

    const handleChange = (event)=>{
        setState({ 
            ...state,
            value: event.target.value,
        });
        console.log(event.target.value);
    }

    React.useEffect(()=>{
        console.log('Empezando el efecto');
        if(state.loading){
            // setError(false);
            setTimeout(()=>{
                console.log("Haciendo la validación xd");
                if(state.value === SECURITY_CODE){
                    setState({ 
                        ...state,
                        error: false, 
                        loading: false ,
                        confirmed: true,
                    });
                }else{
                    setState({ 
                        ...state,
                        error: true, 
                        loading: false 
                    });
                }
                console.log("Terminando la validación");
            },1500);
        }
        console.log('Terminando el efecto');
    },[state.loading]);

    if(!state.deleted && !state.confirmed){
        return (
            <div>
                <h2>Eliminar {name}</h2>
                <p>Por favor, escriba el código de seguridad.</p>
    
                {(state.error && !state.loading ) && (
                    <p>El código es es incorrecto</p>
                )}
    
                {state.loading && (
                    <p>Cargando ...</p>
                )}
    
                <input 
                    type='text' 
                    placeholder='código de seguridad'
                    value={state.value}
                    onChange={handleChange}
                />
                <button
                    // onClick={()=>setError(!error)}
                    onClick={()=>{
                        // setError(false);
                        setState({ 
                            ...state,
                            loading: true 
                        });
                    }}
                >Comprobar</button>
            </div>
        );
    }else if(state.confirmed && !state.deleted){
        return(
            <React.Fragment>
                <p>¿Seguro que quieres eliminar UseState?</p>
                <button
                    onClick={()=>{
                        setState({
                            ...state,
                            deleted: true,
                        })
                    }}
                >Si, eliminar</button>
                <button
                    onClick={()=>{
                        setState({
                            ...state,
                            confirmed: false,
                            value:'',
                        })
                    }}
                >No, volver</button>
            </React.Fragment>
        )
    } else {
        return (
            <React.Fragment>
                <p>Eliminado con exito</p>
                <button
                    onClick={()=>{
                        setState({
                            ...state,
                            confirmed: false,
                            deleted: false,
                            value:'',
                        })
                    }}
                >Recuperar UseState</button>
            </React.Fragment>
        )
    }
}
```

## Estados semideclarativos con useState

Ejemplo de estado imperativo:

```jsx
<button
   onClick={() =>
       setState({
           ...state,
           loading: true
       })
   }>Comprobar</button>
```

Ejemplo de estado declarativo:

```jsx
const onCheck = () => {
   setState({
       ...state,
       loading: true
   })
}
 
...
<button
   onClick={() =>
       onCheck()
   }>Comprobar</button>
```

code

```jsx
import React from 'react';

const SECURITY_CODE = 'hola';

function UseState({ name }) {
  const [state, setState] = React.useState({
    value: '',
    error: false,
    loading: false,
    deleted: false,
    confirmed: false,
  });

  const onConfirm = () => {
    setState({
      ...state,
      error: false,
      loading: false,
      confirmed: true,
    });
  };

  const onError = () => {
    setState({
      ...state,
      error: true,
      loading: false,
    });
  }

  const onWrite = (event) => {
    setState({ 
      ...state, 
      value: event.target.value, 
    });
  };

  const onCheck = () => {
    setState({ 
      ...state, 
      loading: true, 
    });
  };

  const onDelete = () => {
    setState({
      ...state,
      deleted: true,
    });
  };

  const onReset = () => {
    setState({
      ...state,
      confirmed: false,
      deleted: false,
      value: '',
    });
  };

  console.log(state);
  // const [value, setValue] = React.useState('');
  // const [error, setError] = React.useState(false);
  // const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    console.log('Empezando el efecto');

    if (!!state.loading) {
      setTimeout(() => {
        console.log('incio de validacion');

        if (state.value === SECURITY_CODE) {
          onConfirm();
        } else {        // setLoading(false);
          onError();
        }
        console.log('Terminando la validacion');
      }, 3000);
    }

    console.log('Finalizado el efecto');
  }, [state.loading]);

  if (!state.deleted && !state.confirmed) {
    return (
      <div>
        <h2>Eliminar {name}</h2>

        <p>Por favor, escribe el codigo de seguridad.</p>

        {(state.error && !state.loading) && (
        <p>Error: el codigo es incorrecto</p>
        )}

        {state.loading && (
        <p>Cargando...</p>
        )}

        <input 
          placeholder="Codigo de seguridad"
          value={state.value}
          onChange={(event) => {
            onWrite(event.target.value);
          }}
        />
        <button 
          onClick={() => { 
            // setError(false);    // este fue
            onCheck();
          }}
        >Comprobar</button>
      </div>
    );
  } else if(!!state.confirmed && !state.deleted) {
    return (
      <React.Fragment>
        <p>Pedimos confirmacion. Seguro?</p>
        <button
          onClick={() => {
            onDelete();
          }}
        >
        Si
        </button>
        <button
          onClick={() => {
            onReset();
          }}
        >
          No
        </button>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <p>Elimando con exito</p>
        <button
          onClick={() => {
            onReset();
          }}
        >
          Resetear, volver atras
        </button>
      </React.Fragment>
    )
  }
}

export { UseState };
```

## ¿Qué es un reducer?

**¿Qué es un reducer?**

- Son una herramienta que nos permite declarar todos los posibles estados de nuestra App para llamarlos de forma declarativa.
- Necesitan 2 objetos esenciales: los estados compuestos y las acciones.

**Los estados compuestos:**

- Son un objeto donde van a vivir como propiedades todos nuestros estados

**Acciones**

- Responsables, al ser disparados, de pasar de un estado a otro.
- Este objeto tiene 2 propiedades: action type y action payload.

**Action type:**

- Define el nombre clave para encontrar el nuevo estado.

**Action payload:**

- Es opcional e importante con estados dinámicos. Un estado es  dinamico cuando depende del llamado de un API, de lo escrito por el  usuario en un input, etc. Estos son estados dinámicos y los recibimos  con un payload.

**Flujo de trabajo:**

- Definimos distintos tipos de acciones con type y payload.
- Enviamos estas acciones a nuestro reducer.
- El reducer define los posibles estados por donde pasara nuestra App.
- Con action type elegimos cual de esos estados queremos disponer con el cambio o evento del usuario.
- Con action payload damos dinamismo a dicho estado. Será el mismo estado pero le daremos características especiales

> **¿Qué es un reducer?**
>  Es una herramienta que nos permite declarar todos los posibles estados  de la aplicación para que podamos usarlos de forma muy declarativa.
>
> Los reducers necesitan dos objetos, el primero es el estado compuesto o “compone state“ que son un objeto donde los estados son sus  propiedades y solo tenemos una función actualizadora para todos estos  estados. El otro objeto son las acciones, que son las que enviamos a  nuestro reducer para que podamos actualizar el valor de los estados,  estas acciones también son un objeto que tienen dos propiedades:
>
> Action Type: Qué le dice al reducer cuál es nombre clave para encontrar el estado que vamos a modificar.
>
> Action Payload: (Optional) se usa para los estados dinámicos, que son los estados que su nuevo valor depende de otro evento cómo puede ser el llamado a una API, de los que escriban los usuarios en un input, etc.
>
> El flujo sería algo cómo:
>
> Actions → Reducer → New State

## 3 formas de crear un reducer

> Los corchetes son estos:
>  [ ] => Los que utilizamos para los arreglos.
>  Y estas son las llaves:
>  { } => Las que utilizamos para los objetos.

**3 formas de crear un reducer**
 Usando condicionales if:

```jsx
const reducerIF = (state, action) => {
   if (action.type === 'ERROR') {
       return {
           ...state,
           error: true,
           loading: false,
       }
   } else if (action.type === 'CONFIRM') {
       return {
           ...state,
           error: false,
           loading: false,
           confirmed: true,
       }
   } else {
       return {
           ...state,
       }
   }
}
```

Usando switch case:

```jsx
const reducerSWITCH = (state, action) => {
   switch (action.type) {
       case 'ERROR':
           return {
               ...state,
               error: true,
               loading: false,
           };
       case 'CONFIRM':
           return {
               ...state,
               error: false,
               loading: false,
               confirmed: true,
           };
       default:
           return {
               ...state,
           };
   }
}
```

Usando objetos:

```jsx
const reducerOBJECT = (state) => ({
   'ERROR': {
       ...state,
       error: true,
       loading: false,
   },
   'CONFIRM': {
       ...state,
       error: false,
       loading: false,
       confirmed: true,
   },
})
 
const reducer = (state, action) => {
   if (reducerOBJECT(state)[action.type]) {
       return reducerOBJECT(state)[action.type];
   } else {
       return {
           ...state,
       }
   }
}
```

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Reducers in JavaScript. Complete Introduction to JS Reducers… | by Nadun Malinda | Enlear Academy](https://enlear.academy/reducers-in-javascript-f5317b34cba2)

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)Understanding Reducers from scratch | by Arseniy Tomkevich | Medium](https://medium.com/@jsmuster/understanding-reducers-d0f934aceccd)

[![img](https://www.google.com/s2/favicons?domain=https://www.robinwieruch.de/javascript-reducer/favicon-32x32.png?v=9db82c76a9aaf54925ac42d41f3d384c)What is a Reducer in JavaScript/React/Redux](https://www.robinwieruch.de/javascript-reducer)

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)One Cool Trick to Simplify Reducer Functions | by Eric Elliott | JavaScript Scene | Medium](https://medium.com/javascript-scene/one-cool-trick-to-simplify-reducer-functions-bbbffe488bb6)

## Estados declarativos con useReducer

**useReducer:**

- Es una herramientas de React, un React Hook.
- Nos permite usar los reducers para manejar nuestro estado de la forma mas declarativa posible.

Tendra la sintaxis:

```jsx
const [state, dispatch ] = React.useReducer(reducer, initialState);
```

Código del UseReducer.js de la clase, recuerda agregar el componente a  App.js para que se renderice. En este caso uso switch/case por se más  intuitivo de usar 😃

```jsx
import React, { useEffect, useReducer, Fragment } from 'react'

const SECURITY_CODE = 'paradigma'
const initialState = {
	value: '',
	loading: false,
	error: false,
	deleted: false,
	confirmed: false,
}

const reducer = (state, action) => {
	switch (action.type) {
		case 'Error':
			return {
				...state,
				error: true,
				loading: false,
			}
		case 'Confirm':
			return {
				...state,
				loading: false,
				error: false,
				confirmed: true,
			}
		case 'Write':
			return {
				...state,
				value: action.payload,
			}
		case 'Check':
			return {
				...state,
				loading: true,
				error: false,
			}
		case 'Delete':
			return {
				...state,
				deleted: true,
			}
		case 'Reset':
			return {
				...state,
				value: '',
				confirmed: false,
				deleted: false,
			}
		default:
			return {
				...state,
			}
	}
}

export default function UseReducer() {
	const [ state, dispatch ] = useReducer(reducer, initialState)

	useEffect(
		() => {
			if (state.loading) {
				setTimeout(() => {
					if (state.value === SECURITY_CODE) {
						dispatch({ type: 'Confirm' })
					} else {
						dispatch({ type: 'Error' })
					}
				}, 1000)
			}
		},
		[ state.loading ]
	)

	if (!state.deleted && !state.confirmed) {
		return (
			<div>
				<h2>Eliminar UseReducer</h2>
				<p>Por favor, escriba el código de seguridad.</p>
				{state.loading ? 'Cargando...' : state.error ? 'Error :(' : null}
				<br />
				<input
					type='text'
					placeholder='código de seguridad'
					value={state.value}
					onChange={ev => dispatch({ type: 'Write', payload: ev.target.value })}
				/>
				<button
					onClick={() => {
						dispatch({ type: 'Check' })
					}}
				>
					Comprobar
				</button>
			</div>
		)
	} else if (!state.deleted && state.confirmed) {
		return (
			<Fragment>
				<p>Pedimos confirmación. ¿Tas seguro?</p>
				<button onClick={() => dispatch({ type: 'Delete' })}>Si, eliminar</button>
				<button onClick={() => dispatch({ type: 'Reset' })}>No, me arrepentí</button>
			</Fragment>
		)
	} else {
		return (
			<Fragment>
				<p>Eliminado con éxito</p>
				<button onClick={() => dispatch({ type: 'Reset' })}>Regresar</button>
			</Fragment>
		)
	}
}
```

Código del UseReducer.js de la clase utilizando la tercera opción, la del objeto:

```jsx
import React from "react";

const SECURITY_CODE = 'paradigma';

function UseReducer({ name }) {
    const [state, dispatch ] = React.useReducer(reducer, initialState);
    
    React.useEffect(()=>{
        console.log('Empezando el efecto');
        if(state.loading){
            setTimeout(()=>{
                console.log("Haciendo la validación xd");
                if(state.value === SECURITY_CODE){
                    dispatch({
                        type: 'CONFIRM',
                    })
                }else{
                    dispatch({
                        type: 'ERROR'
                    });
                }
                console.log("Terminando la validación");
            },1500);
        }
        console.log('Terminando el efecto');
    },[state.loading]);


    if(!state.deleted && !state.confirmed){
        return (
            <div>
                <h2>Eliminar {name}</h2>
                <p>Por favor, escriba el código de seguridad.</p>
    
                {(state.error && !state.loading ) && (
                    <p>El código es es incorrecto</p>
                )}
    
                {state.loading && (
                    <p>Cargando ...</p>
                )}
    
                <input 
                    type='text' 
                    placeholder='código de seguridad'
                    value={state.value}
                    onChange={(event)=>{
                        dispatch({
                            type:'WRITE',
                            payload: event.target.value,
                        })
                        // onWrite(event);
                    }
                    }
                />
                <button
                    onClick={()=>{
                        dispatch({
                            type: 'CHECK'
                        });
                    }}
                >Comprobar</button>
            </div>
        );
    }else if(state.confirmed && !state.deleted){
        return(
            <React.Fragment>
                <p>¿Seguro que quieres eliminar UseState?</p>
                <button
                    onClick={()=>{
                        dispatch({
                            type: 'DELETE'
                        });
                    }}
                >Si, eliminar</button>
                <button
                    onClick={()=>{
                        dispatch({
                            type:'RESET'
                        });
                    }}
                >No, volver</button>
            </React.Fragment>
        )
    } else {
        return (
            <React.Fragment>
                <p>Eliminado con exito</p>
                <button
                    onClick={()=>{
                        dispatch({
                            type:'RESET'
                        });
                    }}
                >Recuperar UseState</button>
            </React.Fragment>
        )
    }
}

const initialState = {
    value:'',
    error:false,
    loading:false,
    deleted: false,
    confirmed: false,
}

const reducerObject = (state, payload) => ({
    'CONFIRM':{ 
        ...state,
        error: false, 
        loading: false ,
        confirmed: true,
    },
    'ERROR': { 
        ...state,
        error: true, 
        loading: false 
    },
    'WRITE':{ 
        ...state,
        value: payload,
    },
    'CHECK':{ 
        ...state,
        loading: true 
    },
    'DELETE':{
        ...state,
        deleted: true,
    },
    'RESET':{
        ...state,
        confirmed: false,
        deleted: false,
        value:'',
    }
})

 const reducer = (state, action) => {
    return (reducerObject(state, action.payload)[action.type] || state);
};

export { UseReducer }
```

## Action creators y actionTypes

**Action Types:** consiste en usar un objeto para almacenar las keys de nuestro reducer:

```js
const actionTypes = {
   error: 'ERROR',
   confirm: 'CONFIRM',
}
```

**Action Creators:** consiste en declarar funciones que contienen nuestros dispatch:

```js
const onError = () => dispatch({ type: actionTypes.error });
 
const onWrite = ({ target: { value } }) => {
    dispatch({ type: actionTypes.write, payload: value });
}
```

**actionTypes**

- Cuando llamamos a dispath debemos darle un valor a action.type.
- Con el uso de actionTypes podremos evitar los errores de tipeo.
- consiste en usar un objeto para almacenar las keys de nuestro reducer

**Action creators:**

- Son funciones para actualizar el estado
- Consiste en declarar funciones que contienen nuestros dispatch

```jsx
importReactfrom "react";

const SECURITY_CODE = "paradigma";

function UseReducer({name}) {
    const [state, dispatch] =React.useReducer(reducer, initialState);
    const onConfirm = () => {
        dispatch({type: actionTypes.confirm});
    }
    const onError = () => {
        dispatch({type: actionTypes.error});
    }
    const onWrite = (eventValue) => {
        dispatch({type: actionTypes.write, payload:eventValue});
    }
    const onCheck = () => {
        dispatch({type: actionTypes.check});
    }
    const onDelete = () => {
        dispatch({type: actionTypes.delete});
    }
    const onReset = () => {
        dispatch({type: actionTypes.reset});
    }
React.useEffect(() => {

        if (!!state.loading) {
            setTimeout(() => {
                if (state.value === SECURITY_CODE) {
                    onConfirm()
                } else {
                    onError()
                }

            }, 3000);
        }
    }, [state.loading]);
    if (!state.deleted && !state.confirmed) {
        return (
            <React.Fragment>
                <h2>Eliminar{name}</h2>
                <p>Por favor, escribe el código de seguridad</p>
                {(!state.loading && state.error) && (
                    <p>El Código es incorrecto</p>
                )}
                {state.loading && (
                    <p>Cargando...</p>
                )}
                <input
                    value={state.value}
                    onChange={(event) => {
                        onWrite(event.target.value)
                    }}

                    placeholder="Código de Seguridad"
                />
                <button
                    onClick={onCheck}
                >Comprobar
</button>
            </React.Fragment>
        );
    } else if (!!state.confirmed && !state.deleted) {
        return (
            <React.Fragment>
                <h2>Eliminar{name}</h2>
                <p>¿Seguro que quieres eliminar{name}?</p>
                <button type="button" onClick={onDelete}>Si, eliminar
</button>
                <button type="button" onClick={onReset}>No, cancelar
</button>
            </React.Fragment>
        );
    } else {
        return (
            <React.Fragment>
                <h2>¡{name}eliminado!</h2>
                <button type="button" onClick={onReset}>Recuperar{name}</button>
            </React.Fragment>
        );
    }
}

const initialState = {
    value: "",
    error: false,
    loading: false,
    deleted: false,
    confirmed: false
};
const actionTypes = {
    confirm: 'CONFIRM',
    error: 'ERROR',
    write: 'WRITE',
    check: 'CHECK',
    delete: 'DELETE',
    reset: 'RESET',
}
const reducerObject = (state,payload= false) => ({
    [actionTypes.confirm]: {
        ...state,
        error: false,
        loading: false,
        confirmed: true
    },
    [actionTypes.error]: {
        ...state,
        error: true,
        loading: false
    },
    [actionTypes.write]: {
        ...state,
        value:payload
},
    [actionTypes.check]: {
        ...state,
        loading: true
    },
    [actionTypes.delete]: {
        ...state,
        deleted: true
    },
    [actionTypes.reset]: {
        ...state,
        confirmed: false,
        deleted: false,
        value: ''
    },
});
const reducer = (state,action) => {
    if (reducerObject(state)[action.type]) {
        return reducerObject(state,action.payload)[action.type]
    } else {
        returnstate;
    }
}

export {UseReducer};
```

[![img](https://www.google.com/s2/favicons?domain=https://read.reduxbook.com/markdown/part1/04-action-creators.html../../gitbook/images/apple-touch-icon-precomposed-152.png)Action Creators · Human Redux](https://read.reduxbook.com/markdown/part1/04-action-creators.html)

[![img](https://www.google.com/s2/favicons?domain=https://res.cloudinary.com/practicaldev/image/fetch/s--E8ak4Hr1--/c_limit,f_auto,fl_progressive,q_auto,w_32/https://dev-to.s3.us-east-2.amazonaws.com/favicon.ico)Redux action creators - DEV Community](https://dev.to/cesareferrari/redux-action-creators-3ea)

# 5. Manejo del estado en TODO Machine

## ¿Qué son los estados derivados?

Son variables que dependen del estado.
.
Ejemplo de estado derivado:

```jsx
const [allItem, setItem] = React.useState(initialValue); // Estado imperativo
 
const item = allItem.filter(todo => (!todo.deleted)); // Estado derivado
 
// Estado derivado
const saveItem = (newItem) => {
   try {
     const stringifiedTodos = JSON.stringify(newItem);
     localStorage.setItem(itemName, stringifiedTodos);
     setItem(newItem);
   } catch(error) {
     setError(error);
   }
 }
```

**Estados derivados**

- Variables que no se crean su propio **estado** pero dependen de un **estado** anterior
- No llaman a React.useState para definir su información
- Utilizan la información que ya habíamos guardado antes en otro estado y a partir de ahí algún calculo, cuenta, registro, etc.
- Pueden tener la misma convencion de nombres que los estados “normales”
- Se actualizan automáticamente con los cambios a los estados “normales”.
- Se crean a partir de estados normales.

**NO SE CREAN USANDO:**

- React.useState

[![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html/favicon.ico)Probablemente no necesitas estado derivado – React Blog](https://es.reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html)

## Migración de useState a useReducer en useLocalStorage

```jsx
import React from "react";

function useLocalStorage(itemName, initialValue) {
    const [state, dispatch] = React.useReducer(reducer, initialState({ initialValue }))

    const {
        sincronizedItem,
        loading,
        error,
        item,
    } = state;

    // Action creators
    const onError = (error)=>dispatch({ type: actionTypes.error, payload: error});
    const onSuccess = (parsedItem)=>dispatch({ type: actionTypes.success, payload: parsedItem});
    const onSave = (newItem)=>dispatch({ type: actionTypes.save, payload: newItem});
    const onSincronize = ()=>dispatch({ type: actionTypes.sincronize });

    React.useEffect(()=>{
        setTimeout( ()=>{
        try {
            const localStorageItem = localStorage.getItem(itemName);
            let parsedItem;

            if(!localStorageItem) {
            localStorage.setItem(itemName,JSON.stringify(initialValue));
            parsedItem = initialValue;
            } else {
            parsedItem = JSON.parse(localStorageItem);
            }
            onSuccess(parsedItem);
        } catch (error) {
            onError(error);
        }
        },3000);
    },[sincronizedItem])

    const saveItem = (newItem) =>{
        try {
            const stringifiedItem = JSON.stringify(newItem);
            localStorage.setItem(itemName,stringifiedItem);
            onSave(newItem);
        } catch (error) {
            onError(error);
        }
    }

    const sincronizeItem = () =>{
        onSincronize();
    }

    return {
        item, 
        saveItem, 
        loading,
        error,
        sincronizeItem,
    };
}

const initialState = ({ initialValue })=>({
    sincronizedItem: true,
    loading: true,
    error: false,
    item :initialValue,
});

const actionTypes = {
    error: 'ERROR',
    success: 'SUCCESS',
    save: 'SAVE',
    sincronize: 'SINCRONIZE',
}

const reducerObject = (state, payload) =>({
    [actionTypes.error]: {
        ...state,
        error: true,
    },
    [actionTypes.success]:{
        ...state,
        error:false,
        sincronizedItem: true,
        loading: false,
        item: payload,
    },
    [actionTypes.save]:{
        ...state,
        item:payload
    },
    [actionTypes.sincronize]:{
        ...state,
        sincronizedItem: false,
        loading: true,
    },
});

const reducer = (state, action) =>{
    return reducerObject(state, action.payload)[action.type] || state;
}

export { useLocalStorage }
```

## Imponer tu visión vs. trabajar en equipo

Me parece que hay varias oportunidades de mejora en el hook de `useTodos` me parece que esta muy integrado con todas las demás propiedades.

Hay muchas maneras que podríamos separar las responsabilidades porque hay algo que me causa mucha inquietud: ¿por qué `useTodos` se encarga del open modal? ¿por qué `useTodos` se encarga de searchTodos? Podríamos separar responsabilidades e incluso mejorar la re-usabilidad ¿Cómo podríamos mejorar la re-usabilidad?

Para el modal podríamos crear otro hook llamado `useModal` en donde almacenemos toda la lógica para usarlo con nuestro modal y si tuviéramos varias vistas podríamos reutilizar el componente modal en varios lugares de la aplicación.

Para la barra de búsqueda también podríamos tener otro hook `useSearchForm` para tener toda la lógica de nuestra barra de búsqueda y separarlo del resto de la lógica de los otros componentes de nuestra aplicación. Para la barra de búsqueda incluso podríamos agregar una función que venga como prop para reaccionar ante los eventos de escritura, así podríamos manejar como debe cambiar la interfaz con los cambios del hijo.

Para mostrar los todos, podríamos simplemente recibir como props la lista de todos y lo que hay en la barra de búsqueda para que el se encargue de renderizar y meter la lógica en ese componente o usar un Hook si hay oportunidad de re-utlizar la lógica.

Estas son solo algunas sugerencias, puedes dejar aquí 👇 mas sugerencias de mejora. Recuerda que primero puedes implementar la mejor solución posible y después puedes optimizar. Muchas gracias Juan por este curso 💚

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-react-estado-2 at b2b0656e964796842efedc0382e0eb66f236f1a7](https://github.com/platzi/curso-react-estado-2/tree/b2b0656e964796842efedc0382e0eb66f236f1a7)

