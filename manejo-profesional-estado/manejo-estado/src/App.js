import react from 'react';
import { UseState } from './UseState.jsx';
// import { ClassState } from './ClassState.jsx';
import { UseReducer } from './UseReducer';
import './App.css';

function App() {
  return (
    <div className="App">
      <UseState name="User State"/>
      <UseReducer name="Use Reducer" />
    </div>
  );
}

export default App;
