import React from 'react';

const SECURITY_CODE = 'hola';

function UseState({ name }) {
  const [state, setState] = React.useState({
    value: '',
    error: false,
    loading: false,
    deleted: false,
    confirmed: false,
  });

  const onConfirm = () => {
    setState({
      ...state,
      error: false,
      loading: false,
      confirmed: true,
    });
  };

  const onError = () => {
    setState({
      ...state,
      error: true,
      loading: false,
    });
  }

  const onWrite = (newValue) => {
    setState({ 
      ...state, 
      value: newValue, 
    });
  };

  const onCheck = () => {
    setState({ 
      ...state, 
      loading: true, 
    });
  };

  const onDelete = () => {
    setState({
      ...state,
      deleted: true,
    });
  };

  const onReset = () => {
    setState({
      ...state,
      confirmed: false,
      deleted: false,
      value: '',
    });
  };

  console.log(state);
  // const [value, setValue] = React.useState('');
  // const [error, setError] = React.useState(false);
  // const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    console.log('Empezando el efecto');

    if (!!state.loading) {
      setTimeout(() => {
        console.log('incio de validacion');

        if (state.value === SECURITY_CODE) {
          onConfirm();
        } else {        // setLoading(false);
          onError();
        }
        console.log('Terminando la validacion');
      }, 3000);
    }

    console.log('Finalizado el efecto');
  }, [state.loading]);

  if (!state.deleted && !state.confirmed) {
    return (
      <div>
        <h2>Eliminar {name}</h2>

        <p>Por favor, escribe el codigo de seguridad.</p>

        {(state.error && !state.loading) && (
        <p>Error: el codigo es incorrecto</p>
        )}

        {state.loading && (
        <p>Cargando...</p>
        )}

        <input 
          placeholder="Codigo de seguridad"
          value={state.value}
          onChange={(event) => {
            onWrite(event.target.value);
          }}
        />
        <button 
          onClick={() => { 
            // setError(false);    // este fue
            onCheck();
          }}
        >Comprobar</button>
      </div>
    );
  } else if(!!state.confirmed && !state.deleted) {
    return (
      <React.Fragment>
        <p>Pedimos confirmacion. Seguro?</p>
        <button
          onClick={() => {
            onDelete();
          }}
        >
        Si, eliminar
        </button>
        <button
          onClick={() => {
            onReset();
          }}
        >
          No, cancelar
        </button>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <p>Elimando con exito</p>
        <button
          onClick={() => {
            onReset();
          }}
        >
          Resetear, volver atras
        </button>
      </React.Fragment>
    )
  }
}

export { UseState };
