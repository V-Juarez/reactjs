## Busqueda con navegacion

```react
import React from 'react'
import { useSearchParams } from 'react-router-dom'

export function TodoSearch({ setSearchValue, loading }) {
    const [searchParams, setSearchParams] = useSearchParams()
    const paramsValue = searchParams.get('search')

    const onSearchValueChange = ({ target: { value } }) => {
        setSearchValue(value)
        setSearchParams({ search: value })
    }

    return (
        <input
            className={`TodoSearch ${loading && 'TodoSearch--loading'}`}
            onChange={onSearchValueChange}
            value={paramsValue ?? ''}
            placeholder="Search a To-Do"
        />
    )
}
```

#### Version

Para resolver el reto se puede ulitlizar el hook *useSearchParams*

En el HomePage.jsx agregamos:

```react
 const [params, setParams] = useSearchParams();
```

Y se lo enviamos a TodoSearch:

```react
<TodoSearch
          searchValue={searchValue}
          setSearchValue={setSearchValue}
          params={params}
          setParams={setParams}
        />
```

Ya en TodoSearch.jsx lo manejamos de la siguiente manera:

```react
import React, { useEffect } from "react";

function TodoSearch({ searchValue, setSearchValue, params, setParams }) {
  const onSearchValueChange = (event) => {
    setSearchValue(event.target.value);

    let params = {
      search: event.target.value,
    };
    setParams(params);
  };

  useEffect(() => {
    const search = params.get("search") ?? "";
    setSearchValue(search);
  }, [params]);

  return (
    <input
      className="todo-search"
      placeholder="Search..."
      type="text"
      value={searchValue}
      onChange={onSearchValueChange}
    />
  );
}

export { TodoSearch };
```

De esa manera logramos el siguiente resultado
![useSearchParams](https://i66.servimg.com/u/f66/20/43/92/62/usesea10.png)

## React Router DOM5

Los cambios que hice para que funcionará con React router dom versión 5 fueron:

En el archivo App.js

![1.PNG](https://static.platzi.com/media/user_upload/1-6a2e63d6-8370-4b8e-8ae0-62d579cc6054.jpg)

En el archivo TodoForm/index.js

![2.PNG](https://static.platzi.com/media/user_upload/2-568904b8-f51c-4789-a7b3-692076a6d6b5.jpg)

En el archivo HomePage.js

![3.PNG](https://static.platzi.com/media/user_upload/3-7cc6cbcb-0f00-4d50-8aef-e2a227fb6688.jpg)

### Version

```react
function TodoSearch({ searchValue, setSearchValue, loading }) {
  const history = useHistory();

  const onSearchValueChange = (event) => {
    const value = event.target.value;
    setSearchValue(value);
    history.push({ search: value });  
//modificar la url y guarda todo en location
  };

const param = history.location.search.slice(1);
// usar el metodo slice para quitar  '?' al inicio del string

 const searchParam = decodeURI(param) ?? searchValue;
// decodeURI  es una funcion de javascript ayuda a decodificar la url.
  return (
    <input
      className="TodoSearch"
      placeholder="Cebolla"
      value={searchParam}
      onChange={onSearchValueChange}
      disabled={loading}
    />
  );
}
```

## PlatziMovies con React Router

Convertir PlatziMovies en React

## crea tu propio React Router

## Router.jsx

```react
import { useState } from 'react'
import { PathContext } from './Context'

export function Router({ children }) {
    const [currentPath, setCurrentPath] = useState(window.location.pathname)
    const routes = []

    const navigate = to => {
        window.history.pushState({}, '', to)
        setCurrentPath(to)
    }

    return (
        <PathContext.Provider
            value={{ currentPath, setCurrentPath, routes, navigate }}
        >
            {children}
        </PathContext.Provider>
    )
}
```

## Route.jsx

```react
import { useContext } from 'react'
import { PathContext } from './Context'

export function Route({ path, element }) {
    const { currentPath, routes } = useContext(PathContext)

    //Push all routes except '*' (not found) to routes[]
    const routeInRoutes = routes.includes(path)
    if (!routeInRoutes && path != '*') routes.push(path)

    //returning in match
    if (currentPath == path) return element

    // Not found case
    const currentPathInRoutes = routes.includes(currentPath)
    if (path == '*' && !currentPathInRoutes) return element
}
```

## Link.jsx

```react
import { useContext } from 'react'
import { PathContext } from './Context'

const styles = {
    textDecoration: 'underline',
    color: 'blue',
    cursor: 'pointer',
}

export function Link({ to, children }) {
    const { navigate } = useContext(PathContext)

    return (
        <a style={styles} onClick={() => navigate(to)}>
            {children}
        </a>
    )
}
```

## HashRouter.jsx

```react
import { useState } from 'react'
import { PathContext } from './Context'

export function HashRouter({ children }) {
    const windowPath = window.location.pathname.replace(/^\/#/, '')
    const [currentPath, setCurrentPath] = useState(windowPath)
    const routes = []

    const navigate = to => {
        window.history.pushState({}, '', `/#${to}`)
        setCurrentPath(to)
    }

    return (
        <PathContext.Provider
            value={{ currentPath, setCurrentPath, routes, navigate }}
        >
            {children}
        </PathContext.Provider>
    )
}
```

## Context.jsx

```react
import { createContext } from 'react'

export const PathContext = createContext()
```

## App.jsx

```
import { Link } from '../router/Link'
import { Route } from '../router/Route'
import { Router } from '../router/Router'

export function App() {
    return (
        <Router>
            <Route element={<HomePage />} path="/" />
            <Route element={<BlogPage />} path="/blog" />
            <Route element={<NotFound />} path="*" />
        </Router>
    )
}

function HomePage() {
    return (
        <>
            <h1>You are in the home</h1>
            <Link to="/blog">Go to blog</Link>
            <p></p>
            <Link to="/random-link">Go to non-existent page</Link>
        </>
    )
}

function BlogPage() {
    return (
        <>
            <h1>You are in the blog</h1>
            <Link to="/">Back to home</Link>
        </>
    )
}

function NotFound() {
    return (
        <>
            <h1>Page not found</h1>
            <Link to="/">Go to home</Link>
        </>
    )
}
```

## AppWithHash.jsx

```react
import { Link } from '../router/Link'
import { Route } from '../router/Route'
import { HashRouter } from '../router/HashRouter'

export function AppWithHash() {
    return (
        <HashRouter>
            <Route element={<HomePage />} path="/" />
            <Route element={<BlogPage />} path="/blog" />
            <Route element={<NotFound />} path="*" />
        </HashRouter>
    )
}

function HomePage() {
    return (
        <>
            <h1>You are in the home</h1>
            <Link to="/blog">Go to blog</Link>
            <p></p>
            <Link to="/random-link">Go to non-existent page</Link>
        </>
    )
}

function BlogPage() {
    return (
        <>
            <h1>You are in the blog</h1>
            <Link to="/">Back to home</Link>
        </>
    )
}

function NotFound() {
    return (
        <>
            <h1>Page not found</h1>
            <Link to="/">Go to home</Link>
        </>
    )
}
```