import React from 'react';
import { TodoForm } from '../../ui/TodoForm';
import './NewTodoPage.css';
import { useTodos } from '../useTodos';

const NewTodoPage = () => {
  const { stateUpdaters } = useTodos();
  const { addTodo } = stateUpdaters;

  return (
    <div className='new-todo-container'>
      <TodoForm
        label="Write your new TODO"
        submitText="Add"
        submitEvent={(text) => addTodo(text)}
      />
    </div>
  )
}

export { NewTodoPage };
