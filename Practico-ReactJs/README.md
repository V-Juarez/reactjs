<h1>Práctico de React JS</h1>

<h3>Oscar Barajas Tavares</h3>

<img src="https://arepa.s3.amazonaws.com/react.png" style="zoom:100%;" />

<h1>Tabla de Contenido</h1>
- [1. ¿Qué es React JS?](#1-qué-es-react-js)
  - [Aprende qué es React.js](#aprende-qué-es-reactjs)
  - [Historia](#historia)
  - [DOM, Virtual DOM y React DOM](#dom-virtual-dom-y-react-dom)
  - [DOM virtual y detalles de implementación](#dom-virtual-y-detalles-de-implementación)
  - [¿Qué es el DOM virtual?](#qué-es-el-dom-virtual)
  - [¿Es el Shadow DOM lo mismo que el DOM virtual?](#es-el-shadow-dom-lo-mismo-que-el-dom-virtual)
- [2. Crear una Aplicación con React JS](#2-crear-una-aplicación-con-react-js)
  - [Create React App y Tipos de Componentes](#create-react-app-y-tipos-de-componentes)
    - [Inicialización de un proyecto en React](#inicialización-de-un-proyecto-en-react)
    - [Creación y Tipos de Componentes](#creación-y-tipos-de-componentes)
  - [Hola mundo](#hola-mundo)
  - [Stateful and Stateless Components in React](#stateful-and-stateless-components-in-react)
  - [Componentes con estado y sin estado](#componentes-con-estado-y-sin-estado)
  - [Revisión del estado](#revisión-del-estado)
  - [Componentes con estado y sin estado](#componentes-con-estado-y-sin-estado-1)
  - [Componentes Visuales](#componentes-visuales)
    - [Tipos de componentes:](#tipos-de-componentes)
  - [JSX: JavaScript + HTML](#jsx-javascript--html)
  - [Presentando JSX](#presentando-jsx)
  - [¿Por qué JSX?](#por-qué-jsx)
  - [Insertando expresiones en JSX](#insertando-expresiones-en-jsx)
    - [JSX también es una expresión](#jsx-también-es-una-expresión)
    - [Especificando atributos con JSX](#especificando-atributos-con-jsx)
    - [Especificando hijos con JSX](#especificando-hijos-con-jsx)
    - [JSX previene ataques de inyección](#jsx-previene-ataques-de-inyección)
    - [JSX representa objetos](#jsx-representa-objetos)
    - [Estilo y CSS](#estilo-y-css)
    - [¿Cómo agrego clases CSS a los componentes?](#cómo-agrego-clases-css-a-los-componentes)
    - [¿Puedo usar estilos en línea?](#puedo-usar-estilos-en-línea)
    - [¿Los estilos en línea son malos?](#los-estilos-en-línea-son-malos)
    - [¿Qué es CSS-in-JS?](#qué-es-css-in-js)
    - [¿Puedo hacer animaciones en React?](#puedo-hacer-animaciones-en-react)
  - [Props: Comunicación entre Componentes](#props-comunicación-entre-componentes)
  - [¿Qué son los métodos del ciclo vida?](#qué-son-los-métodos-del-ciclo-vida)
  - [State - Events](#state---events)
  - [Instalación y configuración de entorno](#instalación-y-configuración-de-entorno)
  - [Agregando compatibilidad con todos los navegadores usando Babel](#agregando-compatibilidad-con-todos-los-navegadores-usando-babel)
- [3. Configurar un entorno de trabajo profesional](#3-configurar-un-entorno-de-trabajo-profesional)
  - [Webpack: Empaquetando nuestros módulos](#webpack-empaquetando-nuestros-módulos)
  - [Webpack Dev Server: Reporte de errores y Cambios en tiempo real](#webpack-dev-server-reporte-de-errores-y-cambios-en-tiempo-real)
  - [Estilos con SASS](#estilos-con-sass)
  - [Configuración final: ESLint y Git Ignore](#configuración-final-eslint-y-git-ignore)
  - [Arquitectura de componentes para Platzi Video](#arquitectura-de-componentes-para-platzi-video)
- [4. Llevar un diseño de HTML y CSS a React](#4-llevar-un-diseño-de-html-y-css-a-react)
  - [Estructura del Header](#estructura-del-header)
  - [Estilos del Header](#estilos-del-header)
  - [Estructura y Estilos del Buscador](#estructura-y-estilos-del-buscador)
  - [Estructura y Estilos de Carousel y Carousel Item](#estructura-y-estilos-de-carousel-y-carousel-item)
  - [Estructura y Estilos del Footer](#estructura-y-estilos-del-footer)
  - [Añadiendo imágenes con Webpack](#añadiendo-imágenes-con-webpack)
  - [Imports, Variables y Fuentes de Google en Sass](#imports-variables-y-fuentes-de-google-en-sass)
- [5. Uso de una API de desarrollo (Fake API)](#5-uso-de-una-api-de-desarrollo-fake-api)
  - [Creando una Fake API](#creando-una-fake-api)
  - [React Hooks: useEffect y useState](#react-hooks-useeffect-y-usestate)
  - [Lectura React Hooks](#lectura-react-hooks)
  - [Conectando la información de la API](#conectando-la-información-de-la-api)
  - [Custom Hooks](#custom-hooks)
  - [PropTypes](#proptypes)
- [6. Usar React Tools](#6-usar-react-tools)
  - [Debuggeando React con React DevTools](#debuggeando-react-con-react-devtools)
  - [Prepárate para lo que sigue](#prepárate-para-lo-que-sigue)


# 1. ¿Qué es React JS?

## Aprende qué es React.js
React es una librería desarrollada por Facebook que nos ayuda a construir interfaces de usuario interactivas para todo tipo de aplicaciones: web, móviles o de escritorio.

Cada pequeña parte de nuestra página web la conoceremos como “Componente”. Cada componente se encargará de una función en específico. Además, podremos reutilizar nuestros componentes siempre que lo necesitemos.

Al unir todos nuestros componentes tendremos una página web que nos permite cambiar, actualizar o eliminar elementos de forma muy sencilla.

Nuestro profesor será Oscar Barajas: Frontend Developer en Platzi y uno de los líderes en la comunidad de Facebook Developer Circles.

![React.svg](https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/245px-React.svg.png)

**React** (también llamada **React.js** o ReactJS) es una biblioteca [Javascript](https://es.wikipedia.org/wiki/JavaScript) de [código abierto](https://es.wikipedia.org/wiki/Código_abierto) diseñada para crear [interfaces de usuario](https://es.wikipedia.org/wiki/Interfaz_de_usuario) con el objetivo de facilitar el desarrollo de [aplicaciones en una sola página](https://es.wikipedia.org/wiki/Single-page_application). Es mantenido por [Facebook](https://es.wikipedia.org/wiki/Facebook) y la comunidad de [software libre](https://es.wikipedia.org/wiki/Software_libre), han participado en el proyecto más de mil [desarrolladores](https://es.wikipedia.org/wiki/Desarrollador_de_software) diferentes. [1](https://es.wikipedia.org/wiki/React#cite_note-infoworld-1)

React intenta ayudar a los desarrolladores a construir [aplicaciones](https://es.wikipedia.org/wiki/Aplicación_informática) que usan datos que cambian todo el [tiempo](https://es.wikipedia.org/wiki/Tiempo). Su objetivo es ser sencillo, declarativo y fácil de combinar. React sólo maneja la [interfaz de usuario](https://es.wikipedia.org/wiki/Interfaz_de_usuario) en una aplicación; React es la **Vista** en un contexto en el que se use el patrón [MVC](https://es.wikipedia.org/wiki/Modelo–vista–controlador) (Modelo-Vista-Controlador) o [MVVM](https://es.wikipedia.org/wiki/Modelo–vista–modelo_de_vista) (Modelo-vista-modelo de vista). También puede ser utilizado con las extensiones de React-based que se encargan de las partes no-UI (que no forman parte de la interfaz de usuario) de una [aplicación web](https://es.wikipedia.org/wiki/Aplicación_web).

Según el servicio de análisis Javascript (en inglés “javascript analytics service”), Libscore, React actualmente está siendo utilizado en las páginas principales de Imgur, Bleacher Informe, [Feedly](https://es.wikipedia.org/wiki/Feedly), [Airbnb](https://es.wikipedia.org/wiki/Airbnb), SeatGeek, HelloSign, y otras.[2](https://es.wikipedia.org/wiki/React#cite_note-2)

## Historia

React fue creada por Jordan Walke, un ingeniero de software en Facebook, inspirado por los problemas que tenía la compañía con el mantenimiento del código de los anuncios dentro de su plataforma. Enfocado en la experiencia del usuario y la eficiencia para sus programadores, influenciado por XHP (un marco de componentes de [HTML](https://es.wikipedia.org/wiki/HTML) para [PHP](https://es.wikipedia.org/wiki/PHP)), nace el prototipo ReactJS.

## DOM, Virtual DOM y React DOM

El **DOM** es el código HTML que se transforma en páginas web.

Cada vez que cambiamos alguna parte del DOM, también estamos actualizando el HTML con el que interactúan nuestros usuarios. El problema es que todas las operaciones, comparaciones y actualizaciones en el DOM son muy costosas.

El **Virtual DOM** es una herramienta que usan tecnologías como React y Vue para mejorar el rendimiento (*performance*) y velocidad de nuestras aplicaciones.

Es una copia exacta del DOM, pero mucho más ligera, ya que los cambios no actualizan el verdadero HTML de nuestras páginas web. Gracias al Virtual DOM podemos hacer operaciones y comparaciones de forma sumamente rápida.

Recuerda que los cambios en el Virtual DOM no afectan el HTML que ven los usuarios, así que debemos estar sincronizando constantemente las copias con el DOM. Pero no te preocupes, **React DOM** lo hace por nosotros.

## DOM virtual y detalles de implementación

## ¿Qué es el DOM virtual?

El DOM virtual (VDOM) es un concepto de programación donde una representación ideal o “virtual” de la IU se mantiene en memoria y en sincronía con el DOM “real”, mediante una biblioteca como ReactDOM. Este proceso se conoce como [reconciliación](https://es.reactjs.org/docs/reconciliation.html).

Este enfoque hace posible la API declarativa de React: le dices a React en qué estado quieres que esté la IU, y se hará cargo de llevar el DOM a ese estado. Esto abstrae la manipulación de atributos, manejo de eventos y actualización manual del DOM que de otra manera tendrías que usar para construir tu aplicación.

Ya que “DOM virtual” es más un patrón que una tecnología específica, las personas a veces le dan significados diferentes. En el mundo de React, el término “DOM virtual” es normalmente asociado con [elementos de React](https://es.reactjs.org/docs/rendering-elements.html) ya que son objetos representando la interfaz de usuario. Sin embargo, React también usa objetos internos llamados “fibers” para mantener información adicional acerca del árbol de componentes. Éstos pueden ser también considerados como parte de la implementación de “DOM virtual” de React.

## ¿Es el Shadow DOM lo mismo que el DOM virtual?

No, son diferentes. El Shadow DOM es una tecnología de los navegadores diseñada principalmente para limitar el alcance de variables y CSS en componentes web. El DOM virtual es un concepto que implementan bibliotecas en JavaScript por encima de las APIs de los navegadores.

# 2. Crear una Aplicación con React JS

## Create React App y Tipos de Componentes

### Inicialización de un proyecto en React

Creación de nuestro sitio web usando la plantilla por defecto de [create-react-app](https://facebook.github.io/create-react-app/docs/getting-started):

```bash
npx create-react-app nombre-de-tu-proyecto
```

Iniciar el servidor de desarrollo:

```bash
npm start
```

Al clonar el repositorio: Realizar el siguiente proceso.

1. Copiar y pegar lo siguiente para descargar solo la rama donde se encuentra la carpeta inicial y no todo el proyecto inicial.

```bash
git clone --single-branch --branch 1.ReactDOM.render https://github.com/Sparragus/platzi-badges.git
```

2. Luego de tener descargada la carpeta, entramos a la carpeta desde la terminal, luego escribimos lo siguiente para ver cuales dependencias se han actualizado:

```bash
npx npm-check-updates -u
```

3. Luego escribir, para installar los modulos necesarios.

   ```bash
   npm install
   ```

4. Actualizar navegador

   ```bash
   npx browserslist@latest --update-db
   ```

   

### Creación y Tipos de Componentes

Los nombres de nuestros componentes deben empezar con una letra mayúscula, al igual que cada nueva palabra del componente. Esto lo conocemos como *Pascal Case* o *Upper Camel Case*.

Los componentes **Stateful** son los más robustos de React. Los usamos creando clases que extiendan de `React.Component`. Nos permiten manejar estado y ciclo de vida (más adelante los estudiaremos a profundidad).

```js
import React, { Component } from 'react';

class Stateful extends Component {
  constructor(props) {
    super(props);

    this.state = { hello: 'hello world' };
  }

  render() {
    return (
      <h1>{this.state.hello}</h1>
    );
  }
}

export default Stateful;
```

También tenemos componentes **Stateless** o Presentacionales. Los usamos creando funciones que devuelvan código en formato JSX (del cual hablaremos en la próxima clase).

```js
import React from 'react';

const Stateless = () => {
  return (
    <h1>¡Hola!</h1>
  );
}

// Otra forma de crearlos:
const Stateless = () => <h1>¡Hola!</h1>;

export default Stateless;
```

## Hola mundo

El más pequeño de los ejemplos de React se ve así:

```js
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
```

Este muestra un encabezado con el texto “Hello, world!” en la página.

## [Stateful and Stateless Components in React](https://programmingwithmosh.com/javascript/stateful-stateless-components-react/)

## Componentes con estado y sin estado

Hoy, revisaremos qué componentes con estado y sin estado se encuentran en React, cómo puede notar la diferencia y el complejo proceso de decidir si los componentes tienen estado o no.

## Revisión del estado

Primero, repasemos qué estado es.

En un componente, el estado son datos que importamos, generalmente para mostrar al usuario, que están sujetos a cambios. Podría cambiar porque la base de datos de la que estamos obteniendo puede actualizarse, el usuario la modificó, ¡hay muchas razones por las que los datos cambian!

```javascript
import React, {Component} from 'react'

class Pigeons extends Component {
  constructor() {
    super()
    this.state = {
      pigeons: []
    }
  }
  render() {
    return (
      <div>
        <p>Look at all the pigeons spotted today!</p>
        <ul>
          {this.state.pigeons.map(pigeonURL => {
            return <li><img src={pigeonURL} /></li>
          })}
        </ul>
      </div>
    )
  }
}
```

Por lo general, también tendríamos un componenteDidMount () que tomaría nuestros datos de una base de datos, pero el ejemplo anterior debería darle una idea básica: tenemos estado y podemos representar cosas desde el estado.

## Componentes con estado y sin estado

Los componentes con estado y sin estado tienen muchos nombres diferentes.

También son conocidos como:

- Componentes de contenedor vs presentación
- Componentes inteligentes vs tontos

La diferencia literal es que uno tiene estado y el otro no. Eso significa que los componentes con estado realizan un seguimiento de los datos cambiantes, mientras que los componentes sin estado imprimen lo que se les da a través de accesorios, o siempre representan lo mismo.

Componente con estado / contenedor / inteligente:

```javascript
class Main extends Component {
 constructor() {
   super()
   this.state = {
     books: []
   }
 }
 render() {
   <BooksList books={this.state.books} />
 }
}
```

Sin estado / Presentacional / Componente tonto:

```javascript
const BooksList = ({books}) => {
 return (
   <ul>
     {books.map(book => {
       return <li>book</li>
     })}
   </ul>
 )
}
```

Observe que el componente sin estado se escribe como una función. Tan bueno como es el estado, siempre debe tratar de hacer que sus componentes sean lo más simples y sin estado posible, de modo que los diferentes componentes se puedan reutilizar como piezas de Lego, incluso si no tiene planes inmediatos para reutilizar un componente. ¡Los con estado deberían sentirse afortunados de ser así!

## Componentes Visuales

Conocidos en inglés como Presentational Components. Este tipo de componentes solo deben centrase y enfocar sus esfuerzos en como debe renderizarse la UI. Este tipo de componentes puede componerse de otros elementos visuales y suele incluir estilos y clases. Todos los datos implicados en su renderización se deben recibir a través de props, por lo que deben ser independientes de llamadas a servicios externos. Este tipo de componentes suelen ser de tipo Stateless ya que no necesitan estado, y deben de gestionar las acciones pasándoselas a componentes padre a través de sus props.

***Ejemplo:\***

```js
class Item extends React.Component {  
    render () {  
        return (  
            <li><a href='#'>{ this.props.valor }</a></li>  
        );  
    }  
}
```

### Tipos de componentes:

**Stateful**: Es un tipo de componente muy robusto. Es conocido como estructura de clase porque nos permite tener ciclo de vida y estado.

**Stateless**: No depende de un estado ni ciclo de vida. Este presenta solo la lógica. Son mas utilizado porque trabajan con la parte funcional. Son funciones pero nos permiten enfocarnos solo en una particularidad.

**Presentacional**: Son aquellos que tienen una parte muy particular de HTML que se va a ver en el navegador, que no tienen lógica y propiedades.

> Si usa npm 5.1 o anterior, no puede usarlo npx. En su lugar, instale create-react-appglobalmente:
>
> ```bash
> npm install -g create-react-app
> ```
>
> Ahora puedes ejecutar:
>
> ```shell
> create-react-app my-app
> ```
>
> 

![img](https://www.google.com/s2/favicons?domain=https://facebook.github.io/create-react-app/docs/getting-started/img/favicon/favicon.ico)[Getting Started · Create React App](https://facebook.github.io/create-react-app/docs/getting-started)

## JSX: JavaScript + HTML

Estamos acostumbrados a escribir código HTML en archivos `.html` y la lógica de JavaScript en archivos `.js`.

React usa **JSX**: una sintaxis que nos permite escribir la estructura HTML y la lógica en JavaScript desde un mismo lugar: nuestros componentes.

```js
import React from 'react';

const HolaMundo = () => {
  // Esto es JavaScript:
  const claseCSSHolaMundo = 'container-red';
  const mensajeTextoHTML = '¡Hola, Mundo!';
  const isTrue = false;

  // Esto es JSX (HTML + JavaScript):
  return (
    <div className={claseCSSHolaMundo}>
      <h1>{mensajeTextoHTML}</h1>

      {isTrue ? '¡Es verdad! :D' : '¡No es verdad! :('}
    </div>
  );
}

export default HolaMundo;
```

## Presentando JSX

Considera la declaración de esta variable:

```
const element = <h1>Hello, world!</h1>;
```

Esta curiosa sintaxis de etiquetas no es ni un string ni HTML.

Se llama JSX, y es una extensión de la sintaxis de JavaScript. Recomendamos usarlo con React para describir cómo debería ser la interfaz de usuario. JSX puede recordarte a un lenguaje de plantillas, pero viene con todo el poder de JavaScript.

JSX produce “elementos” de React. Exploraremos como renderizarlos en el DOM en la [siguiente sección](https://es.reactjs.org/docs/rendering-elements.html). A continuación puedes encontrar lo básico de JSX que será necesario para empezar.

## ¿Por qué JSX?

React acepta el hecho de que la lógica de renderizado está intrínsecamente unida a la lógica de la interfaz de usuario: cómo se manejan los eventos, cómo cambia el estado con el tiempo y cómo se preparan los datos para su visualización.

En lugar de separar artificialmente *tecnologías* poniendo el maquetado y la lógica en archivos separados, React [separa *intereses*](https://es.wikipedia.org/wiki/Separación_de_intereses) con unidades ligeramente acopladas llamadas “componentes” que contienen ambas. Volveremos a los componentes en [otra sección](https://es.reactjs.org/docs/components-and-props.html), pero si aún no te sientes cómodo maquetando en JS, [esta charla](https://www.youtube.com/watch?v=x7cQ3mrcKaY) podría convencerte de lo contrario.

React [no requiere](https://es.reactjs.org/docs/react-without-jsx.html) usar JSX, pero la mayoría de la gente lo encuentra útil como ayuda visual cuando trabajan con interfaz de usuario dentro del código Javascript. Esto también permite que React muestre mensajes de error o advertencia más útiles.

Con eso fuera del camino, ¡empecemos!

## Insertando expresiones en JSX

En el ejemplo a continuación, declaramos una variable llamada `name` y luego la usamos dentro del JSX envolviéndola dentro de llaves:

```js
const name = 'Josh Perez';
const element = <h1>Hello, {name}</h1>;

ReactDOM.render(
  element,
  document.getElementById('root')
);
```

Puedes poner cualquier [expresión de JavaScript](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Expressions_and_Operators) dentro de llaves en JSX. Por ejemplo, `2 + 2`, `user.firstName`, o `formatName(user)` son todas expresiones válidas de Javascript.

En el ejemplo a continuación, insertamos el resultado de llamar la función de JavaScript, `formatName(user)`, dentro de un elemento `<h1>`.

```js
function formatName(user) {
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: 'Harper',
  lastName: 'Perez'
};

const element = (
  <h1>
    Hello, {formatName(user)}!
  </h1>
);

ReactDOM.render(
  element,
  document.getElementById('root')
);
```

[**Pruébalo en CodePen**](https://es.reactjs.org/redirect-to-codepen/introducing-jsx)

Dividimos JSX en varias líneas para facilitar la lectura. Aunque no es necesario, cuando hagas esto también te recomendamos envolverlo entre paréntesis para evitar errores por la [inserción automática del punto y coma](https://stackoverflow.com/q/2846283).

### JSX también es una expresión

Después de compilarse, las expresiones JSX se convierten en llamadas a funciones JavaScript regulares y se evalúan en objetos JavaScript.

Esto significa que puedes usar JSX dentro de declaraciones `if` y bucles `for`, asignarlo a variables, aceptarlo como argumento, y retornarlo desde dentro de funciones:

```js
function getGreeting(user) {
  if (user) {
    return <h1>Hello, {formatName(user)}!</h1>;
  }
  return <h1>Hello, Stranger.</h1>;
}
```

### Especificando atributos con JSX

Puedes utilizar comillas para especificar strings literales como atributos:

```js
const element = <div tabIndex="0"></div>;
```

También puedes usar llaves para insertar una expresión JavaScript en un atributo:

```js
const element = <img src={user.avatarUrl}></img>;
```

No pongas comillas rodeando llaves cuando insertes una expresión JavaScript en un atributo. Debes utilizar comillas (para los valores de los strings) o llaves (para las expresiones), pero no ambas en el mismo atributo.

> **Advertencia:**
>
> Dado que JSX es más cercano a JavaScript que a HTML, React DOM usa la convención de nomenclatura `camelCase` en vez de nombres de atributos HTML.
>
> Por ejemplo, `class` se vuelve [`className`](https://developer.mozilla.org/es/docs/Web/API/Element/className) en JSX, y `tabindex` se vuelve [`tabIndex`](https://developer.mozilla.org/es/docs/Web/API/HTMLElement/tabIndex).

### Especificando hijos con JSX

Si una etiqueta está vacía, puedes cerrarla inmediatamente con `/>`, como en XML:

```jsx
const element = <img src={user.avatarUrl} />;
```

Las etiquetas de Javascript pueden contener hijos:

```jsx
const element = (
  <div>
    <h1>Hello!</h1>
    <h2>Good to see you here.</h2>
  </div>
);
```

### JSX previene ataques de inyección

Es seguro insertar datos ingresados por el usuario en JSX:

```jsx
const title = response.potentiallyMaliciousInput;
// Esto es seguro:
const element = <h1>{title}</h1>;
```

Por defecto, React DOM [escapa](https://stackoverflow.com/questions/7381974/which-characters-need-to-be-escaped-on-html) cualquier valor insertado en JSX antes de renderizarlo. De este modo, se asegura de que nunca se pueda insertar nada que no esté explícitamente escrito en tú aplicación. Todo es convertido en un string antes de ser renderizado. Esto ayuda a prevenir vulnerabilidades [XSS (cross-site-scripting)](https://es.wikipedia.org/wiki/Cross-site_scripting).

### JSX representa objetos

Babel compila JSX a llamadas de `React.createElement()`.

Estos dos ejemplos son idénticos:

```jsx
const element = (
  <h1 className="greeting">
    Hello, world!
  </h1>
);
const element = React.createElement(
  'h1',
  {className: 'greeting'},
  'Hello, world!'
);
```

`React.createElement()` realiza algunas comprobaciones para ayudarte a escribir código libre de errores, pero, en esencia crea un objeto como este:

```jsx
// Nota: Esta estructura está simplificada
const element = {
  type: 'h1',
  props: {
    className: 'greeting',
    children: 'Hello, world!'
  }
};
```

Estos objetos son llamados “Elementos de React”. Puedes pensar en ellos como descripciones de lo que quieres ver en pantalla. React lee estos objetos y los usa para construir el DOM y mantenerlo actualizado.

Vamos a explorar el renderizado de los elementos de React al DOM en la siguiente sección.

> **Tip:**
>
> Recomendamos usar la [Definición del lenguaje en “Babel”](https://babeljs.io/docs/editors) en tu editor de elección para que tanto el código en ES6 como el código en JSX sea resaltado apropiadamente. Este sitio web utiliza el esquema de color [Oceanic Next](https://labs.voronianski.com/oceanic-next-color-scheme/), el cual es compatible con esto.

### Estilo y CSS

### ¿Cómo agrego clases CSS a los componentes?

Pasa una string como la prop `className`:

```jsx
render() {
  return <span className="menu navigation-menu">Menu</span>
}
```

Es común que las clases CSS dependan de las props o del estado del componente:

```jsx
render() {
  let className = 'menu';
  if (this.props.isActive) {
    className += ' menu-active';
  }
  return <span className={className}>Menu</span>
}
```

> Tip
>
> Si a menudo escribes código como este, el paquete [classnames](https://www.npmjs.com/package/classnames#usage-with-reactjs) puede hacerlo más simple.

### ¿Puedo usar estilos en línea?

Sí, ve la documentación sobre estilo [aquí](https://es.reactjs.org/docs/dom-elements.html#style).

### ¿Los estilos en línea son malos?

Las clases CSS son generalmente mejores para el rendimiento que los estilos en línea.

### ¿Qué es CSS-in-JS?

“CSS-in-JS” se refiere a un patrón donde el CSS se compone usando JavaScript en lugar de definirlo en archivos externos. Lee una comparación de las bibliotecas CSS-in-JS [aquí](https://github.com/MicheleBertoli/css-in-js).

*Ten en cuenta que esta funcionalidad no es parte de React, sino que es proporcionada por bibliotecas de terceros.* React no tiene una opinión sobre cómo se definen los estilos; en caso de dudas, un buen punto de partida es definir tus estilos en un archivo `*.css` separado como de costumbre y referirse a ellos usando [`className`](https://es.reactjs.org/docs/dom-elements.html#classname).

### ¿Puedo hacer animaciones en React?

React puede usarse para potenciar animaciones. Revisa [React Transition Group](https://reactcommunity.org/react-transition-group/) y [React Motion](https://github.com/chenglou/react-motion) o [React Spring](https://github.com/react-spring/react-spring), por ejemplo.

![React](https://arepa.s3.amazonaws.com/react.png)

## Props: Comunicación entre Componentes

Las **Props** son la forma de enviar y recibir información en nuestros componentes. Son la forma de comunicar cada componente con el resto de la aplicación. Son muy parecidas a los parámetros y argumentos de las funciones en cualquier lenguaje de programación.

```js
// Button.jsx
import React from 'react';

const Button = props => {
  return (
	<div>
	  <button type="button">{props.text}button>
	div>
  );
};

export default Button;
// index.jsx
import React from 'react';
import Button from './components/Button';

ReactDOM.render(
  <Button text="¡Hola!" />,
  document.getElementByid('root'),
);
```

Para complementar pueden hacer esto:
En index.js

```js
const texts = {
  text: 'Click',
  text2: 'Click2',
  text3: 'Click3'
}
ReactDOM.render(<Button {...texts} />, document.getElementById('root'));
```

En Button.jsx

```js
const Button = props => {
  const { text, text2, text3 } = props
  return (
    <div>
      <button type="button">{text}</button>
      <button type="button">{text2}</button>
      <button type="button">{text3}</button>
    </div>
  )
}
```

### **Props**

Las **props** son la forma de enviar y recibir información en nuestros componentes, se podría decir que son los ‘argumentos’ que reciben un componente y se escriben como los atributos de html.

```jsx
<Componente atributo="valor" />
```

Estos props salen de los argumentos de nuestros componentes funcionales o de una variable de clase dentro de ***`this.props`\*** y los valores son asignados directamente en el **ReactDOM.render()**.

```jsx
const ComponenteFuncional = (props) => { // <-- Las props son el único argumento que recibe nuestra función
	...
}

class ComponenteClase extends React.Component {
	const {
		children,
		...
	} = this.props; // <-- Una prop se llama con this.props
	...
}
```

Algunos atributos de html que funcionan como props serían por ejemplo: `src`, `href`, `class`, `id`, en React éstos pasan a ser props que pueden ser manejadas dentro de javascript tal cual como argumentos, por ejemplo podríamos hacer lo siguiente:


```jsx
const Padre = () => (<Link to="/home" />);
const Link = (props)=>{
	return <a href={props.to}>{props.children}></a>
}
```


En el ejemplo anterior manejamos una prop nativa de React que son los children, mismos que serían los hijos de nuestro componente, osea todo lo que está dentro del componente `<Componente>` **`hijos`** `</Componente>`

Cuando creamos un componente propio en React, no se manejan los childrens, pero cuando creamos elementos de html, éstos se manejan tal cual como funciona html, así que es completamente posible que un componente que creemos y le pongamos hijos no renderice ninguno de éstos.

**Lectura, escritura, reactividad, no reactividad de props**

React es bastante flexible pero no permite a los componentes modificar sus propias props, pero sí se lo permite al componente padre.

```jsx
const Componente = (props)=>{
	// props.name = `${props.name} el mejor` // No hacer ésto
	name = `${props.name} el mejor`
	return (
		<div>
			{name}
		</div>
	)
}
```


Que un componente padre pueda modificar las props de sus hijos, pero éstos no puedan es lo correcto, porque las props son la manera de comunicación entre el componente padre y sus hijos y si se pudieran modificar no sabríamos quien generó este mensaje, generando un problema de reactividad por la propagación de los cambios.

```jsx
const Padre = () => {
	const droidName = `C${Math.random()*100}`PO;
	const changeName=()=>{...} // de alguna manera cambiar el nombre del hijo
	return (
		<Hijo onClick={changeName} nombre={droidName} />
	)
}
```


Podríamos decir que un componente sin estado (stateless) funciona como una [función puras](https://medium.com/laboratoria-developers/introducción-a-la-programación-funcional-en-javascript-parte-2-funciones-puras-b99e08c2895d), ya que siempre **regresará algo**, pero ésto no se modificará a menos que se modifiquen los argumentos.

Tales funciones son llamadas por que no tratan de cambiar sus entradas, y siempre devuelven el mismo resultado para las mismas entradas.

>  En VSC, deberían de poder agregar fácilmente constantes y el “import React from ‘react’” simplemente escribiendo “rfc” … o “rafc” para un “react arrow functional component” … 

<img src="https://i.ibb.co/jG23h1y/raf.webp" alt="raf" border="0">

<img src="https://i.ibb.co/BKhZ358/src.webp" alt="src" border="0">

> Uso un plugin muy similar pero para WebStorm
>
> <img src="https://i.ibb.co/XY547vk/web.png" alt="web" border="0">

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.01ca534ca7d3.png)[Fundamentos de JavaScript | Materiales](https://platzi.com/clases/fundamentos-javascript/)

## ¿Qué son los métodos del ciclo vida?

Todos los componentes en React pasan por una serie de fases que generalmente se denominan “Ciclo de Vida del componente” es un proceso que React hace en cada componente, en algunos casos no podemos verlos como un bloque de código y en otros podemos llamarlos en nuestro componente para asignar una actividad según sea el caso necesario.

Los componentes en react pasan por un Montaje, Actualización, Desmontaje y Manejo de errores.

## Montaje:

En esta fase nuestro componente se crea junto a la lógica y los componentes internos y luego es insertado en el DOM.

## Actualización:

En esta fase nuestro componente está al pendiente de cambios que pueden venir a través de un cambio en “state” o “props” esto en consecuencia realizan una acción dentro de un componente.

## Desmontaje:

En esta etapa nuestro componente “Muere” cuando nosotros no necesitamos un elemento de nuestra aplicación, podemos pasar por este ciclo de vida y de esta forma eliminar el componente de la representación que tiene en el DOM.

## Manejo de Errores:

Cuando nuestro código se ejecuta y tiene un error, podemos entrar en una fase donde se puede entender mejor qué está sucediendo con la aplicación.

Algo que debemos tener en cuenta es que un componente NO debe pasar por toda las fases, un componente puede ser montado y desmontado sin pasar por la fase de actualización o manejo de errores.

Ahora que entendemos las fases que cumple el ciclo de vida en React vamos a entrar a detalle en cada uno de ellos para ver qué piezas de código se ejecutan y nos ayudarán a crear aplicaciones en React pasando por un ciclo de vida bien estructurado.

## Montado:

**Constructor()**

Este es el primer método al que se hace un llamado, aquí es donde se inicializan los métodos controladores, eventos del estado.

**getDerivedStateFromProps()**

Este método se llama antes de presentarse en el DOM y nos permite actualizar el estado interno en respuesta a un cambio en las propiedades, es considerado un método de cuidado, ya que su implementación puede causar errores sutiles.

**render()**

Si queremos representar elementos en el DOM en este método es donde se escribe esta lógica, usualmente utilizamos JSX para trabajar y presentar nuestra aplicación.

**ComponentDidMount()**

Este método se llama inmediatamente que ha sido montado en el DOM, aquí es donde trabajamos con eventos que permitan interactuar con nuestro componente.

## Actualización:

**getDerivedStateFromProps()**

Este método es el primero en ejecutarse en la fase de actualización y funciona de la misma forma que en el montaje.

**shouldComponentUpdate()**

Dentro de este método se puede controlar la fase de actualización, podemos devolver un valor entre verdadero o falso si queremos actualizar o no el componente y es utilizado principalmente para optimización.

**render()**

Se llama el método render que representa los cambios en el DOM.

**componentDidUpdate()**

Este método es invocado inmediatamente después de que el componente se actualiza y recibe como argumentos las propiedades y el estado y es donde podemos manejar nuestro componente.

## Desmontado

**componentWillUnmount()**

Este método se llama justo antes de que el componente sea destruido o eliminado del DOM.

## Manejo de Errores:

**getDerivedStateFromError()**

Una vez que se lanza un error este es el primer método que se llama, el cual recibe el error como argumento y cualquier valor devuelto en este método es utilizado para actualizar el estado del componente.

**componentDidCatch()**

Este método es llamado después de lanzarse un error y pasa como argumento el error y la información representada sobre el error.

Ahora que entendemos cada una de las fases que tiene el ciclo de vida de react, podemos utilizarlas según sea necesario en nuestra aplicación y de esta forma crear las interacciones que necesitemos.

<img src="https://i.ibb.co/x1xHjtk/vida.webp" alt="vida" border="0">

<img src="https://i.ibb.co/JnbpYtm/reactdom.webp" alt="reactdom" border="0">

<img src="https://i.ibb.co/h2jt5px/Ciclo-de-react.webp" alt="Ciclo-de-react" border="0">

## State - Events

"React nos permite responder a las interacciones de los usuarios con propiedades como **`onClick`**, **`onChange`**, **`onKeyPress`**, **`onFocus`**, **`onScroll`**, entre otras.

Estas propiedades reciben el nombre de la función que ejecuta el código que responde a las interacciones de los usuarios. Seguramente, esta función usará la función **`this.setState`** para actualizar el estado de nuestro componente.

```js
class Button extends React.Component {
  state = { count: 0 }

  handleClick = () => (
     this.setState({ count: this.state.count + 1 })
  );

  render() {
    const { count } = this.state;

    return (
      <div>
        <h1>Manzanas: {count}</h1>
        <button onClick{this.handleClick}>Sumar</button>
      </div>
    );
  }
}
```

Recuerda que los nombres de estos eventos deben seguir la nomenclatura camelCase: primera palabra en minúsculas, iniciales de las siguientes palabras en mayúsculas y el resto también en minúsculas."

![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/events.html/favicon.ico)[SyntheticEvent – React](https://reactjs.org/docs/events.html)

## Instalación y configuración de entorno

```bash
# Iniciar un repositorio en GIT:
git init
```

```bash
# Iniciar un proyecto de Node.js:
npm init -y

# Instalar React:
npm install --save react react-dom
```

#### Estructura de directorios estandar

Esta es la Estructura del proyecto 	

- src 	

- public 

  - index.html

  - src\components 	

    - src\index.js 

    ```bash
    public/
    	|-index.html
    src/
    	|-Components/
    	|-index.js
    package.json
    ```

    

- **src** —> source --> fuente (nuestro ***código fuente\***)
- **dist** --> distribution --> distribución (se refiere a la versión final que se ***distribuye\*** )

<img src="https://i.ibb.co/h1wQYL5/src.png" alt="src" border="0">

>  Usar create-react-app para empezar en el desarrollo con react y darte una idea de como se estructura tu proyecto, pero después seria conveniente que aprendas a hacerlo desde cero para que aprendas mas sobre la configuración de un proyecto y puedas manipularlo mas fácil y corregir posibles errores que salgan en un futuro.

## Agregando compatibilidad con todos los navegadores usando Babel

**Babel** es una herramienta muy popular para escribir JavaScript moderno y transformarlo en código que pueda entender cualquier navegador.

Instalación de Babel y otras herramientas para que funcione con React:

```bash
npm install --save-dev @babel/core @babel/preset-env @babel/preset-react babel-loader
```

Configuración de Babel (`.babelrc`):

```json
{
  presets": [
    "@babel/preset-env",
    "@babel/preset-react"
  ],
}
```

# 3. Configurar un entorno de trabajo profesional

## Webpack: Empaquetando nuestros módulos

"**Webpack** es una herramienta que nos ayuda a compilar multiples archivos (JavaScript, HTML, CSS, imágenes) en uno solo (o a veces un poco más) que tendrá todo nuestro código listo para producción.

Instalación de Webpack y algunos plugins:

```bash
npm install webpack webpack-cli webpack-dev-server html-webpack-plugin html-loader --save-dev
```

problemas con wepack instalamos la version anterior `npm i webpack-4.17.1`. Con ello funcionara.

Configuración de Webpack (`webpack.config.js`):

```js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
      filename: './index.html',
    }),
  ],
};
```

Script para ejecutar las tareas de Webpack (`package.json`):

```js
{
  ""scripts"": {
    ""build"": ""webpack --mode production""
  },
}
```

Cambia la configuración de .babelrc

```js
{
	"presets": [
	["@babel/preset-env",{ "targets":{ "node":"current" } }], 
	"@babel/preset-react"
	]
}
```

Configuración del archivo webpack.config

```js
// Requerimos el modulo de path el Html plugin que isntalamos
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

// Creamos un nuevo modulo que vamos a exportar con esta configuracion
// Vamos a configurar cada unos de los elementos que necesitamos

module.exports = {
  // Iniciando por la entrada del proyecto
  // Haciendo referencia al archivo principal
  entry: "./src/index.js",
  // En este output, es donde vamos a guardar los archivos resultantes cuando hagamos la configuracion
  output: {
    // La instancia resolve nos ayuda a detectar el directorio donde nos encontramos y el directorio donde vamos a guardar los archivos compilados
    path: path.resolve(__dirname, "dist"),
    // Filename nos pode un nombre al archivo compilado
    filename: "bundle.js"
  },
  // Este elemento resulve las extensiones que vamos a utilizar
  resolve: {
    extensions: [".js", ".jsx"]
  },
  // Modulo con las reglas necesarias
  module: {
    rules: [
      {
        // Regla principal
        // Identificacion de los archivos con una expresion regular
        test: /\.(js|jsx)$/,
        // Exclusion de carpetas
        exclude: /node_modules/,
        // Utilizamos el loader de babel instalado
        use: {
          loader: "babel-loader"
        }
      },
      {
        // Regla para trabajar con los archivos html
        test: /\.html$/,
        // Utilizamos el loader de babel instalado
        use: {
          loader: "html-loader"
        }
      }
    ]
  },
  // Se añaden los plugins que necesitamos
  plugins: [
  // pasamo un objeto con la configuracion que necesitamos
    
    new HtmlWebpackPlugin({
      //Donde esta ubicado el template que tenemos
      template: "./public/index.html",
      filename: "./index.html"
    })
  ]
};
```

Corremos nuestro copilador con `npm run build` para que se aloje el la carpeta `dist`.

## Webpack Dev Server: Reporte de errores y Cambios en tiempo real

Instalación de **Webpack Dev Server**:

```bash
npm install --save-dev webpack-dev-server
```

Script para ejecutar el servidor de Webpack y visualizar los cambios en tiempo real (`package.json`):

```js
{
  ""scripts"": {
    ""build"": ""webpack --mode production"",
    ""start"": ""webpack-dev-server --open --mode development""
  },
}
```

## Estilos con SASS

Los **preprocesadores** como **Sass** son herramientas que nos permiten escribir CSS con una sintaxis un poco diferente y más amigable que luego se transformará en CSS normal. Gracias a Sass podemos escribir CSS con variables, mixins, bucles, entre otras características.

Instalación de Sass:

```bash
npm install --save-dev mini-css-extract-plugin css-loader node-sass sass-loader
```

Configuración de Sass en Webpack (`webpack.config.js`):

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// ...

module: {
  rules: [
    {
      test: /\.(s*)css$/,
      use: [
        { loader: MiniCssExtractPlugin.loader },
        'css-loader',
        'sass-loader',
      ],
    }, 
  ],
},

// ...

plugins: [
  new MiniCssExtractPlugin({
    filename: 'assets/[name].css',
  }),
],`
```

## Configuración final: ESLint y Git Ignore

El **Git Ignore** es un archivo que nos permite definir qué archivos NO queremos publicar en nuestros repositorios. Solo debemos crear el archivo **`.gitignore`** y escribir los nombres de los archivos y/o carpetas que no queremos publicar.
Los linters como **ESLint** son herramientas que nos ayudan a seguir buenas prácticas o guías de estilo de nuestro código.
Se encargan de revisar el código que escribimos para indicarnos dónde tenemos errores o posibles errores. En algunos casos también pueden solucionar los errores automáticamente. De esta manera podemos solucionar los errores incluso antes de que sucedan.
Instalación de ESLint:

```bash
npm install --save-dev eslint babel-eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-react eslint-plugin-jsx-a11y
```

Podemos configurar las reglas de ESLint en el archivo **`.eslintrc`**.

>  [gitignore](http://gitignore.io/) esta es una página donde pueden descargar los gititgnore para sus proyectos, no solo de react.

usar npx para crear el gitignore

```bash
# Y asi les creara el archivo completo con todos los mudulos que se necesitan ignorar
npx gitignore
```

**ESLint:** Es una herramienta que se encarga de revisar el código que escribimos para indicarnos donde tenemos errores o posibles errores.

**Gitignore:** Es un archivo que nos permite definir que archivo NO queremos publicar en nuestro repositorio.

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[eslintrc · GitHub](https://gist.github.com/gndx/60ae8b1807263e3a55f790ed17c4c57a)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[gitignore · GitHub](https://gist.github.com/gndx/747a8913d12e96ff8374e2125efde544)

## Arquitectura de componentes para Platzi Video

<img src="https://i.ibb.co/4sZzL9V/platzivideo.jpg" alt="platzivideo" border="0">

![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)[¿Cuándo crear un Componente? Estructura, Organización y Tipos de Componentes en React](https://platzi.com/blog/estructura-organizacion-y-tipos-de-componentes-en-react/)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[PlatziVideo | React | Proyect](https://github.com/platzi/escuela-js/tree/feature/react)

# 4. Llevar un diseño de HTML y CSS a React

## Estructura del Header

Vamos a usar el [Proyecto Final](https://platzi.com/clases/1640-frontend-developer/21874-presentacion-y-bienvenida-al-curso-de-html-y-c-2/?time=56) del [Curso de Frontend Developer](https://platzi.com/clases/frontend-developer/) para dividir la aplicación en componentes y agregar interactividad con React.

Si eliminas las llaves del arrow function

```jsx
const Header = () => (
	
)
```

Al ser una funcion explicita en react, no necesitas del return. Ya que lo da por hecho que espera que se retorne algo.

Ya no es necesario hacer

```javascript
Import React from 'react'
```

[Mira al post aquí.](https://reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html)

**Código**

```jsx
import React from 'react';
import Header from '../components/Header';

const App = () => {
  <div className='App'>
    <Header />
  </div>;
};

export default App;
```

- Y al momento de renderizarlo el navegador me mandaba el siguiente error:

Uncaught Error: App(…): Nothing was returned from render. This usually means a return statement is missing. Or, to render nothing, return null.

Lo pude solucionar de la siguiente manera

```jsx
import React from 'react';
import Header from '../components/Header';

class App extends React.Component {
  render() {
    return (
      <Header />
    );
  }
}

export default App;
```

Instalé esta extensión:

[Simple React Snippets](https://marketplace.visualstudio.com/items?itemName=burkeholland.simple-react-snippets)

## Estilos del Header

App.scss

```scss
body {
    margin: 0;
    font-family: 'Muli', sans-serif;
    background: #8f57fd;
  }
  
  * {
    box-sizing: border-box;
  }
```

Header.scss

```scss
.header {
    align-items: center;
    background: #8f57fd;
    color: white;
    display: flex;
    height: 100px;
    justify-content: space-between;
    top: 0px;
    width: 100%;
  }
  
  .header__img {
    margin-left: 30px;
    width: 200px;
  }
  
  .header__menu {
      margin-right: 30px;
  }
  
  .header__menu ul {
    display: none;
    list-style: none;
    margin: 0px 0px 0px -14px;
    padding: 0px;
    position: absolute;
    width: 100px;
    text-align: right;
  }
  
  .header__menu:hover ul, ul:hover {
      display: block;
  }
  
  .header__menu li {
    margin: 10px 0px;
  }
  
  .header__menu li a {
    color: white;
    text-decoration: none;
  }
  
  .header__menu li a:hover {
    text-decoration: underline;
  }
  
  .header__menu--profile {
    align-items: center;
    display: flex;
    cursor: pointer;
  }
  
  .header__menu--profile img {
    margin-right: 8px;
    width: 40px;
  }
```

Importamos el `scss`  al archivo `Header,jsx`

```jsx
import '../assets/styles/components/Header.scss';
```



## Estructura y Estilos del Buscador

Una buena practica es no usar texto plano al HTML/JSX, es mejor pasarlo como variable para poder modificarlo con facilidad en el futuro.

**Search.jsx**

```jsx
import React from 'react'

const main_title_text = '¿Que quieres ver hoy?'
const main_input_placeholder = 'Buscar...'

export default () => (
    <section className='main'>
        <h2 className="main__title">{main_title_text}</h2>
        <input type="text" className='input' placeholder={main_input_placeholder} />
    </section>
)
```

`scss`

```scss
.main {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 300px;
  border-radius: 0px 0px 40px 40px;
}

.main__title {
  color: white;
  font-size: 25px;
}
```

Recursos ***css\***:

```scss
.main {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 300px;
  border-radius: 0px 0px 40px 40px;
}

.main__title {
  color: white;
  font-size: 25px;
}

.input {
  background-color: rgba(255, 255, 255, 0.1);
  border: 2px solid white;
  border-radius: 25px;
  color: white;
  font-family: "Muli", sans-serif;
  font-size: 16px;
  height: 50px;
  margin-bottom: 20px;
  outline: none;
  padding: 0px 20px;
  width: 70%;
}
```

## Estructura y Estilos de Carousel y Carousel Item

Para los que hayan tenido problemas con el tema de los scroll y los overflow, pude solucionarlo de la siguiente manera:

1. Creen una nueva clase llamada `categories` en el archivo `Categories.scss`. Allí agreguen la propiesdad `margin-bottom: 2rem`. Esto es para separar los Categories y los scrolls no estén solapados por el Título de la categoría.
2. Quiten la propiedad `position: absolute` de la clase `categories__title`, del archivo anterior. Esto es para que los elementos sigan el flujo natural.
3. Eliminen el overflow en la clase `carousel` y agreguen `overflow-x: auto` y `min-height: 400px`, en el archivo `Carousel.scss`. Estas propiedades servirán para que el scroll horizontal aparezca por cada carrusel, el tamaño mínimo es para que las imágenes del carrusel puedan entrar sin problemas al hacer el hover, como saben, sufren una traslación que las hace más grandes y pueden romper el contenedor.
4. Finalmente cambien la propiedad `with: 200px` por `min-width: 200px` en el archivo `CarouselItem.scss`, esto con el mismo propósito que antes, que los items tengan siempre un tamaño mínimo y se rompan los contenedores.

Esto añade un pequeño problema con el tema del hover sobre un carrusel, se cambiará la opacidad de las imágenes sin haber posado el ratón sobre ellas. Aún no descubro cómo arreglar esto, pero presumo que no se puede hacer con `CSS` puro y tendré que usar `JS`. Les dejo el contenido de los archivos a continuación:

`Categories.sccs`:

```css
.categories {
  margin-bottom: 2rem;
}

.categories__title {
  background: #8f57fd;
  color: white;
  font-size: 20px;
  padding-left: 30px;
  width: 100%;
  margin: 0;
}
```

`Carousel.sccs`:

```css
.carousel {
  padding-left: 30px;
  width: 100%;
  position: relative;
}

.carousel__container {
  overflow-x: auto;
  transition: 450ms transform, 450ms -webkit-transform;
  font-size: 0;
  white-space: nowrap;
  margin: 10px 0px 0px;
  display: flex;
  align-items: center;
  min-height: 400px;
}

.carousel__container:hover > .carousel-item {
  opacity: 0.3;
}

.carousel__container:hover .carousel-item:hover {
  -webkit-transform: scale(1.5);
  transform: scale(1.5);
  opacity: 1;
}
```

Sin la barra de `scroll`

Puedes agregar esto:

```scss
.carousel::-webkit-scrollbar{
     display: none;
}
```

`CarouselItem.sccs`:

```css
.carousel-item {
  border-radius: 20px;
  overflow: hidden;
  position: relative;
  display: inline-block;
  min-width: 200px;
  height: 250px;
  margin-right: 10px;
  font-size: 20px;
  cursor: pointer;
  transition: 450ms all;
  -webkit-transform-origin: center left;
  transform-origin: center left;
}

.carousel-item:hover ~ .carousel-item {
  -webkit-transform: translate3d(100px, 0, 0);
  transform: translate3d(100px, 0, 0);
}

.carousel-item__img {
  width: 200px;
  height: 250px;
  -o-object-fit: cover;
  object-fit: cover;
}

.carousel-item__details {
  align-items: flex-start;
  background: linear-gradient(
    to top,
    rgba(0, 0, 0, 0.9) 0%,
    rgba(0, 0, 0, 0) 100%
  );
  bottom: 0;
  display: flex;
  font-size: 10px;
  flex-direction: column;
  justify-content: flex-end;
  left: 0;
  opacity: 0;
  transition: 450ms opacity;
  padding: 10px;
  position: absolute;
  right: 0;
  top: 0;
}

.carousel-item__details--img {
  width: 20px;
}

.carousel-item:hover .carousel-item__details {
  opacity: 1;
}

.carousel-item__details--title {
  color: white;
  margin: 5px 0px 0px 0px;
}

.carousel-item__details--subtitle {
  color: white;
  font-size: 8px;
  margin: 3px 0px;
}
```

❤️ En **vsCode** existe un plugin llamado **HTML to JSX**.
Muy bueno para resolver de errores de cierres de etiquetas, renombrar el atributo class, etc.

Para los scrollbars, la solucion para que no aparezcan:

```scss
.carousel::-webkit-scrollbar{
     display: none;
}
```

Para los que deseen trabajar con emmet en VSCode deben irse a configuraciones y luego irse al archivo json. **File -> Preferences -> Settings** y busquen el archivo settings.json y agreguen lo siguiente:

estando en el archivo json, pegamos el siguiente codigo
![img](https://static.platzi.com/media/user_upload/Screenshot_20200413_114216-522ae1eb-64bb-4ec1-9917-ff84f9a82fa4.jpg)

```
"emmet.includeLanguages": {
        "javascript": "javascriptreact"
},
"emmet.triggerExpansionOnTab": true
```

![img](https://static.platzi.com/media/user_upload/Screenshot_20200413_114253-c6c9f3cf-c380-4a08-bd0e-0322c62069d6.jpg)

y luego ya solo falta reiniciar el VSCode, y ya pueden trabajar con emmet.

> En React es un estándar nombrar a los componentes en singular, en vez de **Categories** lo mejor sería llamarlo **Category**.

## Estructura y Estilos del Footer

Footer.scss

```scss
.footer {
  align-items: center;
  background-color: #8f57fd;
  display: flex;
  height: 100px;
  width: 100%;

  a {
    color: white;
    cursor: pointer;
    font-size: 14px;
    padding-left: 30px;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
}
```

## Añadiendo imágenes con Webpack

Vamos a usar **File Loader** para acceder a las imágenes de nuestro proyecto desde el código.

Inicialmente, estos archivos estáticos se encuentran junto al código de desarrollo. Pero al momento de compilar, Webpack guardará las imágenes en una nueva carpeta junto al código para producción y actualizará nuestros componentes (o donde sea que usemos las imágenes) con los nuevos nombres y rutas de los archivos.

Instalación de File Loader:

```bash
npm install --save-dev file-loader
```

Configuración de File Loader en Webpack (`webpack.config.js`):

```js
rules: [
  {
    test: /\.(png|gif|jpg)$/,
    use: [
      {
        loader: 'file-loader',
        options: { name: 'assets/[hash].[ext]' },
      }
    ],
  },
],
```

Uso de File Loader con React:

```js
import React from 'react';
import nombreDeLaImagen from '../assets/static/nombre-del-archivo';

const Component = () => (
  <img src={nombreDeLaImagen} />
);

export default Component;
```

agregar soporte para SVG solo agreguen svg en la configuración quedaría así

```js
{
        test: /\.(png|gif|jpg|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/[hash].[ext]',
            },
          },
        ],
      },
```

Para jugar un poco con React y renderizar “categories” desde un arreglo:

```jsx
export default class App extends Component {
  render() {
    const categories = [
      { title: 'Nueva Lista', items: 3 },
      { title: 'Otra Lista', items: 2 },
      { title: 'Mi Lista', items: 1 },
    ];
    return (
      <div className='App'>
        <Header />
        <Search />
        {categories.map((item) => (
          <Categories title={item.title}>
            <Carousel>
              {[...Array(item.items)].map((res, index) => (
                <CarouselItem key={index} />
              ))}
            </Carousel>
          </Categories>
        ))}

        <Footer />
      </div>
    );
  }
}
```

## Imports, Variables y Fuentes de Google en Sass

Así como JavaScript, Sass nos permite almacenar valores en variables que podemos usar en cualquier otra parte de nuestras hojas de estilo.

```css
$theme-font: 'Muli, sans-serif;
$main-color: #8f57fd;

body {
  background: $main-color;
  font-family: $theme-font;
}
```

Podemos guardar nuestras variables en un archivo especial e importarlo desde los archivos de estilo donde queremos usar estas variables.

```css
# Vars.scss
$theme-font: 'Muli, sans-serif;
$main-color: #8f57fd;

# App.scss
@import ""./Vars.scss""

`body {
  background: $main-color;
  font-family: $theme-font;
}
```

También podemos importar hojas de estilo externas a nuestra aplicación. Por ejemplo: las fuentes de Google.

```css
@import url(https://fonts.googleapis.com/css?family=Muli&display-swap)
```

> SOLUCON :::::: en la uri de vars va con comillas simples
>
> ```bash
> ERROR in ./src/assets/styles/App.scss (./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/assets/styles/App.scss)
> Module build failed (from ./node_modules/sass-loader/dist/cjs.js):
> SassError: URI is missing ')'
> on line 1 of src/assets/styles/vars.scss
> from line 1 of /Users/davidlechugahuerta/Desktop/PlatziVideo/src/assets/styles/App.scss
> \>> @import url( https://fonts.googleapis.com/css?family=Muli&display=swap> );
> ```
>
> 

<img src="https://sass-lang.com/assets/img/logos/logo-b6e1ef6e.svg" alt="Sass" style="zoom:8%;" />[Sass](https://sass-lang.com/guide)

# 5. Uso de una API de desarrollo (Fake API)

## Creando una Fake API

Vamos a usar **JSON Server** para crear una Fake API: una API ““falsa”” construida a partir de un archivo JSON que nos permite preparar nuestro código para consumir una API de verdad en el futuro.

Instalación de JSON Server:

```bash
# Instalar json-server
sudo npm install json-server -g

# Recuerda que en Windows debes correr tu terminal de comandos en modo administrador.

# Ejecutar el servidor de JSON Server:

json-server archivoParaTuAPI.json
```

FakeAPI en Windows 10 con

```bash
npx json-server --watch initialState.json
```

Traemos los datos de un JSON [de este enlace](https://gist.github.com/gndx/d4ca4739450afaa614efe4570ac362ee)

***ADVERTENCIA\***: el JSON empieza con ‘initalState’ en vez de ‘initialState’, hay que corregir para no tener errores.

Instalaremos json-server para pretender que traemos datos de una API. Tenemos que hacer esto con privilegios de administrador.

```bash
sudo npm i json-server -g
```

Ejecutamos `json-server initialState.json` y tendremos un servidor corriendo en el puerto 3000.

Y así tenemos lista nuestra fake API.

Ya esta solucionado el problema: tuve que instalar json-server con el siguiente comando `initialState.json`:

```bash
npm i json-server
```

y despues ejecutarlo con el siguiente comando:

```bash
npx json-server --watch initialState.json
```

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[GitHub - typicode/json-server: Get a full fake REST API with zero coding in less than 30 seconds (seriously)](https://github.com/typicode/json-server)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[GitHub - typicode/json-server: Get a full fake REST API with zero coding in less than 30 seconds (seriously)](https://github.com/typicode/json-server)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[initalState.json · GitHub](https://gist.github.com/gndx/d4ca4739450afaa614efe4570ac362ee)

## React Hooks: useEffect y useState

En esta clase el profesor Oscar Barajas nos enseña qué es y cómo implementar React Hooks: una característica de React disponible a partir de la versión 16.8 que nos permite agregar estado y ciclo de vida a nuestros componentes creados como funciones.

React es una librería desarrollada por Facebook que nos ayuda a construir interfaces de usuario interactivas para todo tipo de aplicaciones: páginas web, aplicaciones móviles o de escritorio, experiencias de realidad virtual, entre otras.

Para los que quieran usar `async/await` en lugar de promesas, pude hacerlo modificando el archivo `.babelrc` de esta manera:

```javascript
{
  "presets": [
    [
      "@babel/preset-env",
      {
        "targets": {
          "esmodules": true
        }
      } 
    ],
    "@babel/preset-react"
  ]
}
```

Por otro lado, `useEffect` no puede recibir una función asíncrona (porque no puede devolver una promesa), pero podemos solucionarlo de así:

```javascript
  useEffect(() => {
    const fetchVideos = async () => {
      try {
        const response = await fetch("http://localhost:3000/initialState");
        const data = await response.json();
        return setVideos(data);
      } catch {
        console.log(error);
      }
    };
    fetchVideos();
  }, []);
```

Para los que quieran usar `async/await` en lugar de promesas, pude hacerlo modificando el archivo `.babelrc` de esta manera:

```javascript
{
  "presets": [
    [
      "@babel/preset-env",
      {
        "targets": {
          "esmodules": true
        }
      } 
    ],
    "@babel/preset-react"
  ]
}
```

Por otro lado, `useEffect` no puede recibir una función asíncrona (porque no puede devolver una promesa), pero podemos solucionarlo de así:

```javascript
  useEffect(() => {
    const fetchVideos = async () => {
      try {
        const response = await fetch("http://localhost:3000/initialState");
        const data = await response.json();
        return setVideos(data);
      } catch {
        console.log(error);
      }
    };
    fetchVideos();
  }, []);
```

> UseState: Manejamos el estado.
> UseEffect: Se haceUseState: Manejamos el estado.
> UseEffect: Se hace las transmisiones es decir, peticiones de una API las transmisiones es decir, peticiones de una API

Más información sobre:

- [useEffect](https://midu.dev/react-hooks-use-effect-funcionalidad-en-el-ciclo-vida-componentes/)
- [useState](https://midu.dev/react-hooks-use-state-anadiendo-estado-a-nuestro-componente-funcional/)

![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org/docs/hooks-intro.html/favicon.ico)[Presentando Hooks – React](https://es.reactjs.org/docs/hooks-intro.html)

![img](https://www.google.com/s2/favicons?domain=https://s.ytimg.com/yts/img/favicon-vfl8qSV2F.ico)[React Today and Tomorrow and 90% Cleaner React With Hooks - YouTube](https://www.youtube.com/watch?v=dpw9EHDh2bM)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.01ca534ca7d3.png)[Hooks en Curso de React.js](https://platzi.com/clases/1548-react/18723-hooks2677/)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/static/images/logos/platzi_favicon.01ca534ca7d3.png)[Curso de React.js Avanzado | React | Materiales](https://platzi.com/clases/react-avanzado/)

## Lectura React Hooks

Los **React Hooks** son una característica de React que tenemos disponible a partir de la versión 16.8. Nos permiten agregar estado y ciclo de vida a nuestros componentes creados como funciones.

El Hook **`useState`** nos devuelve un array con dos elementos: la primera posición es el valor de nuestro estado, la segunda es una función que nos permite actualizar ese valor.

El argumento que enviamos a esta función es el valor por defecto de nuestro estado (initial state).

```js
import React, { useState } from 'react';

const Component = () => {
  const [name, setName] = useState('Nombre por defecto');

  return <div>{name}</div>;
}
```

El Hook **`useEffect`** nos permite ejecutar código cuando se monta, desmonta o actualiza nuestro componente.

El primer argumento que le enviamos a `useEffect` es una función que se ejecutará cuando React monte o actualice el componente. Esta función puede devolver otra función que se ejecutará cuando el componente se desmonte.

El segundo argumento es un array donde podemos especificar qué propiedades deben cambiar para que React vuelva a llamar nuestro código. Si el componente actualiza pero estas props no cambian, la función no se ejecutará.

Por defecto, cuando no enviamos un segundo argumento, React ejecutará la función de useEffect cada vez que el componente o sus componentes padres actualicen. En cambio, si enviamos un array vacío, esta función solo se ejecutará al montar o desmontar el componente.

```js
import React, { useState, useEffect } from 'react';

const Component = () => {
  const [name, setName] = useState('Nombre por defecto');

  useEffect(() => {
    document.title = name;
    return () => {
      document.title = 'el componente se desmontó';
    };
  }, [name]);

  return <div>{name}</div>;
}
```

No olvides importar las funciones de los hooks desde la librería de React. También puedes usarlos de esta forma: **`React.useNombreDelHook`**.

[**Hooks de React, ¿Cómo usar useState y useEffect en nuestros componentes?**](https://medium.com/@joangalilea/entendiendo-los-hooks-de-react-cómo-usar-usestate-y-useeffect-en-nuestros-componentes-611b9e826dfa)

## Conectando la información de la API

Recuerda que puedes hacer fetch a la siguiente URL para obtener una lista de videos temporal: http://localhost:3000/initalState.

> Para asegurarse de que las propiedades a las que intentan acceder esten definidas pueden usar el operador " ?. " para cada una de ellas.
>
> ```jsx
> videos.myList?.length
> videos.trends?.map()
> videos.originals?.map()
> ```

## Custom Hooks

React nos permite crear nuestros propios Hooks. Solo debemos seguir algunas convenciones:

- Los hooks siempre deben empezar con la palabra `use`: `useAPI`, `useMovies`, `useWhatever`.
- Si nuestro custom hook nos permite consumir/interactuar con dos elementos (por ejemplo, title y setTitle), nuestro hook debe devolver un array.
- Si nuestro custom hook nos permite consumir/interactuar con tres o más elementos (por ejemplo, name, setName, lastName, setLastName, etc.), nuestro hook debe devolver un objeto.

archivo de hooks, cree una función llamada `selectCategorieTitle` para cambiar los nombres de las categorías y poder utilizarlas.

```jsx
const selectCategorieTitle = categorie => {
    let title
    switch (categorie) {
    case 'trends': {
        title = 'Tendencias'
        break
    }
    case 'originals': {
        title = 'Originales de Platzi Video'
        break
    }
    default: {
        title = categorie
        break
    }
    }
    return title
}

export const useInitialVideos = API => {
    const [videos, setVideos] = useState(null)
    useEffect(() => {
        const getVideos = () => {
            fetch(API)
                .then(videos => videos.json())
                .then(videos => {
                    const response = {}
                    Object.keys(videos).forEach(categorie => {
                        response[selectCategorieTitle(categorie)] = videos[categorie]
                    })
                    setVideos(response)
                })
        }
        getVideos()
    }, [])
    return videos
}
```

Y teniendo ese hook, así quedó mi componente App:

```jsx
export const App = () => {
    const videos = useInitialVideos(API)

    return (
        <div>
            <Header/>
            <Search/>
            {videos && Object.keys(videos).map(categorie => {
                if (videos[categorie].length) {
                    return (
                        <Categories
                            title={categorie}
                            key={categorie}
                        >
                            <Carousel>
                                {videos[categorie].map(video => {
                                    return (
                                        <CarouselItem
                                            image={video.cover}
                                            alt={video.title}
                                            key={video.id}
                                            year={video.year}
                                            title={video.title}
                                            content={video.contentRating}
                                            duration={video.duration}
                                        />
                                    )
                                })}
                            </Carousel>
                        </Categories>
                    )
                }
                return null
            })}
            <Footer/>
        </div>
    )
}
```

![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org/docs/hooks-custom.html/favicon.ico)[Construyendo tus propios Hooks – React](https://es.reactjs.org/docs/hooks-custom.html)

## PropTypes

Los **PropTypes** son una propiedad de nuestros componentes que nos permiten especificar qué tipo de elementos son nuestras props: arrays, strings, números, etc.

Instalación de PropTypes:

```bash
npm install --save prop-types
```

Uso de PropTypes:

```js
import React from 'react';
import PropTypes from 'prop-types';

const Component = ({ name, lastName, age, list }) => {
  // ...
};

Component.propTypes = {
  name: PropTypes.string,
  lastName: PropTypes.string,
  age: PropTypes.number,
  list: PropTypes.array,
};

export default Component;
```

Por defecto, enviar todas nuestras props es opcional, pero con los `propTypes` podemos especificar cuáles props son obligatorias para que nuestro componente funcione correctamente con el atributo `isRequired`.

```js
Component.propTypes = {
  name: PropTypes.string.isRequired, // obligatorio
  lastName: PropTypes.string.isRequired, // obligatorio
  age: PropTypes.number, // opcional,
  list: PropTypes.array, // opcional
};
```

# 6. Usar React Tools

## Debuggeando React con React DevTools

**React DevTools** es una herramienta muy parecida al Inspector de Elementos. Nos permite visualizar, analizar e interactuar con nuestros componentes de React desde el navegador.

Encuentra más información sobre está herramienta en: [github.com/facebook/react-devtools](https://github.com/facebook/react-devtools).

> **Acualización**: La pestaña `React` ha sido sustituida por dos pestañas nuevas en sus `Chrome DevTools`: “⚛️ Componentes” y “⚛️ Profiler”.

Si te sale el siguiente error en la consola:

```bash
bundle.js:6 asset size limit: The following asset(s) exceed the recommended size limit (244 KiB).
This can impact web performance.
Assets: 
  bundle.js (275 KiB)
```

Lo que tienes que hacer para solucionarlo es agregar el siguiente bloque de código a `webpack.config`:

```js
  performance: {
    hints: false
  },
```

[PlatziVideo](https://github.com/musenberg404/platzivideo)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[GitHub - facebook/react-devtools: An extension that allows inspection of React component hierarchy in the Chrome and Firefox Developer Tools](https://github.com/facebook/react-devtools)

![img](https://www.google.com/s2/favicons?domain=https://www.google.com/images/icons/product/chrome_web_store-32.png)[React Developer Tools - Chrome Web Store](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

![img](https://www.google.com/s2/favicons?domain=https://addons.cdn.mozilla.net/favicon.ico?v=2)[React Developer Tools – Get this Extension for 🦊 Firefox (en-US)](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/)

![img](https://www.google.com/s2/favicons?domain=https://github.githubassets.com/favicon.ico)[Page not found · GitHub · GitHub](https://github.com/facebook/react-devtools/blob/master/packages/react-devtools/README.md)

![img](https://www.google.com/s2/favicons?domain=https://static.npmjs.com/b0f1a8318363185cc2ea6a40ac23eeb2.png)[react-devtools - npm](https://www.npmjs.com/package/react-devtools)

## Prepárate para lo que sigue

[Instalacion-de-ract.pdf](https://drive.google.com/file/d/1gUVhAff3TpBePdOrv0jQW16VF5DdWwii/view?usp=sharing)

![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/blog/2019/08/08/react-v16.9.0.html/favicon.ico)[React v16.9.0 and the Roadmap Update – React Blog](https://reactjs.org/blog/2019/08/08/react-v16.9.0.html)