# Aplicacion creado en React

## Platzi Video

Inicializamos el proyecto 

```bash
# Iniciar un repositorio en GIT:
git init
```

```bash
# Iniciar un proyecto de Node.js:
npm init -y

# Instalar React:
npm install --save react react-dom
```

### Estructura del Proyecto

```bash
public/
	|-index.html
src/
	|-Components/
	|-index.js
package.json
```

<img src="https://i.ibb.co/h1wQYL5/src.png" alt="src" border="0">

Instalación de Babel y otras herramientas para que funcione con React:

```bash
npm install --save-dev @babel/core @babel/preset-env @babel/preset-react babel-loader
```

Configuración de Babel (`.babelrc`):

```json
{
  "presets": [
    "@babel/preset-env",
    "@babel/preset-react"
  ],
}
```

Instalación de Webpack y algunos plugins:

```bash
npm install webpack webpack-cli webpack-dev-server html-webpack-plugin html-loader --save-dev
```

Configurar el achivo `.json`

```json
{
	"presets": [
	["@babel/preset-env",{ "targets":{ "node":"current" } }], 
	"@babel/preset-react"
	]
}
```

Instalación de **Webpack Dev Server**:

```bash
npm install --save-dev webpack-dev-server
```

(`package.json`):

```js
{
  ""scripts"": {
    ""build"": ""webpack --mode production"",
    ""start"": ""webpack-dev-server --open --mode development""
  },
}
```

Instalación de Sass:

```bash
npm install --save-dev mini-css-extract-plugin css-loader node-sass sass-loader
```

