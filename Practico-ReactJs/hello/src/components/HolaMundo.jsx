import React from 'react';

const HolaMundo = () => {
  const Hello = 'Hola Mundo!';
  const isTrue = false;
  return(
    <div className="HolaMundo">
      <h1>{Hello}</h1>
      <h2>Curso esencial de React</h2>
      <img src="https://arepa.s3.amazonaws.com/react.png" alt="react" />
      {isTrue ? <h4>Esto es verdadero</h4> : <h5>Soy falso</h5>}
      {isTrue && <h4>Soy Verdadero</h4>}
    </div>
  )
};

export default HolaMundo;