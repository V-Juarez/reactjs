<h1>Introducción a React.js</h1>

<h3>Juan David Castro</h3>

<h1>Tabla de Contenido</h1>

- [1. Primeros pasos con React](#1-primeros-pasos-con-react)
  - [Cómo aprender React.js](#cómo-aprender-reactjs)
  - [Cuándo usar React.js](#cuándo-usar-reactjs)
  - [Instalación con Create React App](#instalación-con-create-react-app)
- [2. Fundamentos de React: maquetación](#2-fundamentos-de-react-maquetación)
  - [JSX: componentes vs. elementos (y props vs. atributos)](#jsx-componentes-vs-elementos-y-props-vs-atributos)
  - [Componentes de TODO Machine](#componentes-de-todo-machine)
  - [CSS en React](#css-en-react)
- [3. Fundamentos de React: interacción](#3-fundamentos-de-react-interacción)
  - [Manejo de eventos](#manejo-de-eventos)
  - [Manejo del estado](#manejo-del-estado)
  - [Contando y buscando TODOs](#contando-y-buscando-todos)
  - [Completando y eliminando TODOs](#completando-y-eliminando-todos)
- [4. Fundamentos de React: escalabilidad](#4-fundamentos-de-react-escalabilidad)
  - [Organización de archivos y carpetas](#organización-de-archivos-y-carpetas)
  - [Persistencia de datos con Local Storage](#persistencia-de-datos-con-local-storage)
  - [Custom Hook para Local Storage](#custom-hook-para-local-storage)
  - [Manejo de efectos](#manejo-de-efectos)
  - [React Context: estado compartido](#react-context-estado-compartido)
  - [useContext](#usecontext)
- [5. Modales y formularios](#5-modales-y-formularios)
  - [Portales: teletransportación de componentes](#portales-teletransportación-de-componentes)
  - [Formulario para crear TODOs](#formulario-para-crear-todos)
- [6. Retos](#6-retos)
  - [Reto: loading skeletons](#reto-loading-skeletons)
  - [Reto: icon component](#reto-icon-component)
- [7. Próximos pasos](#7-próximos-pasos)
  - [Deploy con GitHub Pages](#deploy-con-github-pages)
  - [Qué más puedes aprender de React.js](#qué-más-puedes-aprender-de-reactjs)

# 1. Primeros pasos con React
## Cómo aprender React.js

  Conceptos Basicos:
    - html
    - css
    - JavaScript

React es una libreria que esta rompiendo barreras en la programacion y permite crear plataformas increibles y sexys para el usuario. 

  > Usuario, es genial tu plataforma, toma mi dinero.

Aprender React, te permitira mejar tu estilo de vida, no desesperes que puedes repasar la clase con el que tienes problemas.


  > Que no te moleste no encontrar la respueta inmediate, el profe. es un buen profe. en caso de que no sepa o que la informacion no este. No te preocupes, tu y el profesor pueden buscar la respuesta.

  No pares de aprender.

## Cuándo usar React.js

Si React es tu primera libería de JavaScript, debes saber que muchas de las librerías están basadas en componentes, pero… ¿Realmente qué significa un “componente”? 

☝ Básicamente un componente es un pedacito de tu página web, es decir, puede ser una sección específica de tu página web, o puede ser algún elemento que se repita múltiples veces en la misma. Lo importante a tener en cuenta es que, un componente es una parte específica de tu página, es algo que cumple una acción simple 👀.

*¿El header de mi página puede ser un componente?* **¡Sí!**
*¿El sidebar puede ser un componente?* **¡Por su puesto!**
*Y si tengo varios articulos en mi página…* **¿Puedo convertirlos a componentes? ¡Por su pollo!** 😄

Recuerda que todo puede ser un componente, y esto nos permite modularizar nuestro código. Es decir, podemos dividir y “aislar” cada parte de nuestra página. Si por alguna razón necesitaramos actualizar nuestro header (por ejemplo), bastaría con entrar al componente header modificar una pequeña línea y listo! Ya no tendríamos que buscar el header dentro de tooooodo nuestro HTML UwU.

👀 Otra ventaja de los componentes es que son reutilizables, es decir, puedes usarlos cuantas veces quieras. Por ejemplo, si tuvieras un sitio web sobre blogs, ya sabes que muchos blogs suelen tener una imagen, un título y una descripción. Entonces podríamos crear un componente con la estructura de nuestro blogpost y únicamente mandarle la información que necesitemos por cada blogpost y cada uno se crearía automáticamente!!

<img src="https://i.ibb.co/SQV7j9p/reutilizables.jpg" alt="reutilizables" border="0">

### Prerequisites:

- HTML and CSS
- DOM manipulation with JavaScript
- Practical JavaScript
- ECMAScript6+
- Closures y Scope
- Asynchronous JavaScript

#### Software requirements

- Node.js
- Code Editors
- NPM o Yarn

### Minimum viable product oriented to functionalities

> Make a modular development (component-based) so that users can interact and developers receive feedback, in short development cycles. NOTE: this means to develop complete modular FUNCTIONALITIES NOT single components.

<img src="https://i.ibb.co/993vVDG/react-mvps.webp" alt="react-mvps" border="0">

(Minimum viable product oriented to functionalities in order to allow the user to use them and return us feedback)

> React works pretty well with Analysis of Components and Behaviors

Los componentes permiten separar la interfaz de usuario en piezas independientes, reutilizables y pensar en cada pieza de forma aislada.

Componentes: son las partes en las cuales se divide nuestra aplicación, cabe destacar que estas pueden estar comunicadas entre sí, de esta manera permite escalar de una manera muy fácil nuestra app.
Comportamientos: son las acciones que un componente puede realizar, al igual que los componentes estos pueden estar conectados entre sí.

<img src="https://i.ibb.co/QQDVQNf/component.jpg" alt="component" border="0">

## Instalación con Create React App

Un comando que les puede ahorrar algo de tiempo si tienes **Zsh**

```sh
take curso-react
```

Es lo mismo que

```sh
mkdir curso-react && cd curso-react
```

[<img src="https://cdn.iconscout.com/icon/free/png-256/react-1-282599.png" style="zoom: 25%;" />](https://github.com/facebook/react)

iniciar un proyecto en React desde cero, aquí les dejo los comandos que usé:

1. Abrí la terminal y me dirigí a la carpeta donde guardaré el proyecto con: **cd <nombre-del-proyecto>**
   -Si quieren crear una carpeta más, se puede usar: **mkdir <nombre-de-carpeta>**
2. Una vez en la carpeta: **npx create-react-app <nombre-de-carpeta>**
3. Al terminar de instalar las dependencias: **cd <nombre-de-carpeta>**
4. Para abrirlo en VSC: **code . **
5. Al abrir VSC, puedes abrir la terminal de a misma y escribir: **npm start**. Se abrirá un localhost:3000
   A partir de aquí nos quedaría eliminar logo.svg, reportWebVitals.js, setupTest.js, App.css y App.test.js de la carpeta SRC.
   En Index.js eliminamos la línea 5 y de la 14 a la 17
   En App.js la línea 1 , la linea 2 y de la linea 6 a la 21. Dentro del return podemos escribir un <div>Hola</div>, y listo, se debería poder ver ese Hola, y ya estaría listo para crear y jalar componentes 😄

# 2. Fundamentos de React: maquetación

## JSX: componentes vs. elementos (y props vs. atributos)

En React hay varias formas de crear un elemento:

- **Con clases:** Actualmente no se usa

```jsx
class Componenete extends React.Components {
	render() {
		return (
			//Codigo
		)
	}
} 
```

- **Con React.createElement:** Se sigue usando. Es opcional

  Sintaxis: `React.createElement(elemento, atributos, texto/contenido)`
  En el *elemento* se colocaran el nombre de las etiquetas HTML. Ejemplo, h1, h2, p, form,…

  En los *atributos* se colocaran los atributos de las etiquetas, es decir, id, class, placeholder,…

  En texto o contenido se coloca contenido que va dentro de la etiqueta. Es decir, <h1>**este contenido**</h1>

  Veamos con un ejemplo:

```jsx
const ejemplo1 = React.createElement('h1', {'id': 'title'}, 'Oli React')

const ejemplo2 = React.createElement(
	'p',
	{
		'id': 'paragraph-elemental',
		'class': 'paragraph'
	},
	'Oli React'
)
```

En el ejemplo 1, lo que hice fue colocar el ejemplo de Juan que mostró en el video.

En el ejemplo 2, lo que hice fue colocar un ejemplo parecido, solo que le di una lectura más legible para los casos que tengamos que colocar más atributos y no se nos complique la lectura al tener esto:

```jsx
const ejemplo2 = React.createElement('p',	{'id': 'paragraph-elemental','class': 'paragraph'	},	'Oli React');
```

Ambas versiones del ejemplo 2 son validas, solo es cuestión de que la persona lea mejor el código.

- **Con Funciones:** Se usa actualmente y es más cómodo que usar React.createElement()

```jsx
function Componente = () => {
	return(
		//Codigo
	)
}
```

`ReactDOM.render(qué_elemento, dónde)` se encarga de renderizar el elemento y colocarlo en el dom. Se pasa por por parametro el elemento a colocar en el DOM y en dónde se quiere colocar

Esto es un componente:

```jsx
//Componente
const Componente = () => {
	return (
		//Codigo
	)
}
<Componente />
```

Este es un elemento:

```jsx
//Elemento
<h1>Dorime</h1>
```

Nota: Los componentes son una version traducida de los elementos en HTML a Javascript (JSX).

### Props vs Atributos

Cuando estamos trabajando con React, para definir el atributo class, no usamos `class` sino `className`. React te puede aceptar `class` como atributo, pero luego te saldrá advertencias y, de paso, es una mala práctica

Las propiedades las podemos recibir de los parametros de los componentes. Ejemplo:

```jsx
<App nombre="Dorime">
const App = (props) = {
	return (
		<p>{props.nombre}</p>
	)
}
```

Tambien podemos recibir children que vienen entre el contenido del Componente.

```jsx
<App>
	<p>Dorime, Ameno</p>
</App>
const App = (props) = {
	return (
{props.children}
	)
}
```

Podemos crear componentes usando:

class componente extends React.component = Aunque ya casi nadie usa esta forma de crear componentes.

function App (){} = Por convención se empiezan con mayúscula.

Se usa “className” en los elementos HTML para evitar confusión entre las clases de JS y las de HTML.

Los componentes son invisibles para HTML pero si son visibles para React y este los puede usar para renderizar de la forma más optimizada posible.

Lo que renderiza react en HTML son los elementos.

Usamos JSX para que con una sintaxis parecida a HTML podamos escribir de manera más entendible el código.

Los componentes pueden tener propiedades haciendo más interactivo el componente. Por ejemplo usando props.saludo.

Usando las propiedades podemos reutilizar un mismo componente muchas veces ahorrándonos trabajo.

Todas las propiedades las podemos mandar de dos formas, como lo vimos en la clase

```jsx
<App saludo="Buenas" /> o usando “children” <App>Buenaaaaaas</App>
```

(donde podemos poner también etiquetas de HTML) y usandolo con props.children

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/jsx-in-depth.html/favicon.ico)JSX In Depth – React](https://reactjs.org/docs/jsx-in-depth.html)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/introducing-jsx.html/favicon.ico)Introducing JSX – React](https://reactjs.org/docs/introducing-jsx.html)

[![img](https://www.google.com/s2/favicons?domain=https://es.reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html/favicon.ico)Introducción a la nueva transformación de JSX – React Blog](https://es.reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html)

## Componentes de TODO Machine



Para export por deafult

```jsx
rfce
```

output

```jsx
import React from'react'

functionTodoList() {
  return (
    <div>
      
    </div>
  )
}

export default TodoList;
```

> Un tip:
> Pueden crear varios archivos JavaScript en la terminal de VSC así:
>
> ```shell
> touch {TodoCounter, TodoSearch, TodoItem}.js
> ```
>
> Solo asegúrense de estar en la carpeta src

archivos JS:
/*App.js*/

```jsx
import react from "react";
import { TodoCounter } from "./TodoCounter";
import { TodoSearch } from "./TodoSearch.js";
import { TodoList } from "./TodoList.js";
import { TodoItem } from "./TodoItem.js";
import { CreateTodoButtom } from "./CreateTodoButton.js";
//import './App.css';
const todos=[
  {text:'Cortar cebolla', completed:false},
  {text:'Tormar el curso de intro a react', completed:false},
  {text:'Llorar con la llorona', completed:false}
];
function App() {
  return (
   <react.Fragment>
      <TodoCounter />    
      <TodoSearch />
      <TodoList>
        {todos.map(todo =>(<TodoItem key={todo.text} text={todo.text} />))}
      </TodoList>
      <CreateTodoButtom />      
   </react.Fragment>
  );
}

export default App;
```

/*TodoCounter*/

```jsx
import React from "react";
function TodoCounter(){
    return(
        <h2> Has complentado 2 de 3 ToDos</h2>
    )
}

export {TodoCounter};
```

/*TodoSearch*/

```jsx
import react from "react";

function TodoSearch(){
    return(
        <input placeholder="Cebolla" />
    );
}

export {TodoSearch};
```

/*TodoList*/

```jsx
import react from "react";

function TodoList(props){
    return(
        <section>
            <ul>
                {props.children}
            </ul>
        </section>
    );
}

export { TodoList};
```

/*TodoItem*/

```jsx
import react from "react";

function TodoItem(props){
    return(
        <li>
            <span>C</span>
            <p>{props.text}</p>
            <span>X</span>
        </li>
    );
}

export { TodoItem };
```

/*CreateTodoButtom*/

```jsx
import react from "react";

function CreateTodoButtom(props){
    return(
        <button>+</button>
    );
}

export { CreateTodoButtom};
```

## CSS en React

**Example Todo's**

![Desktop - 1.jpg](https://static.platzi.com/media/user_upload/Desktop%20-%201-d311dd26-9ee9-40ad-a9df-39d01714d7c4.jpg)

![Screenshot_20211003-220921.png](https://static.platzi.com/media/user_upload/Screenshot_20211003-220921-fb7c1663-c6ee-4c56-a94c-4929432e43d8.jpg)

![Capture.PNG](https://static.platzi.com/media/user_upload/Capture-5b5b081f-aacb-4c78-8da0-0dd7981a67f5.jpg)

![X - 2.png](https://static.platzi.com/media/user_upload/X%20-%202-e4a35bc2-6f95-49a5-a372-a506c8629773.jpg)

![img](https://i.ibb.co/cwHMPX7/japanesetodo.png)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/faq-styling.html/favicon.ico)Styling and CSS – React](https://reactjs.org/docs/faq-styling.html)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/dom-elements.html#style/favicon.ico)DOM Elements – React](https://reactjs.org/docs/dom-elements.html#style)

# 3. Fundamentos de React: interacción

## Manejo de eventos

por qué a veces enviamos arrow functions y por qué otras veces no, aquí te lo explico:

Cualquier evento recibe sí o sí una función, es decir, debemos mandarle sí o sí una función para que React internamente pueda ejecutarla en cuanto dicho eventop ocurre.

El asunto, es que tiene que ser sí o sí una función que React pueda ejecutar, por eso no podemos mandar directamente un `console.log()` ni un `alert()`, porque aunque ambos son funciones, nosotros estamos ejecutándolas directamente al ponerles los paréntesis, pero nosotros no debemos ejecutarlas, nosotros solo debemos mandarlas y ya React se encargará de ejecutarlas.

Es por eso que mandamos arrow functions, porque estas son funciones que React puede ejecutar cuando quiera, y pues dentro de esas arrow functions está el código que queremos ejecutar cuando el evento suceda.

```jsx
onClick={() => alert("React sí puede ejecutar esta arrow function cada que le de la gana OwO")}
```


Sin embargo, recordando que los eventos reciben funciones, yo puedo crear una variable **que dentro guarde una función**, por ejemplo:

```jsx
const adentroTengoUnaFuncion = () => {
    console.log("Hola");
    console.log("Soy una función que está siendo guardada dentro de una variable UwU");
}
```


Yo puedo ejecutar esta función sin problemas de esta forma `adentroTengoUnaFuncion()`, pero también puedo mandarsela a React para que él lo ejecute cuando quiera (en este caso, cuando el evento suceda):

```jsx
onClick={adentroTengoUnaFuncion}
```


Nota como aquí mandamos la función **sin paréntesis**, esto es porque en el momento en el que le ponemos paréntesis seríamos nosotros quienes ejecutan la función, pero recuerda que **nosotros no debemos ejecutar la función**, sino React es quien tiene que ejecutarla. ¿Por qué? Pues porque si la ejecutamos nosotros, esta se va a ejecutar justo en el momento que esa línea de código sea leída por nuestra computadora, y nosotros no queremos eso, nosotros queremos que nuestra función se ejecute únicamente cuando el evento suceda, por eso la mandamos sin paréntesis, para que React pueda ejecutarla cuando dicho evento ocurra 😄

Peeeeeero, podemos hacer algo genial (y puede ponerse complicado), no veo razón para hacer esto, pero te lo explico por alimentar tu curiosidad jaja:

Sí podemos ejecutar nosotros la función 👀… Yo sé, esto es totalmente lo contrario a lo que te acabo de decir, pero checa esto 👇

Nosotros sí podemos hacer esto:

```jsx
onClick={adentroTengoUnaFuncion()}
```

Solamente sí nuestra función está así:

```jsx
const adentroTengoUnaFuncion = () => {
    return () => {
        console.log("Hola");
        console.log("Soy una función que está siendo guardada dentro de una variable UwU");
    }
}
```


Wait… what? Es simple 👀 Mi función `adentroTengoUnaFuncion` **está retornando otra función**, eso significa que, en el momento que mi código se ejecute, mi función `adentroTengoUnaFuncion` también se va a ejecutar inmediatamente, pero como esta función está retornando otra función, al final mi evento `onClick` acabará recibiendo la función que necesita para funcionar!!! OwO

**¿Por qué haríamos esto?** Seguramente tenga algún caso de uso, pero también es interesante saber que se pueden hacer este tipo de cosas.

En react todo lo que empiece con on nos va a ayudar a recibir los eventos de nuestros usuarios al cual le vamos a enviar una función que se ejecute cuando los usuarios ejecuten la acción que estamos escuchando. Si no usamos una arrow function el código se va a ejecutar de una, se tiene que envolver en una función para que se ejecute cuando sea necesario, en este caso cuando se de click.

```jsx
<button className="CreateTodoButton" onClick={() => console.log("click")}>
```

Los eventos también se pueden crear guardando la función en una constante, y al momento de llamarlo solo se nombra.

```jsx
const onClickButton = () =>{
alert('Aquí debería ir el modal');

};

return (

<button className="CreateTodoButton" onClick={onClickButton}>

+

</button>

);
```

o

```jsx
const onClickButton = (*msg*) => {
	alert(*msg*);
	
	};
	
	return (
	
	<button
	
	className="CreateTodoButton"
	
	onClick={() => onClickButton("Aquí va el modal:)")}
	
	>
	
	+
	
	</button>
	
);
```

## Manejo del estado

Para poder manejar los estados, vamos a necesitar de react el useState.

```jsx
//Si quieres importar todo React para usar useState
import React from 'react';
//Lo vas a usar de esta forma:
const [searchValue, setSearchValue] = React.useState('')

//Si quieres importar unicamente useState
import { useState } from 'react';
//Lo vas a usar de esta forma: 
const [searchValue, setSearchValue] = useState('')
```

`useState` tiene dos elementos:

- **El valor** (quien seria *value*)
- **La funcion que cambia el valor** (quien seria *setValue*)

**En useState es siempre necesario definir un valor como parametro**. Puede ser un string, array, booleano o número

```jsx
const [searchValue, setSearchValue] = useState('')
const [searchValue, setSearchValue] = useState(false)
const [searchValue, setSearchValue] = useState(['Dorime', 'Ameno'])
const [searchValue, setSearchValue] = useState(0)
```

En la etiqueta donde vamos a manejar el evento, no debemos olvidar colocar el value que recibirá el valor de useState. Ejemplo:

```jsx
<input
  className="TodoSearch"
  placeholder="Ingresa un POYO Todo"
  value={searchValue}
	onChange={onSearchValueChange}
/>
```

Espero que les haya ayudado 🌟 .

Un poco de info sobre como funcionan más los ESTADOS en React:

**Las actualizaciones de estado pueden ser asíncronas**. React puede agrupar varias invocaciones a `setState()` en una sola actualización para mejorar el rendimiento.
Debido a que `props` y `state` pueden actualizarse de forma asincrónica, no debes confiar en sus valores para calcular el siguiente estado.

```jsx
//Creando el estado
const [stateX, setStateX] = React.useState(0); 

// Incorrecta forma de modificar el estado nuevo, usando el estado actual ya que podría fallar al ser asíncrono.
setStateX(stateX + props.increment);
```

Para arreglarlo, usa una segunda forma de `setState()` que acepta una función en lugar de un objeto. Esa función recibirá el **estado** previo como primer argumento, y las **props** en el momento en que se aplica la actualización como segundo argumento:

```jsx
// Forma correcta de actualizarlo si necesitamos usar el estado previo en el calculo.
setStateX((stateX, props) => ({
  stateX + props.increment
}))
```

No nos sucede en nuestra app, pero para saberlo frente a nuevos casos donde tengamos quehacer algo similar y no sepamos porqué falla.

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/hooks-state.html/favicon.ico)Using the State Hook – React](https://reactjs.org/docs/hooks-state.html)

## Contando y buscando TODOs

Combinando el estado con las props podremos enviar desde el componente padre la información del estado y como actualizarlo a todos nuestros componentes. De esta manera los componentes hijos le comunicaran al padre que debe actualizar el estado cuando sea necesario y cuando esto suceda, el padre comunicara el cambio a los demás hijos así toda la app podrá reaccionar a las interacciones de los usuarios.

Cada vez que se cambia el estado se hace render (re-render) con la nueva info, pero como se llama a varios componentes no solo será un re-render sino varios. Hay que tener mucho cuidado con eso.

Yo filtré los Todos de la siguiente manera:

```TypeScript
const todosFiltered = todos.filter(todo => todo.text.toLowerCase().includes(searchValue.toLowerCase()));
```

Queda mucho más compacto y no es necesario hacer la validación inicial del largo puesto que include si le pasas una cadena vacía te muestra todos.

😭

```jsx
const [completedTodo, setCompletedTodo] = React.useState(props.completed)

    const onComplete = () => {
        setCompletedTodo(!completedTodo);
    }

// El elemento quedaría así 

<span 
            onClick={onComplete} 
            className={`Icon Icon-check ${completedTodo && 'Icon-check--active'}`}
            >
                √
            </span>
```

## Completando y eliminando TODOs

método `completeTodo()` por `toggleCompleteTodo()` para permitir que destilde de completado algún TODO que le marco completado sin querer. De esta forma sigue funcionando todo y me parece más genial para el usuario por si se confunde.

La solución :

```jsx
const toggleCompleteTodos = (text) => {
	const todoIndex = todos.findIndex(todo => todo.text === text);
	const newTodos = [...todos];
	newTodos[todoIndex].completed = !newTodos[todoIndex].completed;
	setTodos(newTodos);
}
```

Otra forma de borrar los todos

```jsx
function deleteTodo(text){
    const newTodos = todos.filter(todo=>todo.text !== text)
    setTodos(newTodos)
  }
```

optimizar el código y permitir al usuario cambiar entre completo o no. Quedó así:

```jsx
  const findIndex = (text) => {
    return todos.findIndex(todo => todo.text === text)
  }

  const toggleTodo = (text) => {

    const newTodos = [...todos]
    newTodos[findIndex(text)].completed = !newTodos[findIndex(text)].completed
    setTodos(newTodos)
  }

  const deleteTodo = (text) => {

    const newTodos = [...todos]
    newTodos.splice(findIndex(text), 1)
    setTodos(newTodos)
  }
```

# 4. Fundamentos de React: escalabilidad

## Organización de archivos y carpetas

- Los componentes stateful son los componentes que guardan y manejan estados. Por lo que hemos aprendido hasta ahora sería los que usan alguna variable que usa *React.useState*
- Mientras los componentes stateless son los componentes que solo presentan información. Es decir son los componentes que reciben *props* o simplemente muestran algun contenido

Así lo he organizado:

```sh
/📂src
  App.jsx
  Index.jsx
  /📂components
   /📂TodoCounter
     TodoCounter.jsx
     todocounter.scss
   /📂TodoSearch
     TodoSearch.jsx
     todosearch.scss
   /📂TodoList
     TodoList.jsx
     todolist.scss
```

![as.png](https://static.platzi.com/media/user_upload/as-8f21d250-30d7-48b8-8409-316b243e30a8.jpg)

Para tener una mejor organización dentro de nuestro proyecto vamos a hacer una carpeta para cada componente.

Una desventaja de esa manera de organizar es que muchos archivos tienen el nombre de index, pero visual no ayuda de cierta manera a identificar cuál es cual.

NOTA: Cuando no se especifica un archivo dentro de una carpeta automáticamente llama a index.js

Stateless vs Stateful

**Stateful** son componentes que tienen declaración de estado en su funcion.

**Stateless** son componentes que no tienen ningun tipo de estado declarado en el cuerpo de la funcion.

👀Que un componente reciba, o envíe props, no afecta en que sea stateful o stateless.

En App hay que abstraer la UI en un nuevo componente al cual llamaremos con todas las props que necesitemos desde el componente App general.

## Persistencia de datos con Local Storage

**localStorage:** guarda la información en memoria incluso cuando el navegador se cierre y se reabra.

**sesionStorage:** uarda la informacion en memoria mientras dure la sesión de la página.

**Storage.setItem()**
cuando recibe un nombre y valor, lo almacena o actualiza si ya existe.

Ejemplo:

```js
storage.setItem(nombre, valor);
```


**Storage.getItem()**
Devuelve el valor cuando le pasamos el nombre.

Ejemplo

```js
let userBackground = storage.getItem(userBackground); 

// #000000
```


**Storage.removeItem()**
Elimina el valor almacenado cuando le pasamos el nombre:

Ejemplo

```js
let userBackground = storage.removeItem(userBackground); 

// Queda el valor por defecto en caso que exista, por lo ejemplo un background blanco.
```


**Storage.length**
Devuelve un entero que indica el número de elementos guardados en el objeto ** Storage.**

Ejemplo

```js
function userSettings() {
  localStorage.setItem('userBackground', '#000000');
  localStorage.setItem('font', comic sans');
  localStorage.setItem('fontSize', '18');

  localStorage.length;

 // 3
}
```

**storage.clear();**
borra todos los registros guardados en local.

La principal diferencia entre estas dos, es que el Local Storage no tiene una fecha de expiración y está disponible en la web que estamos desarrollando de forma global. 💯 Lo interesante del Session Storage es que solo esta disponible ventana actual en la que estamos navegando y solo son accesibles para el dominio actual. 🔐.

___

Nos permite tener persistencia de datos directamente en el front-end, sin necesidad del back-end.

Todos los navegadores web (o los mejorcitos) tienen esta herramienta, que nos permite guardar la info que nosotros queramos y mantenerla activa durante mucho tiempo.

- No importa si los usuarios cierran o recargan la ventana, cierran el navegador o apagan la computadora o vuelven dentro de cierto tiempo, local storage va a seguir guardando toda la info en el navegador.
- No solo se puede guardar, también se puede recibir.

Para hacer uso de esta herramienta vamos a crear un item, un nuevo elemento a guardar dentro de local storage.

NOTA: Solo se puede guardar texto, no objetos, números, ni otro tipo de datos.

```jsx
localStorage.setItem('ejemploTodos', ejemplo);  //(lo que queremos guardar, lo que vamos a guardar)

//Para guardar los todos usaremos 
const ejemplo = JSON.stringify() //Nos permite transformar en texto un objeto o array de JS
JSON.parse(ejemplo) //Transforma en JS la variable con el array hecho string

localStorage.getItem('ejemploTodos'); //Nos lo va a mostrar
JSON.parse(localStorage.getItem('ejemploTodos'));
```

> Si les aparece el siguiente error: > Uncaught Error: A cross-origin error was thrown. React doesn’t have access to the actual error object in development
>
> Pueden resolverlo con los siguientes pasos:
>
> 1. Abran Dev tools en dsu navegador (Chrome).
> 2. Vayan a la sección “Application”
> 3. Limpien su local storage, que se encuentra en la sección “Storage” en la barra lateral izquierda.
>
> > El *local storage* es muy útil si quieres “simular” una base de datos como desarrollador Frontend y no sabes mucho de Base de Datos. Al menos, para almacenar información temporalmente
> >
> > Sin embargo, **si borras la cache y las cookies, los datos del local storage se eliminaran**
> >
> > Esto es algo que aprendi cuando hacia una TodoApp mientras aprendía Javascript y el manejo de Local Storage

Para ver en tiempo real las acciones que realizas sobre el localStorage:
ve a la pestaña Application de tu dev tools en google chrome.

![Captura de Pantalla 2021-11-17 a la(s) 17.17.07.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-11-17%20a%20la%28s%29%2017.17.07-01401383-26c1-4f42-8621-a8dd963232be.jpg)

El **Local Storage** tiene un almacenamiento máximo de 5MB y la información no se va con cada request al servidor, la información va a persistir aunque cerremos el navegador.

El **Session Storage** es similar al Local Storage solo que la información está disponible por pestaña o por la ventana del navegador. La información estará disponible solo en esa pestaña.

Las** Cookies** tienen solo un almacenamiento de 4KB, se les puede establecer un tiempo de expiración, la desventaja de usarlo es que al hacer request de imágenes o datos las cookies van junto con la petición.

Si la información no es sensible podemos almacenarla en Local Storage o en Session Storage.

## Custom Hook para Local Storage

Para crear un custom hook es necesario:

1. definir el lugar donde se guardará la variable,
2. implementar una funcion que se encargue de actualizar el valor de la variable.
3. **RETORNAR** la variable y la funcion en forma de array!

Los custom hook SIEMPRE deben comenzar por la palabra *use* ejemplo useLocalStorage, y como es una funcion, debe recibir mínimo 2 parámetros:

1. El nombre de lo que buscará, o creará, y
2. el estado inicial de la variable(puede ser un objeto {}, un array [], un string vacío “”, etc)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/hooks-rules.html/favicon.ico)Rules of Hooks – React](https://reactjs.org/docs/hooks-rules.html)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/hooks-custom.html/favicon.ico)Building Your Own Hooks – React](https://reactjs.org/docs/hooks-custom.html)

## Manejo de efectos

la función **`useEffect`** para determinar cuándo ejecutamos o no el código de nuestro efecto.

🔂 Podemos enviar un **array vacío** para decirle a nuestro efecto **solo se ejecute una vez**, cuando recién hacemos el primer render de nuestro componente.

👂 O también podemos enviar un **array con distintos elementos** para decirle a nuestro efecto que no solo ejecute el efecto en el primer render, sino **también cuando haya cambios** en esos elementos del array.

🔁 Si no enviamos **ningún array** como segundo argumento de nuestro efecto, esta función se ejecutará **cada vez que nuestro componente haga render** *(es decir, cada vez que haya cambios en cualquiera de nuestros estados)*.

❓ **¿Cuál de estas opciones crees que debimos usar en nuestro efecto dentro de `useLocalStorage`?**

Al menos por ahora, lo mejor habría sido enviar un array vacío para que nuestro efecto solo se ejecute una vez, en el primer amado a nuestro custom hook / render de nuestro componente. 😌

Lastimosamente, olvidé escribir ese segundo parámetro durante la clase. Esto hace que el código de mi efecto se ejecute cada vez que hay un cambio en el estado. Y como hacemos cambios a estado dentro del efecto, entonces el efecto se ejecutará sin parar todo el tiempo que usemos la aplicación. 😱

Afortunadamente, como todo el código del useEffect está envuelto en un setTimeout, cada ejecución del código de efecto tarda 1 segundo en volver a ejecutarse, por lo que la app no va a crashear. 😓

¡Este error podemos resolverlo en el siguiente curso de la saga! 🙏
Incluso podemos profundizar muchísimo más en este tipo de errores en un curso de optimización, rendimiento y debugging con React.js. 💚

💪 Mientras tanto, sé mejor que yo. No olvides escribir el segundo parámetro de tu efecto en **useLocalStorage** para que el código de tu efecto solo se ejecute una vez y no tengas problemas de rendimiento en tu versión de TODO Machine.

![Los errores te hacen más fuerte](https://static.platzi.com/media/user_upload/Meme%20Intro%20React%20%281%29-7bcd9f4e-74d4-4204-8abd-a04fcb863bb2.jpg)

- Una cosa es el render de React. Y otra cossa es el render en el navegador.
- React ejecuta el `useEffect` luego del render de React, pero antes del render en el navegador.
- En cambio, React ejecuta el `useLayoutEffect` luego del render de React y del render en el navegador.

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/hooks-effect.html/favicon.ico)Using the Effect Hook – React](https://reactjs.org/docs/hooks-effect.html)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)5 estados clave para crear interfaces coherentes](https://platzi.com/blog/ui-stack/)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)useEffect vs. useLayoutEffect - Platzi](https://platzi.com/blog/useeffect-uselayouteffect/)

## React Context: estado compartido

¿Que es el React Context?

- Es una herramienta de React que permite compartir estados a través de nuestros diferentes componentes de la app.
- Esto a partir de Providers y Consumer.
- Nos ayuda a reducir la cantidad de props que tengamos que compartir en todos los elementos de nuestros componentes.

Ejemplo:

```
//Archivo para usar React Context
const TodoContext = React.createContext();

//TodoContext se convierte asi en un Provider y un Consumer del método createContext.

function TodoProvider(props) {
	//Logica de tu app
	return (
		<TodoContext.Provider value={  { //propiedades (estados) que quieras compartir} }
			{props.children}
		</TodoContext.Provider>
}

export {TodoContext, TodoProvider};
```

Asi envovlviendo tu componente <AppUI> con <TodoProvider> compartes los estados que necesites con el restos de tu app (sin necesidad de recibirlas mediante props) de la siguinete manera:

```jsx
//Dentro del componente App UI
<TodoContext.Consumer>
	{ value => { 
		//estados que tu componente va a necesitar 					usandando value.{estado}
		 }
	}
</TodoContext.Consumer>
```

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/render-props.html/favicon.ico)Render Props – React](https://reactjs.org/docs/render-props.html)

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/context.html/favicon.ico)Context – React](https://reactjs.org/docs/context.html)

[A Guide to React Context and useContext() Hook](https://dmitripavlutin.com/react-context-and-usecontext/)

## useContext

Me encanto este video, me venia haciendo mucho ruido la forma de pasar props `padre>hijo>nieto>...>x` todo el tiempo.

Creo que esta técnica es excelente. ¿Hay alguna forma de como investigar sobre estos `patrones` por asi decirlo, para buscar buenas prácticas de como escribir nuestros componentes en React?

Si ya desde el principio comienzo a escribir con buenas prácticas creo que va a ser mejor y hará más claro mi código.

No se olviden que el argumento de `useContext` debe ser nuestro **objeto** contexto y no por separado el `provider` o el `consumer`, como el profe lo habia planteado que podria usarse cuando lo haciamos con la otra técnica sin el hook `useContext`

Correcto: `useContext(MiContexto)`
Incorrecto: `useContext(MiContexto.Consumer)`
Incorrecto: `useContext(MiContexto.Provider)`

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/hooks-reference.html#usecontext/favicon.ico)Hooks API Reference – Reacthttps://reactjs.org/docs/hooks-reference.html#usecontext](https://reactjs.org/docs/hooks-reference.html#usecontext)

# 5. Modales y formularios

## Portales: teletransportación de componentes

Una posible solución para cerrar el Modal, puede ser la siguiente: Al hacer click en cualquier lugar afuera del componente “Portaleado”, se cierra la modal.

Al componente padre (Modal) se le añade el evento de al hacer click, cerrar modal.

**Modal/index.js**

```js
export const Modal = ({ children, setOpenModal }) => {
  const handleClick = () => setOpenModal(false);

  return ReactDOM.createPortal(
    <Wrapper onClick={handleClick}>{children}</Wrapper>,
    document.getElementById('modal')
  );
};
```

Para impedir que al hacer click dentro del componente “portaleado” se cierre, se usa un evento `stopPropagation()`

**AppUI.js**

```js
<Modal setOpenModal={setOpenModal}>
  <div onClick={(e) => e.stopPropagation()}>Transporteee</div>
</Modal>
```

[![img](https://www.google.com/s2/favicons?domain=https://reactjs.org/docs/portals.html/favicon.ico)Portals – React](https://reactjs.org/docs/portals.html)

## Formulario para crear TODOs

### Aqui el CSS del archivo TodoForm.css de la carpeta TodoForm

```css
form {
  width: 90%;
  max-width: 300px;
  background-color: #fff;
  padding: 33px40px;
  display: flex;
  align-content: center;
  flex-direction: column;
}

label {
  text-align: center;
  font-weight: bold;
  font-size: #1E1E1F;
  margin-bottom: 26px;
}

textarea {
  background-color: #F9FBFC;
  border: 2px, solid #202329;
  border-radius: 2px;
  box-shadow: 0px5px50pxrgba(32, 35, 41, 0.25);
  color: #1E1E1F;
  font-size: 20px;
  text-align: center;
  padding: 12px;
  height: 96px;
  width: calc(100%-25px);
}

textarea::placeholder {
color: #a5a5a5;
font-family: 'Montserrat';
font-weight: 400;
}

textarea:focus {
  outline-color: #61DAFA;
}

.TodoForm-buttonContainer {
  margin-top: 14px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
}

.TodoForm-button {
  cursor: pointer;
  display: inline-block;
  font-size: 20px;
  color: #202329;
  font-weight: 400;
  width: 120px;
  height: 48px;
  border-radius: 2px;
  border: none;
  font-family: 'Montserrat';
}

.TodoForm-button-add {
  background-color: #61DAFA;
  box-shadow: 0px5px25pxrgba(97, 218, 250, 0.5);
}

.TodoForm-button-cancel {
  background: transparent;
}
```

### Codigo del archivo index.js de la carpeta TodoForm

```jsx
import './ToDoForm.css'


<formonSubmit={onSubmit} >
      <label>Escribe tu nuevo To Do</label>
      <textarea
        value = {newTodoValue}
        onChange = {onChange}
        placeholder = "Escribe una nueva tarea"
      />
      <divclassName="TodoForm-buttonContainer">
        <button
          type="button"
          className="TodoForm-button TodoForm-button-cancel"
          onClick = {onCancel}
        >
          Cancelar
        </button>

        <button
          className="TodoForm-button TodoForm-button-add"
          type= "submit"
        >
          Añadir
          </button>
      </div>
    </form>
```

**NOTA:** Recuerden importar el archivvo CSS en el archivo index.js 🧐

![4.png](https://static.platzi.com/media/user_upload/4-3df4194c-45c0-4cd5-ba20-1e0bd448729a.jpg)

# 6. Retos

## Reto: loading skeletons

# Reto: loading skeleton

Muchas aplicaciones se quedan en blanco mientras cargan su contenido. Otras muestran algún mensaje de “cargando” y luego sí renderizan todo el contenido de la aplicación.

Pero para ofrecerle una mejor experiencia a nuestros usuarios (UX) es mejor renderizar todo el contenido posible, incluso si no ha terminado de cargar alguna parte de la aplicación. En TODO Machine, por ejemplo, podemos mostrar varios componentes desde el principio aunque no hayamos terminado de cargar la lista de TODOs.

Pero un párrafo que diga “cargando” definitivamente NO es la mejor forma de comunicarle a los usuarios que estamos cargando (el mensaje es claro, pero podríamos buscar una solución más… estética).

Existen muchas posibles soluciones, desde animaciones sencillas *(como 3 puntitos intercalando diferentes niveles de opacidad)* hasta [loading skeletons *(esqueletos de carga)*](https://platzi.com/blog/tutorial-como-crear-una-animacion-de-carga-de-contenido-tipo-facebook/). Incluso existen herramientas interactivas como [Create React Content Loader](https://skeletonreact.com/) para agilizar estos desarrollos.

El reto de esta clase es que maquetes cualquiera de estas soluciones y nos la muestres en la sección de comentarios. En esta lectura te mostraré mi solución, pero te recomiendo que no la veas hasta intentar tu propia solución.

> 💡 Te recomiendo esta lectura: [5 estados clave para crear interfaces coherentes](https://platzi.com/blog/ui-stack/)

## Tu propio loading skeleton en React

Lo primero que vamos a hacer es crear 3 nuevos componentes para trabajarlos independientemente: **TodosError**, **TodosLoading** y **EmptyTodos**.

![carbon.png](https://static.platzi.com/media/user_upload/carbon-d51e9e2c-d8f4-4e01-8004-f6125bc2d513.jpg)

Ya que tenemos estos 3 componentes, ahora vamos a llamarlos desde el componente AppUI para conectarlos con la aplicación.

![carbon (1).png](https://static.platzi.com/media/user_upload/carbon%20%281%29-d7e38037-a785-40fb-95d6-55ad9a3a1eb8.jpg)

¡Muy bien! Ahora sí podemos concentrarnos mucho mejor para trabajar el estado de carga de TODOS dentro del componente **TodosLoading**.

Para empezar, voy a crear y conectar un archivo **TodosLoading.css** para definir los estilos de mi esqueleto:

![carbon (2).png](https://static.platzi.com/media/user_upload/carbon%20%282%29-c6568583-65bc-46b6-ad2f-b2b1a9d88424.jpg)

Llegó el momento más importante: maquetar.

Debemos definir qué elementos necesitamos para el esqueleto y luego les daremos estilos con CSS. Como la idea es replicar la estructura de un TODO, vamos a necesitar una cajita para el contenedor del TODO, una cajita para el ícono de completar, otra cajita para el ícono de borrar y una última cajita para el texto.

> 💡 Le digo a cada elemento “cajita” porque el objetivo de los esqueletos de carga no es replicar todos estos elementos, sino que cada uno será un elemento “fantasma”. Deben ser lo suficientemente parecidos a los TODOs reales para que los usuarios entiendan que estos elementos están relacionados con los TODOs, pero lo suficientemente abstractos y grisáceos para que se entienda que aún los estamos cargando.

![carbon (3).png](https://static.platzi.com/media/user_upload/carbon%20%283%29-bf37a90a-1f86-4d07-b6f4-84e6e24a6827.jpg)

¡Y ahora debemos crear los estilos!

Primero vamos a definir los tamaños y posiciones de cada elemento (tal cual copiando y pegando los estilos del TodoItem.css, pero cambiando los nombres de las clases y descartando las propiedades innecesarias):

![carbon (4).png](https://static.platzi.com/media/user_upload/carbon%20%284%29-4915f59e-9f35-47dd-acd1-62cf7b739278.jpg)

Ahora vamos a todas las cajitas (menos la del texto) para darles un color de fondo con gradiente:

![carbon (5).png](https://static.platzi.com/media/user_upload/carbon%20%285%29-8ce49083-b052-4ec5-a0ba-d3a4382f0843.jpg)

Luego le configuraremos un tamaño de fondo lo suficientemente grande como para que pueda darla vuelta sin dejar espacios vacíos (400% es más que suficiente):

![carbon (6).png](https://static.platzi.com/media/user_upload/carbon%20%286%29-5d6ac227-9d79-44dd-81da-874ade9b9720.jpg)

Y finalmente le daremos una animación que cambie la posición del fondo al principio, a la mitad y al final (te recomiendo darle al menos 3 segundos de duración para que tu animación no se vea atropellada, sino por el contrario con un efecto suave e hipnotizante):

![carbon (7).png](https://static.platzi.com/media/user_upload/carbon%20%287%29-f7a6159f-dc51-4b3f-b8d3-a0899cc9aef1.jpg)

> 💡 Puedes ir a `useLocalStorage.js` y cambiar el tiempo que tardamos en llamar a nuestro efecto con la función setTimeout para poder visualizar tu esqueleto de carga correctamente.

Recuerda que puedes tomar esta trilogía de cursos para aprender muchísimo más sobre transformaciones, transiciones y animaciones con CSS:

**Loading and Empty state screens**:

![img](https://i.imgur.com/hdoWpbw.gif)

**Error State Screen**:

![screenshootTodoError.png](https://static.platzi.com/media/user_upload/screenshootTodoError-c72ec20a-805d-4504-b869-bf5255457021.jpg)

## Reto: icon component

# Reto: icon component

Por ahora los íconos de nuestra aplicación solo son etiquetas span con algún carácter representando la acción que conseguirán los usuarios al presionarlos. No está mal, funciona. Pero queremos reutilizar estos íconos fuera del componente TodoItem.

Por eso el reto de esta clase es que crees tu propio componente TodoIcon, las reglas son:

Te debe permitir elegir qué tipo de ícono quieres (marcar como completado, borrar o incluso algunos más que podamos necesitar).
Cada ícono también debe poder tener estados (cambios al color o alguna otra propiedad del ícono para darle feedback a los usuarios de que realizaron alguna acción, como dar click o pasar el mouse por encima).
Es válido que uses varios componentes en vez de solo uno para definir la lógica de tus íconos.

Haz tu mayor esfuerzo por completar el reto y publicar tu solución en los comentarios. Luego de eso puedes ver mi propuesta de solución.

## Solución:

La ventaja de usar letras para simular el comportamiento de nuestros íconos es que podemos cambiarles el color con CSS extremadamente fácil. La desventaja es que no son muy “estéticas”.

La ventaja de usar imágenes es que podemos tener la versión más estética de cada ícono. La desventaja es que no podemos cambiarles el color con CSS, necesitamos a fuerzas otra imagen con el nuevo color.

Afortunadamente, tenemos una tercera alternativa con las ventajas de ambos mundos: svg.

Las imágenes en SVG son diseñadas por la persona encargada del diseño, pero al exportarlas en formato SVG tenemos la imagen “maquetada” con etiquetas HTML que podemos modificar con CSS.

Su única desventaja es la complejidad de su implementación. Pero somos hijas e hijos de Platzi, no vinimos para lo fácil, sino para nunca parar de aprender. ¡Así que a la carga!

Empecemos creando un componente TodoIcon en su propia carpeta y con sus respectivos archivos de JavaScript y CSS:

![carbon (8).png](https://static.platzi.com/media/user_upload/carbon%20%288%29-7742af27-18f7-4095-8276-159e7f044b41.jpg)

Ahora debemos pensar qué propiedades pueden necesitar nuestros íconos, yo elegí las siguientes:

`type`: para seleccionar qué ícono svg vamos a mostrar.
`color`: para seleccionar el color de nuestro ícono svg, por defecto será `gray`.
`onClick`: para invocar alguna función cuando le demos click al contenedor de nuestro ícono.

Recuerda que los usuarios no siempre dan click o presionan la pantalla de sus celulares con completa precisión, por lo que es muy buena idea crear un contenedor invisible alrededor de nuestros íconos.

> 💡 Material Design tiene una excelente [guía sobre diseño de íconos](https://material.io/design/iconography/system-icons.html), te recomiendo estudiarla para descubrir más detalles interesantes.

Yo decidí utilizar una etiqueta span para el contenedor de los íconos. Así que este span recibirá la propiedad onClick, mientras que el SVG (que ya en un momento vamos a crear) recibirá las otras dos propiedades.

También utilizaré la prop `type` para darle clases personalizadas a cada contenedor de mis íconos.

![carbon (9).png](https://static.platzi.com/media/user_upload/carbon%20%289%29-02448fa0-61d2-4555-87a8-f7d3a8268f05.jpg)

¡Contenedor listo!

Ahora debemos encargarnos de los SVG. Está perfecto si quieres hacerlos a mano con herramientas como Figma o Illustrator.

En mi caso simplemente usaré alguna herramienta de íconos como [Flaticon](https://flaticon.com/) y descargaré los íconos que vea conveniente en la misma carpeta TodoIcon. De esta forma podremos importarlos como componentes de React gracias a la configuración por defecto que nos ofrece Create React App.

Te recomiendo importar tus íconos SVG desde tus componentes en React de esta forma:

![carbon (10).png](https://static.platzi.com/media/user_upload/carbon%20%2810%29-4ce0e539-51ee-4b3c-a305-eec8427486f7.jpg)

Que luego podremos llamar así:

![carbon (11).png](https://static.platzi.com/media/user_upload/carbon%20%2811%29-9ce35dff-496a-4d58-a93d-3ed49bb4cdc2.jpg)

Yo usaré dos íconos (uno de hecho y otro de delete):

![carbon (12).png](https://static.platzi.com/media/user_upload/carbon%20%2812%29-5165fb55-04f8-431a-bd2d-3e9ee77b54eb.jpg)

Lo que nos falta es determinar cuál SVG mostrar dependiendo de la propiedad type que nos envíe el componente que llame a nuestro TodoIcon.

Hay muchas formas de lograr esto, mi forma favorita es crear un objeto con todos los íconos que nuestro componente puede mostrar:

![carbon (13).png](https://static.platzi.com/media/user_upload/carbon%20%2813%29-741119c3-acf7-46b9-8729-506aef6da3db.jpg)

Y luego dentro de nuestro componente TodoIconllamamos al SVG que esté dentro de la propiedad con el mismo nombre que recibamos en la prop `type`:

![carbon (14).png](https://static.platzi.com/media/user_upload/carbon%20%2814%29-bb1c3646-d37b-4162-865c-bad7db48baf1.jpg)

Así automáticamente mostraremos el icono CheckSVG cuando la prop `type` tenga el valor `check` o el icono DeleteSVG cuando tenga el valor `delete`.

Aunque hay un superpoder más que podemos usar. Si envolvemos nuestros íconos dentro de funciones vamos a poder enviarles propiedades que cambien su presentación o comportamiento.

En este caso voy a usar esta funcionalidad para enviarle la prop `color` a nuestros SVG (y así cambiarles su color cuando los componentes que llamen a nuestros íconos así lo requieran).

![carbon (15).png](https://static.platzi.com/media/user_upload/carbon%20%2815%29-e0a37aa5-52e7-412d-a3bb-88c02689ed7b.jpg)

El último paso para que nuestros componentes funcionen sería agregar nuestro código CSS al nuevo archivo de estilos TodoIcon.css (y de paso borrar el código que ya no necesitamos en TodoItem.css):

![carbon (16).png](https://static.platzi.com/media/user_upload/carbon%20%2816%29-38badf59-6ed1-4e1b-9167-03c11739c55b.jpg)

¡Excelente! Nuestro componente TodoIcon ya funciona y podemos llamarlo desde el componente TodoItem para darle sus íconos mucho mejor configurados.

Aunque antes quiero hacer un poco más de composición.

Cada vez que llamamos a nuestro ícono debemos enviar varias props. Pero ¿qué tal si crearamos un componente que envíe todas esas props por nosotros?

Lo que voy a hacer es crear dos nuevos componentes, CompleteIcon y DeleteIcon. Cada uno llamará al componente TodoIcon con sus respectivas props necesarias. Y luego podremos llamar mucho más fácil a estos dos nuevos componentes donde los necesitemos.

![carbon (17).png](https://static.platzi.com/media/user_upload/carbon%20%2817%29-aebf6017-68f3-465e-93a8-0b304eb28fb3.jpg)

Y estos componentes ahora sí los llamaremos desde TodoItem:

![carbon (18).png](https://static.platzi.com/media/user_upload/carbon%20%2818%29-b382e83d-47e0-445c-9596-5daffac98aae.jpg)

Así seguiría construyendo mi componente TodoIcon con todos los íconos que vaya requiriendo nuestra aplicación TODO Machine.

Existe una librería de React donde solo tenemos que instalarla, importar el icono que queremos y ya podremos usarlo en nuestros componentes.


#### Instalación 🚀

```jsx
npm install react-icons --save
```

#### Cómo usar la herramienta

En nuestro componente, importar los iconos de la siguiente manera:

```jsx
import { NOMBRE_DEL_ICONO } from'react-icons/CATEGORIA_DEL_ICONO'
```

Pero… de donde obtengo los nombres y categorias de los iconos 🤔. Entren a su sitio web [**React Icons**](https://react-icons.github.io/react-icons/), escoge el que mas te guste y utilizalo en la parte de tu código en donde lo necesites.

**Ejemplo:**

```jsx
import { FiArrowDownCircle } from'react-icons/fi'
```

Para usarlo, deben hacerlo igual que un componente. Por ejemplo:

```jsx
import { FiArrowDownCircle } from'react-icons/fi'

const MiComponente = () => {
  return (
	<div>
        <h3>Add a new task</h3>
        <button>
	  	<FiClipboard />
	</button>
      </div>	
  )  
}
```

También pueden usar props especiales para pasarle un tamaño o color sin tener que hacerlo mediante CSS.

```jsx
<button>
	  	<FiClipboardsize="30px"color="#000000" />
</button>
```

![eliablm.github.io_todo-app_.png](https://static.platzi.com/media/user_upload/eliablm.github.io_todo-app_-76c2a7bc-3dec-4e97-a805-662155ac23a8.jpg)

# 7. Próximos pasos

## Deploy con GitHub Pages

Instalar `gh-pages` 

```sh
npm install --save-dev gh-pages
```

[![img](https://www.google.com/s2/favicons?domain=https://platzi.github.io/curso-intro-react//curso-intro-react/favicon.ico)React Apphttps://platzi.github.io/curso-intro-react/](https://platzi.github.io/curso-intro-react/)


## Qué más puedes aprender de React.js