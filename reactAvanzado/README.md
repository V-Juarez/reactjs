<h1>React avanzado</h1>

<h1>Miguel Ángel Durán</h1>

<h1>Tabla de Contenido</h1>

- [1. Introducción al curso avanzado de React](#1-introducción-al-curso-avanzado-de-react)
  - [Qué necesitas para este curso y qué aprenderás sobre React.js](#qué-necesitas-para-este-curso-y-qué-aprenderás-sobre-reactjs)
  - [Proyecto y tecnologías que usaremos](#proyecto-y-tecnologías-que-usaremos)
- [2. Preparando el entorno de desarrollo](#2-preparando-el-entorno-de-desarrollo)
  - [Clonando el repositorio e instalando Webpack](#clonando-el-repositorio-e-instalando-webpack)
  - [Instalación de React y Babel](#instalación-de-react-y-babel)
  - [Zeit es ahora Vercel](#zeit-es-ahora-vercel)
  - [Linter, extensiones y deploy con Now](#linter-extensiones-y-deploy-con-now)
- [3. Creando la interfaz con styled-components](#3-creando-la-interfaz-con-styled-components)
  - [¿Qué es CSS-in-JS?](#qué-es-css-in-js)
  - [Creando nuestro primer componente: Category](#creando-nuestro-primer-componente-category)
  - [Creando ListOfCategories y estilos globales](#creando-listofcategories-y-estilos-globales)
  - [Usar información real de las categorías](#usar-información-real-de-las-categorías)
  - [Creando PhotoCard y usando react-icon](#creando-photocard-y-usando-react-icon)
  - [SVGR: de SVG a componente de ReactJS](#svgr-de-svg-a-componente-de-reactjs)
  - [Creando animaciones con keyframes](#creando-animaciones-con-keyframes)
- [4. Hooks](#4-hooks)
  - [¿Qué son los Hooks?](#qué-son-los-hooks)
  - [useEffect: limpiando eventos](#useeffect-limpiando-eventos)
  - [useCategoriesData](#usecategoriesdata)
  - [Usando Intersection Observer](#usando-intersection-observer)
  - [Uso de polyfill de Intersection Observer e imports dinámicos](#uso-de-polyfill-de-intersection-observer-e-imports-dinámicos)
  - [Usando el localStorage para guardar los likes](#usando-el-localstorage-para-guardar-los-likes)
  - [Custom Hooks: useNearScreen y useLocalStorage](#custom-hooks-usenearscreen-y-uselocalstorage)
- [5. GraphQL y React Apollo](#5-graphql-y-react-apollo)
  - [¿Qué es GraphQL y React Apollo? Inicializando React Apollo Client y primer HoC](#qué-es-graphql-y-react-apollo-inicializando-react-apollo-client-y-primer-hoc)
  - [Parámetros para un query con GraphQL](#parámetros-para-un-query-con-graphql)
  - [Usar render Props para recuperar una foto](#usar-render-props-para-recuperar-una-foto)
  - [Refactorizando y usando variables de loading y error](#refactorizando-y-usando-variables-de-loading-y-error)
  - [Usando las mutaciones con los likes](#usando-las-mutaciones-con-los-likes)
- [6. Reach Router](#6-reach-router)
  - [¿Qué es Reach Router? Creando la ruta Home](#qué-es-reach-router-creando-la-ruta-home)
  - [Usando Link para evitar recargar la página](#usando-link-para-evitar-recargar-la-página)
  - [Creando la página Detail](#creando-la-página-detail)
  - [Agregando un NavBar a nuestra app](#agregando-un-navbar-a-nuestra-app)
  - [Estilando las páginas activas](#estilando-las-páginas-activas)
  - [Rutas protegidas](#rutas-protegidas)
- [7. Gestión del usuario](#7-gestión-del-usuario)
  - [Introducción a React.Context](#introducción-a-reactcontext)
  - [Creación del componente UserForm; y Hook useInputValue](#creación-del-componente-userform-y-hook-useinputvalue)
  - [Estilando el formulario](#estilando-el-formulario)
  - [Mutaciones para registro](#mutaciones-para-registro)
  - [Controlar estado de carga y error al registrar un usuario](#controlar-estado-de-carga-y-error-al-registrar-un-usuario)
  - [Mutaciones para iniciar sesión](#mutaciones-para-iniciar-sesión)
  - [Persistiendo datos en Session Storage](#persistiendo-datos-en-session-storage)
  - [Hacer like como usuario registrado](#hacer-like-como-usuario-registrado)
  - [Mostrar favoritos y solucionar fetch policy](#mostrar-favoritos-y-solucionar-fetch-policy)
  - [Cerrar sesión](#cerrar-sesión)
- [8. Mejores prácticas, SEO y recomendaciones](#8-mejores-prácticas-seo-y-recomendaciones)
  - [Últimos retoques a las rutas de nuestra aplicación](#últimos-retoques-a-las-rutas-de-nuestra-aplicación)
  - [React Helmet](#react-helmet)
  - [Midiendo el performance de nuestra app y usando React.memo()](#midiendo-el-performance-de-nuestra-app-y-usando-reactmemo)
  - [React.lazy() y componente Suspense](#reactlazy-y-componente-suspense)
  - [Usando PropTypes para validar las props](#usando-proptypes-para-validar-las-props)
  - [PWA: generando el manifest](#pwa-generando-el-manifest)
  - [PWA: soporte offline](#pwa-soporte-offline)
  - [Testing con Cypress](#testing-con-cypress)
- [9. Conclusiones](#9-conclusiones)
  - [¡Felicidades!](#felicidades)

# 1. Introducción al curso avanzado de React

## Qué necesitas para este curso y qué aprenderás sobre React.js

## Proyecto y tecnologías que usaremos

# 2. Preparando el entorno de desarrollo

## Clonando el repositorio e instalando Webpack

## Instalación de React y Babel

## Zeit es ahora Vercel

## Linter, extensiones y deploy con Now

# 3. Creando la interfaz con styled-components

## ¿Qué es CSS-in-JS?

## Creando nuestro primer componente: Category

## Creando ListOfCategories y estilos globales

## Usar información real de las categorías

## Creando PhotoCard y usando react-icon

## SVGR: de SVG a componente de ReactJS

## Creando animaciones con keyframes

# 4. Hooks

## ¿Qué son los Hooks?

## useEffect: limpiando eventos

## useCategoriesData

## Usando Intersection Observer

## Uso de polyfill de Intersection Observer e imports dinámicos

## Usando el localStorage para guardar los likes

## Custom Hooks: useNearScreen y useLocalStorage

# 5. GraphQL y React Apollo

## ¿Qué es GraphQL y React Apollo? Inicializando React Apollo Client y primer HoC

## Parámetros para un query con GraphQL

## Usar render Props para recuperar una foto

## Refactorizando y usando variables de loading y error

## Usando las mutaciones con los likes

# 6. Reach Router

## ¿Qué es Reach Router? Creando la ruta Home

## Usando Link para evitar recargar la página

## Creando la página Detail

## Agregando un NavBar a nuestra app

## Estilando las páginas activas

## Rutas protegidas

# 7. Gestión del usuario

## Introducción a React.Context

## Creación del componente UserForm; y Hook useInputValue

## Estilando el formulario

## Mutaciones para registro

## Controlar estado de carga y error al registrar un usuario

## Mutaciones para iniciar sesión

## Persistiendo datos en Session Storage

## Hacer like como usuario registrado

## Mostrar favoritos y solucionar fetch policy

## Cerrar sesión

# 8. Mejores prácticas, SEO y recomendaciones

## Últimos retoques a las rutas de nuestra aplicación

## React Helmet

## Midiendo el performance de nuestra app y usando React.memo()

## React.lazy() y componente Suspense

## Usando PropTypes para validar las props

## PWA: generando el manifest

## PWA: soporte offline

## Testing con Cypress

# 9. Conclusiones

## ¡Felicidades!